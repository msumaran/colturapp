'use strict';
angular.module('newApp')
  .controller('reportesCtrl', ['$scope', '$http', 'pluginsService','$location', function ($scope,$http,pluginsService,$location) {

    var json = [];
    $http.get('backend/reportes/mensual').then(function(result){
        
        json = result.data;


        var lineChartData = {
        labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre"],
        datasets: [
          {
              label: "Ano 2016",
              fillColor: "rgba(220,220,220,0.2)",
              strokeColor: "rgba(220,220,220,1)",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: json[2016]
          },
          {
              label: "Ano 2017",
              fillColor: "rgba(49, 157, 181,0.2)",
              strokeColor: "#319DB5",
              pointColor: "#319DB5",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "#319DB5",
              data: json[2017]
          }
        ]
        }
        var ctx = document.getElementById("line-chart").getContext("2d");
        window.myLine = new Chart(ctx).Line(lineChartData, {
            responsive: true,
            tooltipCornerRadius: 0
        });
    });

    var json = [];
    $http.get('backend/reportes/visitas').then(function(result){
        
        json = result.data;


        var lineChartData = {
        labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio"],
        datasets: [
          {
              label: "Ano 2016",
              fillColor: "rgba(49, 157, 181,0.2)",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#319DB5",
              pointHighlightFill: "#319DB5",
              pointHighlightStroke: "#319DB5",
              data: json[2017]
          }
        ]
        }
        var ctx2 = document.getElementById("line-visitas").getContext("2d");
        window.myLine = new Chart(ctx2).Line(lineChartData, {
            responsive: true,
            tooltipCornerRadius: 0
        });
    });

          

          


          


      
      $scope.$on('$destroy', function () {
          
      });
  }]);