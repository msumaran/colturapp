'use strict';

/**
 * @ngdoc overview
 * @name newappApp
 * @description
 * # newappApp
 *
 * Main module of the application.
 */
var MakeApp = angular
  .module('newApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ng-xlsx',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
      $routeProvider
        .when('/', {
            templateUrl: 'master/dashboard',
            controller: 'dashboardCtrl'
        })
        .when('/viajes', {
            templateUrl: 'master/viajes',
            controller: 'viajesCtrl'
        })
        .when('/horarios', {
            templateUrl: 'master/horarios',
            controller: 'horariosCtrl'
        })
        .when('/hotel', {
            templateUrl: 'master/hotel',
            controller: 'hotelCtrl'
        })
        .when('/agenda', {
            templateUrl: 'master/agenda',
            controller: 'agendaCtrl'
        })
        .when('/destinos', {
            templateUrl: 'master/destinos',
            controller: 'destinosCtrl'
        })
        .when('/imagenes', {
            templateUrl: 'master/imagenes',
            controller: 'imagenesCtrl'
        })
        .when('/emergencia', {
            templateUrl: 'master/emergencia',
            controller: 'emergenciaCtrl'
        })
        .when('/recomendaciones', {
            templateUrl: 'master/recomendaciones',
            controller: 'recomendacionesCtrl'
        })
        .when('/vuelos', {
            templateUrl: 'master/vuelos',
            controller: 'vuelosCtrl'
        })
        .when('/participantes', {
            templateUrl: 'master/participantes',
            controller: 'participantesCtrl'
        })
        .when('/opciones', {
            templateUrl: 'master/opciones',
            controller: 'opcionesCtrl'
        })
        .when('/historial', {
            templateUrl: 'master/historial',
            controller: 'historialCtrl'
        })

        .when('/notificaciones', {
            templateUrl: 'master/notificaciones',
            controller: 'notificacionesCtrl'
        })


        .when('/contactos', {
            templateUrl: 'master/contactos',
            controller: 'contactosCtrl'
        })
        .when('/prospectos', {
            templateUrl: 'master/prospectos',
            controller: 'prospectosCtrl'
        })
        .when('/perdidos', {
            templateUrl: 'master/perdidos',
            controller: 'perdidosCtrl'
        })
        .when('/verclientes/:id', {
            templateUrl: function(params){ return 'master/verclientes/' + params.id; },
            controller: 'verclientesCtrl'
        })
        .when('/empresas', {
            templateUrl: 'master/empresas',
            controller: 'empresasCtrl'
        })

        .when('/brief', {
            templateUrl: 'master/brief',
            controller: 'briefCtrl'
        })

        .otherwise({
            redirectTo: '/'
        });
  });


// Route State Load Spinner(used on page or content load)
MakeApp.directive('ngSpinnerLoader', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default
                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$routeChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                });
                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$routeChangeSuccess', function() {
                    setTimeout(function(){
                        element.addClass('hide'); // hide spinner bar
                    },500);
                    $("html, body").animate({
                        scrollTop: 0
                    }, 500);
                });
            }
        };
    }
])
