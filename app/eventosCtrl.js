'use strict';
angular.module('newApp')
  .controller('eventosCtrl', ['$scope', '$http', 'applicationService', function ($scope,$http,applicationService) {
      
      var model = 'eventos';
      function editableTable() {
          
          if(oTable_eventos != null) oTable_eventos.fnDestroy();
          var oTable_eventos = $('#table-editable').dataTable({
              "ajax": 'api/get/'+model,
              "pageLength": 25,
              columns: [
                  { data: "name" },
                  { data: "eventos_tipo" },
                  {data : "fecha"},
                  {data : "ubicacion"},
                  {data : "opciones"}
                  ],
              
              "bDestroy": true,
              "searching": true
          });
          $('#table-editable a.delete').live('click', function (e) {
              e.preventDefault();
              if (confirm("Estas seguro que quieres eliminarlo?") == false) {
                  return;
              }

              var id = $(this).data('id');
              var nRow = $(this).parents('tr')[0];

              $http.get('api/del/'+model+'/'+id).then(successCallback, errorCallback);
              function successCallback(){

                  
                  oTable_eventos.fnDeleteRow(nRow);
              }
              function errorCallback(){

                  
                  alert('Hubo un error al borrar el registro!');
              }
              
              
              // alert("Deleted! Do not forget to do some ajax to sync with backend :)");
          });

          $('.create').live('click', function (e) {
                applicationService.create($(this),oTable_eventos);
                
          });
          $('.dataTables_filter input').attr("placeholder", "Buscar un evento...");

      };

      editableTable();

      $scope.$on('$destroy', function () {
          $('table').each(function () {
              if ($.fn.dataTable.isDataTable($(this))) {
                  $(this).dataTable({
                      "bDestroy": true
                  }).fnDestroy();
              }
          });
      });
  }]);
