'use strict';
angular.module('newApp')
  .controller('viajesCtrl', ['$scope', '$http', 'applicationService','$location','$route', function ($scope,$http,applicationService,$location,$route) {
      
      //$scope.modalShown = true;
      console.log('viajesCtrl');
      $scope.model = 'viajes';
      $scope.get = '';
      $scope.name_controller = 'Viajes';
      $scope.id = '';
      angular.$locationat = $location; 
      angular.$routeat = $route;
      /*
      var id = $location.search().id; 
      var segment = $location.path().split("/")[2];
      if(id == undefined){
       id = segment;
      }
      */
      $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
             
      }
      $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
      }
      $scope.borrar = function(id){
        $scope.id = id;
        if (confirm("Seguro?")) {
            $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                $scope.fetchContent();
            });
        }
      }
      $scope.fetchContent = function() {
          $http.get('api/get/'+$scope.model+'/'+$scope.id).then(function(result){
            var resultado = []; // my object
            if( result.data.data != undefined ){

              $.each(result.data.data, function (key, data) {
                  resultado.push(data);
              });
              
            }
            $scope.viajes = resultado;
          });
      }

      $scope.fetchContent();

     

      $scope.$on('$destroy', function () {
          
      });
  }]);
