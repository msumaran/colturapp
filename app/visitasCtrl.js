'use strict';
angular.module('newApp')
  .controller('visitasCtrl', ['$scope', '$http', 'applicationService','$location','$route', function ($scope,$http,applicationService,$location,$route) {
      
      var model = 'clientes_visitas';
      angular.$locationat = $location; 
      angular.$routeat = $route; 


      function editableTable($location) {
          
          if(oTable_visitas != null) oTable_visitas.fnDestroy();
          var oTable_visitas = $('#table_visitas').dataTable({
              "ajax": 'api/get/'+model,
              "pageLength": 25,
              columns: [
                  {data: "name" },
                  {data: "tipo" },
                  {data : "cliente"},
                  {data : "opciones"}
                  ],
              "bDestroy": true,
              'processing': true,
              "searching": true,
              "columnDefs": [
                { className: "clickeable", "targets": [ 0 ] },
                { className: "clickeableclientes", "targets": [ 1 ] }
              ]
          });
          $('#table_visitas a.delete').live('click', function (e) {
              e.preventDefault();
              if (confirm("Estas seguro que quieres eliminarlo?") == false) {
                  return;
              }

              var id = $(this).data('id');
              var nRow = $(this).parents('tr')[0];

              $http.get('api/del/'+model+'/'+id).then(successCallback, errorCallback);
              function successCallback(){

                  
                  oTable_visitas.fnDeleteRow(nRow);
              }
              function errorCallback(){

                  
                  alert('Hubo un error al borrar el registro!');
              }
              
              
              // alert("Deleted! Do not forget to do some ajax to sync with backend :)");
          });
          
          $('#table_visitas tbody').on( 'click', '.clickeableclientes', function () {
              var id = oTable_visitas.fnGetData(this);
              

              angular.$locationat.path('/verclientes/'+id).search('id='+id);
              angular.$routeat.reload();
          } );

          $('.create').live('click', function (e) {
                applicationService.create($(this),oTable_visitas);
                
          });
          $('.dataTables_filter input').attr("placeholder", "Buscar un cliente...");

      };

      editableTable();

      $scope.$on('$destroy', function () {
          $('table').each(function () {
              if ($.fn.dataTable.isDataTable($(this))) {
                  $(this).dataTable({
                      "bDestroy": true
                  }).fnDestroy();
              }
          });
      });
  }]);
