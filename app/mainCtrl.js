﻿angular.module('newApp').controller('mainCtrl',
    ['$scope', 'applicationService', 'quickViewService', 'builderService', 'pluginsService', '$location',
    function ($scope, applicationService, quickViewService, builderService, pluginsService, $location) {
        console.log('mainCtrl');
        $(document).ready(function () {
            applicationService.init();
            quickViewService.init();
            builderService.init();
            pluginsService.init();
            //Dropzone.autoDiscover = false;
        });

        $scope.$on('$viewContentLoaded', function () {
            pluginsService.init();
            applicationService.customScroll();
            applicationService.handlePanelAction();
            $('.nav.nav-sidebar .nav-active').removeClass('nav-active active');
            $('.nav.nav-sidebar .active:not(.nav-parent)').closest('.nav-parent').addClass('nav-active active');

            if($location.$$path == '/' || $location.$$path == '/layout-api'){
                $('.nav.nav-sidebar .nav-parent').removeClass('nav-active active');
                $('.nav.nav-sidebar .nav-parent .children').removeClass('nav-active active');
                if ($('body').hasClass('sidebar-collapsed') && !$('body').hasClass('sidebar-hover')) return;
                if ($('body').hasClass('submenu-hover')) return;
                $('.nav.nav-sidebar .nav-parent .children').slideUp(200);
                $('.nav-sidebar .arrow').removeClass('active');
            }
            if($location.$$path == '/'){
                $('body').addClass('dashboard');
            }
            else{
                $('body').removeClass('dashboard');
            }

        });

        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };

}]);

/********************************************************************
*
* VUELOS CTRL
*
********************************************************************/
angular.module('newApp').controller('vuelosCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route',
    function ($scope, applicationService, $http, $location, $route) {
        console.log('vuelosCtrl');
        applicationService.select_viajes();
        $scope.elegir = function() {
            $scope.vuelosdiv = true;
            $scope.vuelosselect = false;
            $scope.viajes_id = $("#viajes_id_select_vuelos").val();
            $scope.get = '?viajes_id='+$scope.viajes_id;
            $scope.fetchContent();

            return false;
        }
        $scope.vuelosdiv = false;
        $scope.vuelosselect = true;
        $scope.model = 'vuelos';
        $scope.get = '';
        $scope.name_controller = 'Vuelos';
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_vuelos/'+$scope.viajes_id).then(function(result){
              var resultado = []; // my object
              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

              }
              $scope.vuelos = resultado;
            });
        }

}]);
/********************************************************************
*
* HORARIOS CTRL
*
********************************************************************/
angular.module('newApp').controller('horariosCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route',
    function ($scope, applicationService, $http, $location, $route) {
        console.log('horariosCtrl');
        applicationService.select_viajes();

        $scope.model = 'horarios';
        $scope.agenda_id = $location.search().id;
        $scope.get = '?agenda_id='+$scope.agenda_id;
        $scope.name_controller = 'Horarios';
        angular.$locationat = $location;
        angular.$routeat = $route;
        console.log($scope.agenda_id);

        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_horarios/'+$scope.agenda_id).then(function(result){
              var resultado = []; // my object
              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

              }
              $scope.horarios = resultado;
            });
        }
        $scope.fetchContent();
}]);

/********************************************************************
*
* AGENDA CTRL
*
********************************************************************/
angular.module('newApp').controller('agendaCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route',
    function ($scope, applicationService, $http, $location, $route) {
        console.log('agendaCtrl');
        applicationService.select_viajes();
        $scope.guardar_agenda = function(){
                $.ajax({
                     type: "POST",
                     dataType: 'json',
                     timeout: '10000',
                     url: 'backend/update_dialog',
                     data: $('#form_agenda').serialize(),
                         success: function(json){

                         }
                   });


        }
        $scope.elegir = function() {
            $scope.agendadiv = true;
            $scope.agendaselect = false;
            $scope.viajes_id = $("#viajes_id_select_agenda").val();
            $scope.get = '?viajes_id='+$scope.viajes_id;
            $("input[name='viajes_id']").val($scope.viajes_id);
            $scope.fetchContent();

            return false;
        }
        $scope.agendadiv = false;
        $scope.agendaselect = true;
        $scope.model = 'agenda';
        $scope.get = '';
        $scope.name_controller = 'Agenda';
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.horarios = function(id){

            angular.$locationat.path('/horarios/').search('id='+id);
            angular.$routeat.reload();
        }

        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_agenda/'+$scope.viajes_id).then(function(result){
              var resultado = []; // my object
              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    console.log(data);
                    $("#titulo").val(data.titulo);
                    $("#titulo_fecha").val(data.titulo_fecha);
                    resultado.push(data);
                });

              }
              $scope.agenda = resultado;
            });
        }

}]);


/********************************************************************
*
* EMERGENCIA CTRL
*
********************************************************************/
angular.module('newApp').controller('emergenciaCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route',
    function ($scope, applicationService, $http, $location, $route) {
        console.log('emergenciaCtrl');
        applicationService.select_viajes();
        $scope.elegir = function() {
            $scope.emergenciadiv = true;
            $scope.emergenciaselect = false;
            $scope.viajes_id = $("#viajes_id_select_emergencia").val();
            $scope.get = '?viajes_id='+$scope.viajes_id;
            $scope.fetchContent();

            return false;
        }
        $scope.emergenciadiv = false;
        $scope.emergenciaselect = true;
        $scope.model = 'emergencia';
        $scope.get = '';
        $scope.name_controller = 'Emergencia';
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_emergencia/'+$scope.viajes_id).then(function(result){
              var resultado = []; // my object
              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

              }
              $scope.emergencia = resultado;
            });
        }

}]);


/********************************************************************
*
* IMAGENES CTRL
*
********************************************************************/
angular.module('newApp').controller('imagenesCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route',
    function ($scope, applicationService, $http, $location, $route) {
        console.log('imagenesCtrl');
        applicationService.select_viajes();
        $scope.elegir = function() {
            $scope.imagenesdiv = true;
            $scope.imageneselect = false;
            $scope.viajes_id = $("#viajes_id_select_imagenes").val();
            $scope.get = '?viajes_id='+$scope.viajes_id;
            $scope.fetchContent();

            return false;
        }
        $scope.imagenesdiv = false;
        $scope.imageneselect = true;
        $scope.model = 'imagenes';
        $scope.get = '';
        $scope.name_controller = 'Imagenes';
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_imagenes/'+$scope.viajes_id).then(function(result){
              var resultado = []; // my object
              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

              }
              $scope.imagenes = resultado;
            });
        }

}]);

/********************************************************************
*
* DESTINOS CTRL
*
********************************************************************/
angular.module('newApp').controller('destinosCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route',
    function ($scope, applicationService, $http, $location, $route) {
        console.log('destinosCtrl');
        applicationService.select_viajes();
        $scope.elegir = function() {
            $scope.destinosdiv = true;
            $scope.destinoselect = false;
            $scope.viajes_id = $("#viajes_id_select_destinos").val();
            $scope.get = '?viajes_id='+$scope.viajes_id;
            $scope.fetchContent();

            return false;
        }
        $scope.destinosdiv = false;
        $scope.destinoselect = true;
        $scope.model = 'destinos';
        $scope.get = '';
        $scope.name_controller = 'Destinos';
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_destinos/'+$scope.viajes_id).then(function(result){
              var resultado = []; // my object
              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

              }
              $scope.destinos = resultado;
            });
        }

}]);


/********************************************************************
*
* Recomendaciones CTRL
*
********************************************************************/
angular.module('newApp').controller('recomendacionesCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route',
    function ($scope, applicationService, $http, $location, $route) {
        console.log('RecomendacionesCtrl');
        applicationService.select_viajes();
        $scope.elegir = function() {
            $scope.recomendacionesdiv = true;
            $scope.recomendacionesselect = false;
            $scope.viajes_id = $("#viajes_id_select_recomendaciones").val();

            $scope.get = '?viajes_id='+$scope.viajes_id;
            $scope.fetchContent();

            return false;
        }
        $scope.recomendacionesdiv = false;
        $scope.recomendacionesselect = true;
        $scope.model = 'recomendaciones';
        $scope.get = '';
        $scope.name_controller = 'Recomendaciones';
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_recomendaciones/'+$scope.viajes_id).then(function(result){
              var resultado = []; // my object
              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

              }
              $scope.recomendaciones = resultado;
            });
        }

}]);

/********************************************************************
*
* HOTEL CTRL
*
********************************************************************/
angular.module('newApp').controller('hotelCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route',
    function ($scope, applicationService, $http, $location, $route) {
        console.log('hotelCtrl');
        applicationService.select_viajes();
        $scope.elegir = function() {
            $scope.hoteldiv = true;
            $scope.hotelselect = false;
            $scope.viajes_id = $("#viajes_id_select_hotel").val();
            $scope.get = '?viajes_id='+$scope.viajes_id;
            $scope.fetchContent();

            return false;
        }
        $scope.hoteldiv = false;
        $scope.hotelselect = true;
        $scope.model = 'hotel';
        $scope.get = '';
        $scope.name_controller = 'Hotel';
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_hotel/'+$scope.viajes_id).then(function(result){
              var resultado = []; // my object
              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

              }
              $scope.hoteles = resultado;
            });
        }

}]);


/********************************************************************
*
* opciones CTRL
*
********************************************************************/
angular.module('newApp').controller('opcionesCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route','$excel',
    function ($scope, applicationService, $http, $location, $route, $excel) {
        console.log('opcionesCtrl');
        applicationService.select_viajes();




        $scope.participantesdiv = false;
        $scope.participantesselect = true;
        $scope.model = 'opciones';
        $scope.get = '';
        $scope.name_controller = 'opciones';
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_opciones/').then(function(result){
              var resultado = []; // my object
              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

              }
              $scope.opciones = resultado;
            });
        }
         $scope.fetchContent();

}]);


/********************************************************************
*
* participantes CTRL
*
********************************************************************/
angular.module('newApp').controller('participantesCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route','$excel',
    function ($scope, applicationService, $http, $location, $route, $excel) {
        console.log('participantesCtrl');
        applicationService.select_viajes();



        $scope.elegir = function() {
            $scope.participantesdiv = true;
            $scope.participantesselect = false;
            $scope.viajes_id = $("#viajes_id_select_participantes").val();
            $scope.get = '?viajes_id='+$scope.viajes_id;
            $scope.fetchContent();
            return false;
        }
        $scope.participantesdiv = false;
        $scope.participantesselect = true;
        $scope.model = 'participantes';
        $scope.get = '';
        $scope.name_controller = 'Participantes';
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.data = {};
        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_participantes/'+$scope.viajes_id).then(function(result){
              var resultado = []; // my object
              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

              }
              $scope.hoteles = resultado;
            });
        }

        $excel.setup({
            drop_selector: '#drop',
            input_selector: '#pero',
            campos: [
                'name',
                'lastname',
                'dni',
                'email'
            ],
            worker_file: 'assets/web/plugins/xlsx/worker_readfile.js',
            delimiter: '|',
            success: function (excel_data) {
                $scope.tramamsg = 'Subido con exitó!';
                $scope.$apply(function () {

                    var data = {
                            json:excel_data
                    };
                    $scope.data = data;

                });
            }
        });
        $scope.subirtrama = function(){

            $http.post('api/excel/'+$scope.viajes_id,$scope.data).then(function(result){
                        $scope.fetchContent();
            });
            alert('Trama subida con éxito!');
        }


}]);

/********************************************************************
*
* historial CTRL
*
********************************************************************/
angular.module('newApp').controller('historialCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route','$excel',
    function ($scope, applicationService, $http, $location, $route, $excel) {
        console.log('historialCtrl');
        applicationService.select_viajes();

        $scope.participantesdiv = false;
        $scope.participantesselect = true;
        $scope.model = 'historial';
        $scope.get = '';
        $scope.name_controller = 'historial';
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {
            $http.get('api/get_historial/').then(function(result){
              var resultado = []; // my object
              if( result.data != undefined ){

                $.each(result.data, function (key, data) {
                    resultado.push(data);
                });

              }
              $scope.historial = resultado;


              console.log('js', $scope.historial);
            });
        }
        $scope.fetchContent();
}]);








/********************************************************************
*
* notificaciones CTRL
*
*********************************************************************/

angular.module('newApp').controller('notificacionesCtrl',
    ['$scope', 'applicationService', '$http', '$location','$route',
    function ($scope, applicationService, $http, $location, $route) {
        console.log('notificacionesCtrl');
        applicationService.select_viajes();

        $scope.elegir = function() {
            $scope.agendadiv = true;
            $scope.agendaselect = false;
            $scope.viajes_id = $("#viajes_id_select_notificaciones").val();
            $scope.get = '?viajes_id='+$scope.viajes_id;
            $("input[name='viajes_id']").val($scope.viajes_id);
            $scope.fetchContent();

            return false;
        }
        $scope.agendadiv = false;
        $scope.agendaselect = true;
        $scope.model = 'notificaciones';
        $scope.get = '';
        $scope.name_controller = 'notificaciones';
        angular.$locationat = $location;
        angular.$routeat = $route;

        $scope.create = function(){
          $scope.id = '';
          applicationService.create_app( $scope);
        }
        $scope.editar = function(id){
          $scope.id = id;
          applicationService.create_app( $scope);
        }
        $scope.borrar = function(id){
            $scope.id = id;
            if (confirm("Seguro?")) {
                $http.get('api/del/'+$scope.model+'/'+$scope.id).then(function(result){
                    $scope.fetchContent();
                });
            }
        }
        $scope.fetchContent = function() {

            console.log('fetch entro');

            $http.get('api/get_notificaciones/'+$scope.viajes_id).then(function(result){
              var resultado = []; // my object

              console.log(result.data.data);

              if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    console.log(data);
                    $("#title").val(data.title);
                    $("#text").val(data.text);
                    $("#fecha_total").val(data.text);
                    resultado.push(data);
                });

              }
            $scope.agenda = resultado;


             console.log($scope.agenda);
        });
    }

}]);
