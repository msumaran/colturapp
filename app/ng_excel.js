(function() {
     var app = angular.module('ng-xlsx', []);

    app.service('_xlsx', [function () {

        var x = XLSX;
        var service = {
            options: {
                drop_selector: undefined,
                input_selector: undefined,
                campos: [],
                worker_file: undefined,
                delimiter: ',',
                success: function () {}
            },
            excel: {
                result: []
            },
            use_worker: typeof Worker !== 'undefined',
            worker_file: '',
            setup: function (options) {

                this.options = Object.assign(this.options, options);

                if (this.options.drop_selector) {

                    this.setup_dropfile();
                }

                if (this.options.input_selector) {

                    this.setup_selectfile();
                }
            },
            setup_dropfile: function () {

                jq_selector = this.options.drop_selector;

                $(jq_selector).on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                });

                $(jq_selector).on('dragover dragenter', function() {
                    $(jq_selector).addClass('drag');
                });

                $(jq_selector).on('dragleave dragend drop', function() {
                    $(jq_selector).removeClass('drag');
                });

                $(jq_selector).on("drop", this.get_file_data);
            },
            setup_selectfile: function () {

                $(service.options.input_selector).on("change", service.get_file_data);
            },
            get_file_data: function (e) {
                e.preventDefault();

                var file;

                if (e.originalEvent.dataTransfer !== undefined) {

                    console.log('Image Dropped');

                    file = e.originalEvent.dataTransfer.files[0];
                } else {

                    console.log('Image Selected');

                    file = e.originalEvent.target.files[0];
                }

                var reader = new FileReader();

                reader.onload = (function() {

                    return function(e) {

                        service.process_file(e.target.result, service.procesa_excel);
                    };
                })(file);

                reader.readAsBinaryString(file);
            },
            process_file: function (data, callback) {

                if (this.use_worker && this.options.worker_file) {

                    console.log('Using webWorkers');

                    var worker = new Worker(this.options.worker_file);

                    worker.onmessage = function(e) {

                        switch(e.data.t) {
                            case 'ready': break;
                            case 'e': console.error(e.data.d); break;
                            case 'xlsx': callback(JSON.parse(e.data.d)); break;
                        }
                    };

                    worker.postMessage({
                        d : data,
                            b : true
                        });
                    } else {
                        console.log('Not using webWorkers');

                        var workbook = x.read(data, {type: 'binary'});

                        callback(workbook);
                    }
                },
                procesa_excel: function (workbook) {

                    var hojas = [],
                        hoja,
                        rows,
                        row,
                        campos,
                        i,
                        j;

                    workbook.SheetNames.forEach(function(sheetName) {

                        var csv = x.utils.sheet_to_csv(workbook.Sheets[sheetName], {FS: service.options.delimiter});

                        if (csv.length > 0) {

                            rows = csv.split('\n');

                            hoja = {
                                name: sheetName,
                                rows: []
                            };

                            for (i = 0; i < rows.length; i += 1) {

                                if (i === 0) {

                                    if (!service.options.campos.length) {

                                        console.log("No hay campos, se toma la primera linea");

                                        service.options.campos = rows[0].split("|");
                                    }
                                } else if (rows[i].length > 0) {

                                    campos = rows[i].split('|');

                                    row = {};

                                    for (j = 0; j < campos.length; j += 1) {

                                        row[service.options.campos[j]] = campos[j];
                                    }

                                    hoja.rows.push(row);
                                }
                            }

                            hojas.push(hoja);
                        }
                    });

                    service.excel.result = hojas[0].rows;

                    service.options.success(service.excel.result);
                }
            };

            return service;
        }]);

        app.provider("$excel", function excelProvider() {

            this.$get = ['_xlsx', function (_xlsx) {

                return _xlsx;
            }];
        });
    })();