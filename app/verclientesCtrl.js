'use strict';
angular.module('newApp')
  .controller('verclientesCtrl', ['$scope', '$http', 'pluginsService','$location', function ($scope,$http,pluginsService,$location) {
      $scope.isTabActive = true;
      var model = 'clientes';
      var id = $location.search().id; 

     
      $scope.content = [];
      $scope.fetchContent = function() {
          function formatNumber(str) {
              var parts = (str + "").split("."),
                  main = parts[0],
                  len = main.length,
                  output = "",
                  i = len - 1;

              while(i >= 0) {
                  output = main.charAt(i) + output;
                  if ((len - i) % 3 === 0 && i > 0) {
                      output = " " + output;
                  }
                  --i;
              }
              // put decimal part back
              if (parts.length > 1) {
                  output += "." + parts[1];
              }
              return output;
          }


          $http.get('api/get_id/'+model+'/'+id).then(function(result){
              
              $scope.content = result.data;
          });
          $http.get('api/get/clientes_brief/').then(function(result){
              var resultado = []; // my object
              $.each(result.data.data, function (key, data) {
                  data.input = 'http://'+$location.host()+'/crm/download/brief/'+data.id+'/'+id;
                  resultado.push(data);
              });
              $scope.clientes_brief = resultado;
          });


          $http.get('api/get_cliente/cotizacion/'+id).then(function(result){
              var resultado = []; // my object
              $.each(result.data.data, function (key, data) {
                  
                  resultado.push(data);
              });
              $scope.cotizacion = resultado;
          });


          $http.get('api/get_contactos/'+id).then(function(result){
              var resultado = []; // my object
              $.each(result.data.data, function (key, data) {
                  
                  resultado.push(data);
              });
              $scope.contactos = resultado;
          });


          $('.create').live('click', function (e) {
                applicationService.create($(this),oTable_contactos);
                
          });
      }


      $scope.fetchContent();

      
      $scope.$on('$destroy', function () {
          
      });
  }]);
