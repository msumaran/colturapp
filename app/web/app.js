'use strict';

/**
 * @ngdoc overview
 * @name newappApp
 * @description
 * # newappApp
 *
 * Main module of the application.
 */
var MakeApp = angular
  .module('newApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
      $routeProvider
        .when('/', {
            templateUrl: 'web/inicio',
            controller: 'inicioCtrl'
        })
       
        .when('/intro', {
            templateUrl: 'web/intro',
            controller: 'inicioCtrl'
        })
        .when('/destino', {
            templateUrl: 'web/destino',
            controller: 'destinoCtrl'
        })
        .when('/hotel', {
            templateUrl: 'web/hotel',
            controller: 'hotelCtrl'
        })
        .when('/vuelos', {
            templateUrl: 'web/vuelos',
            controller: 'vuelosCtrl'
        })
        .when('/agenda', {
            templateUrl: 'web/agenda',
            controller: 'agendaCtrl'
        })
        .when('/participantes', {
            templateUrl: 'web/participantes',
            controller: 'participantesCtrl'
        })
        .when('/recomendaciones', {
            templateUrl: 'web/recomendaciones',
            controller: 'recomendacionesCtrl'
        })
        .when('/emergencia', {
            templateUrl: 'web/emergencia',
            controller: 'emergenciaCtrl'
        })
        .when('/imagenes', {
            templateUrl: 'web/imagenes',
            controller: 'imagenesCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
  });
// Route State Load Spinner(used on page or content load)




MakeApp.directive('customScroll', function ($log) {
    return {
        restrict: 'A',
        scope: {
            config: '&customScroll'
        },
        link: function (scope, element, iAttrs, controller, transcludeFn) {
            var config = scope.config();
            console.log(element);
            // create scroll elemnt
            $(element).mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                autoHideScrollbar: false,
                scrollInertia: 150,
                theme: "amarillo",
                set_height: 350,
                advanced: {
                    updateOnContentResize: true
                }
            });
           
        }
    };
});
MakeApp.directive('ngSpinnerLoader', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default
                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$routeChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                });
                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$routeChangeSuccess', function() {
                    setTimeout(function(){
                        element.addClass('hide'); // hide spinner bar
                    },500);
                    $("html, body").animate({
                        scrollTop: 0
                    }, 500);
                });
            }
        };
    }
])

