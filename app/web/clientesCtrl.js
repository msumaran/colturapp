'use strict';
angular.module('newApp')
  .controller('clientesCtrl', ['$scope', '$http', 'applicationService','$location','$route', function ($scope,$http,applicationService,$location,$route) {
      
      var model = 'clientes';
      angular.$locationat = $location; 
      angular.$routeat = $route; 


      function editableTable($location) {
          
          if(oTable_clientes != null) oTable_clientes.fnDestroy();
          var oTable_clientes = $('#table-editable').dataTable({
              "ajax": 'api/get_all_cliente_tipo/'+model+'/cliente',
              "pageLength": 25,
              
              columns: [
                  {data: "name" },
                  {data: "estado" },
                  {data : "score"},
                  {data : "giro"},
                  {data : "opciones"}
                  ],
              "bDestroy": true,
              'processing': true,

              "searching": true,
              "columnDefs": [
                { className: "clickeable", "targets": [ 0 ] }
              ]
          });
          $('#table-editable a.delete').live('click', function (e) {
              e.preventDefault();
              if (confirm("Estas seguro que quieres eliminarlo?") == false) {
                  return;
              }

              var id = $(this).data('id');
              var nRow = $(this).parents('tr')[0];

              $http.get('api/del/'+model+'/'+id).then(successCallback, errorCallback);
              function successCallback(){

                  
                  oTable_clientes.fnDeleteRow(nRow);
              }
              function errorCallback(){

                  
                  alert('Hubo un error al borrar el registro!');
              }
              
              
              // alert("Deleted! Do not forget to do some ajax to sync with backend :)");
          });
          
          $('#table-editable tbody').on( 'click', '.sorting_1', function () {
              var id = $(this).parent('tr').attr('id').replace("row_", "");;
              
              angular.$locationat.path('/verclientes/'+id).search('id='+id);
              angular.$routeat.reload();
          } );

          $('.create').live('click', function (e) {
                applicationService.create($(this),oTable_clientes);
                
          });
          $('.dataTables_filter input').attr("placeholder", "Buscar un cliente...");

      };

      editableTable();

      $scope.$on('$destroy', function () {
          $('table').each(function () {
              if ($.fn.dataTable.isDataTable($(this))) {
                  $(this).dataTable({
                      "bDestroy": true
                  }).fnDestroy();
              }
          });
      });
  }]);
