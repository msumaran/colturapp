angular.module('newApp').controller('loginCtrl',
['$scope', 'applicationService', 'builderService', 'pluginsService', '$location', '$route', '$rootScope','$http',
function ($scope, applicationService, builderService, pluginsService, $location, $route, $rootScope, $http) {
    console.log('loginCtrl');
    angular.$locationat = $location;
    angular.$routeat = $route;

    $rootScope.title = '';

    $(document).ready(function () {
        applicationService.init();
        builderService.init();
        pluginsService.init();
    });

    $scope.login = function(dni){

        $http.get('apiapp/login/'+$scope.dni).then(function(result){
            console.log('LOGIN APIAPP');
            if(result.data == "null" ){

                alert('Participante no registrado!');
                return false;
            }

            console.log('after login',result.data);
            //TEST
            window.location.href = "web/intro";
            //PRODUCCIÓN
            //window.location.href = "/web/intro";
        });

    }
    $scope.seguir = function (){

        //TEST
        window.location.href = "web/#/";
        //PRODUCCIÓN
        //window.location.href = "/web/#/";
    }
    $scope.isActive = function (viewLocation) {
        applicationService.resetStyle( $rootScope.credentials );
        return viewLocation === $location.path();
    };

}]);


angular.module('newApp').controller('introCtrl',
    ['$scope', 'applicationService', 'builderService', 'pluginsService', '$location', '$route', '$rootScope','$http',
        function ($scope, applicationService, builderService, pluginsService, $location, $route, $rootScope, $http) {
            console.log('introCtrl');
            angular.$locationat = $location;
            angular.$routeat = $route;

            $rootScope.title = ' ';
            $rootScope.credentials = {};

            $('.intro, .int').hide();

            setTimeout(function(){
                $('div.intro').css('background-color','#333');
                $('.intro, .int').show();
            }, 3000);

            $(document).ready(function () {
                applicationService.init();
                builderService.init();
                pluginsService.init();
            });

            $rootScope.verifilogin = function(callback){


                $http.get('apiapp/login/').then(function(result){
                    if(result.data == "null" ){

                        window.location.href = "/";
                        return false;
                    }

                    $rootScope.credentials = result.data;
                    console.log('intro login callback', $rootScope.credentials);
                    applicationService.resetStyle( $rootScope.credentials );
                    callback();
                });
            };

        }]);


angular.module('newApp').controller('mainCtrl',
['$scope', 'applicationService', 'builderService', 'pluginsService', '$location', '$route', '$rootScope','$http',
function ($scope, applicationService, builderService, pluginsService, $location, $route, $rootScope, $http) {
    console.log('mainCtrl');
    angular.$locationat = $location;
    angular.$routeat = $route;

    $rootScope.title = ' ';
    $rootScope.credentials = {};

    console.log('esta vacio?', $rootScope.credentials);

    $(document).ready(function () {
        applicationService.init();
        builderService.init();
        pluginsService.init();
    });

    $rootScope.verifilogin = function(callback){


        $http.get('apiapp/login/').then(function(result){
            if(result.data == "null" ){

                window.location.href = "/";

                return false;

            }

            $rootScope.credentials = result.data;

            console.log('main login callback', $rootScope.credentials);
            applicationService.resetStyle( $rootScope.credentials );


            callback();
        });
    };

    console.log('esta lleno?', $rootScope.credentials);

    $scope.local = function (){
        console.log('local¿?');
    }
    $rootScope.verifilogin($scope.local);

    $scope.seguir = function (){

        //TEST
        window.location.href = "web/#/";
        //PRODUCCIÓN
        //window.location.href = "/web/#/";
    }
    $scope.isActive = function (viewLocation) {

        return viewLocation === $location.path();
    };

}]);

angular.module('newApp')
.controller('inicioCtrl', ['$scope', '$http', 'applicationService','$location','$route', '$rootScope',
function ($scope,$http,applicationService,$location,$route, $rootScope) {
    $scope.init = function () {
        angular.$locationat = $location;
        angular.$routeat = $route;
        $rootScope.title = '';
        console.log('inicioCtrl');
    };
    $rootScope.verifilogin($scope.init);
}]);

angular.module('newApp')
.controller('destinoCtrl', ['$scope', '$http', 'applicationService','$location','$route', '$rootScope',
function ($scope,$http,applicationService,$location,$route, $rootScope) {
    $scope.init = function () {
        angular.$locationat = $location;
        angular.$routeat = $route;
        $rootScope.title = 'Destino';
        console.log('destinoCtrl');
        $http.get('apiapp/get_destinos/'+$rootScope.credentials.viajes_id).then(function(result){

            var resultado = []; // my object
            if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

            }
            $scope.destinos = resultado;
            //window.location.href = "web/intro";
        });
    };
    $rootScope.verifilogin($scope.init);
}]);


angular.module('newApp')
.controller('hotelCtrl', ['$scope', '$http', 'applicationService','$location','$route', '$rootScope',
function ($scope,$http,applicationService,$location,$route, $rootScope) {
    $scope.init = function () {
        angular.$locationat = $location;
        angular.$routeat = $route;
        $rootScope.title = 'Hotel';
        console.log('hotelCtrl');
        $http.get('apiapp/get_hotel/'+$rootScope.credentials.viajes_id).then(function(result){

            var resultado = []; // my object
            if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

            }
            $scope.hotel = resultado;
            //window.location.href = "web/intro";
        });



    };
    $rootScope.verifilogin($scope.init);
}]);


angular.module('newApp')
.controller('vuelosCtrl', ['$scope', '$http', 'applicationService','$location','$route', '$rootScope',
function ($scope,$http,applicationService,$location,$route, $rootScope) {
    $scope.init = function () {
        angular.$locationat = $location;
        angular.$routeat = $route;
        $rootScope.title = 'Vuelos';




        console.log('vuelosCtrl');

        var resultado = []; // my object

        $http.get('apiapp/get_vuelos_tipo/'+$rootScope.credentials.viajes_id+'/ida').then(function(result){
            console.log(result.data.data);
            $.each(result.data.data, function (key, data) {
                data.fecha_text = moment(data.salida_hora, 'MM/DD/YYYY hh:mm', 'es').format("LL");
                data.salida_hora = moment(data.salida_hora, 'MM/DD/YYYY hh:mm', 'es').format("HH:mm");
                data.llegada_hora = moment(data.llegada_hora, 'MM/DD/YYYY hh:mm', 'es').format("HH:mm");
                resultado.push(data);
            });
            $scope.vuelo_ida = resultado;
        });




        var resultado2 = [];
        $http.get('apiapp/get_vuelos_tipo/'+$rootScope.credentials.viajes_id+'/vuelta').then(function(result){

            $.each(result.data.data, function (key, data) {
                data.fecha_text = moment(data.salida_hora, 'MM/DD/YYYY hh:mm', 'es').format("LL");
                data.salida_hora = moment(data.salida_hora, 'MM/DD/YYYY hh:mm', 'es').format("HH:mm");
                data.llegada_hora = moment(data.llegada_hora, 'MM/DD/YYYY hh:mm', 'es').format("HH:mm");
                resultado2.push(data);
            });
            $scope.vuelo_vuelta = resultado2;
        });


    };
    $rootScope.verifilogin($scope.init);



}]);




angular.module('newApp')
.controller('agendaCtrl', ['$scope', '$http', 'applicationService','$location','$route', '$rootScope',
function ($scope,$http,applicationService,$location,$route, $rootScope) {


    $scope.init = function () {
        angular.$locationat = $location;
        angular.$routeat = $route;
        $rootScope.title = 'Agenda';
        console.log('agendaCtrl');

        $http.get('apiapp/get_agenda_total/'+$rootScope.credentials.viajes_id).then(function(result){
            console.log(result);
            var resultado = []; // my object
            if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    if( data.titulo != ''){

                        $("#titulo").html(data.titulo);
                        $("#titulo_fecha").html(data.titulo_fecha);
                    }

                    resultado.push(data);
                });

            }
            console.log(resultado);
            $scope.agenda = resultado;
            //window.location.href = "web/intro";
        });
        $('#listdiv').mCustomScrollbar({
            scrollButtons: {
                enable: false
            },
            autoHideScrollbar: false,
            scrollInertia: 150,
            theme: "amarillo",
            set_height: 350,
            advanced: {
                updateOnContentResize: true
            }
        });

    };
    $rootScope.verifilogin($scope.init);
}]);

angular.module('newApp')
.controller('participantesCtrl', ['$scope', '$http', 'applicationService','$location','$route', '$rootScope',
function ($scope,$http,applicationService,$location,$route, $rootScope) {

    $scope.init = function () {
        angular.$locationat = $location;
        angular.$routeat = $route;
        $rootScope.title = 'Participantes';
        var login = $rootScope.credentials;
        console.log(login);
        $http.get('apiapp/get_participantes/'+$rootScope.credentials.viajes_id).then(function(result){

            var resultado = []; // my object
            if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

            }
            $scope.participantes = resultado;
            //window.location.href = "web/intro";
        });

    };
    $rootScope.verifilogin($scope.init);
    $scope.$watch('$viewContentLoaded', function(){

        applicationService.resetStyle($rootScope.credentials);
    });
}]);

angular.module('newApp')
.controller('recomendacionesCtrl', ['$scope', '$http', 'applicationService','$location','$route', '$rootScope',
function ($scope,$http,applicationService,$location,$route, $rootScope) {
    $scope.init = function () {
        angular.$locationat = $location;
        angular.$routeat = $route;
        $scope.theme = 'amarillo';
        $rootScope.title = 'Recomendaciones';
        console.log('recomendacionesCtrl');
        $(window).load(function(){
            console.log('');
        });
        $http.get('apiapp/get_recomendaciones/'+$rootScope.credentials.viajes_id).then(function(result){

            var resultado = []; // my object
            if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

            }
            $scope.recomendaciones = resultado;

            $("#listdivrecomendaciones").mCustomScrollbar("scrollTo","h2:last");
        });
    };
    $rootScope.verifilogin($scope.init);

}]);

angular.module('newApp')
.controller('emergenciaCtrl', ['$scope', '$http', 'applicationService','$location','$route', '$rootScope',
function ($scope,$http,applicationService,$location,$route, $rootScope) {
    $scope.init = function () {
        angular.$locationat = $location;
        angular.$routeat = $route;
        $rootScope.title = 'Emergencia';
        console.log('emergenciaCtrl');
        $http.get('apiapp/get_emergencia/'+$rootScope.credentials.viajes_id).then(function(result){

            var resultado = []; // my object
            if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

            }
            $scope.emergencia = resultado;
            //window.location.href = "web/intro";
        });
    };
    $rootScope.verifilogin($scope.init);
}]);



angular.module('newApp')
.controller('imagenesCtrl', ['$scope', '$http', 'applicationService','$location','$route', '$rootScope',
function ($scope,$http,applicationService,$location,$route, $rootScope) {

    $scope.init = function () {
        angular.$locationat = $location;
        angular.$routeat = $route;
        $rootScope.title = 'Imágenes';
        console.log('imagenesCtrl');
        $http.get('apiapp/get_imagenes/'+$rootScope.credentials.viajes_id).then(function(result){

            var resultado = []; // my object
            if( result.data.data != undefined ){

                $.each(result.data.data, function (key, data) {
                    resultado.push(data);
                });

            }
            $scope.imagenes = resultado;
            //window.location.href = "web/intro";
        });
    };
    $rootScope.verifilogin($scope.init);
}]);
