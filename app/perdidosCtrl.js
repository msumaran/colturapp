'use strict';
angular.module('newApp')
  .controller('perdidosCtrl', ['$scope', '$http', 'applicationService','$location','$route', function ($scope,$http,applicationService,$location,$route) {
      
      var model = 'clientes';
      angular.$locationat = $location; 
      angular.$routeat = $route; 


      function editableTable($location) {
          var $input_id = $("#modal_estado_perdidos input[name='id']");
          var $select_estado = $("#select_estado_perdidos");
          var $modal_estado = $("#modal_estado_perdidos");
          
          if(oTable_perdidos != null) oTable_perdidos.fnDestroy();
          jQuery.fn.dataTableExt.oApi.fnReloadAjax = function ( oSettings, sNewSource, fnCallback, bStandingRedraw )
          {
              if ( jQuery.fn.dataTable.versionCheck ) {
                  var api = new jQuery.fn.dataTable.Api( oSettings );
           
                  if ( sNewSource ) {
                      api.ajax.url( sNewSource ).load( fnCallback, !bStandingRedraw );
                  }
                  else {
                      api.ajax.reload( fnCallback, !bStandingRedraw );
                  }
                  return;
              }
           
              if ( sNewSource !== undefined && sNewSource !== null ) {
                  oSettings.sAjaxSource = sNewSource;
              }
           
              // Server-side processing should just call fnDraw
              if ( oSettings.oFeatures.bServerSide ) {
                  this.fnDraw();
                  return;
              }
           
              this.oApi._fnProcessingDisplay( oSettings, true );
              var that = this;
              var iStart = oSettings._iDisplayStart;
              var aData = [];
           
              this.oApi._fnServerParams( oSettings, aData );
           
              oSettings.fnServerData.call( oSettings.oInstance, oSettings.sAjaxSource, aData, function(json) {
                  /* Clear the old information from the table */
                  that.oApi._fnClearTable( oSettings );
           
                  /* Got the data - add it to the table */
                  var aData =  (oSettings.sAjaxDataProp !== "") ?
                      that.oApi._fnGetObjectDataFn( oSettings.sAjaxDataProp )( json ) : json;
           
                  for ( var i=0 ; i<aData.length ; i++ )
                  {
                      that.oApi._fnAddData( oSettings, aData[i] );
                  }
           
                  oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
           
                  that.fnDraw();
           
                  if ( bStandingRedraw === true )
                  {
                      oSettings._iDisplayStart = iStart;
                      that.oApi._fnCalculateEnd( oSettings );
                      that.fnDraw( false );
                  }
           
                  that.oApi._fnProcessingDisplay( oSettings, false );
           
                  /* Callback user function - for event handlers etc */
                  if ( typeof fnCallback == 'function' && fnCallback !== null )
                  {
                      fnCallback( oSettings );
                  }
              }, oSettings );
          };
          console.log('entro');
          var oTable_perdidos = $('#table-perdidos').dataTable({
              "ajax": 'api/get_all_cliente_estado/'+model+'/Perdido',
              "pageLength": 25,
              columns: [
                  {data: "name" },
                  {data: "estado" },
                  {data : "score"},
                  {data : "giro"},
                  {data : "opciones"}
                  ],
              "bDestroy": true,
              "destroy":true,
              "bStateSave": true,
              "bProcessing": true,
              'processing': true,
              fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                  
                  $('td', nRow).on('click', function() {


                        var row   = $(this).index();
                        console.log(row);
                        switch(row){

                          case 1:

                              $select_estado.val(aData.estado).change();
                              $modal_estado.modal('show');
                              $input_id.val(aData.id);
                          break;
                          case 2:
                              var obj = {
                                id: aData.id,
                                name: aData.name
                              }
                              angular.$locationat.path('/calculadoralead').search(obj);
                              angular.$routeat.reload();
                          break;

                        }
                       
                        

                        
                    });
              },
              "searching": true,
              "columnDefs": [
                { className: "clickeable", "targets": [ 0,1 ] }
              ]
          });
          $('#btn_change_estado').live('click', function (e) {
              e.preventDefault();
              var id = $input_id.val();
              $http.post('api/save/'+model+'/'+id,  {estado: $select_estado.val() } ).then(successCallback, errorCallback);
              function successCallback(){
                oTable_perdidos.fnReloadAjax(null, null, true);

                $modal_estado.modal('hide');
                  
                
              }
              function errorCallback(){
                  alert('Hubo un error al actualizar el registro!');
              }


          });
          $('#table-perdidos a.delete').live('click', function (e) {
              e.preventDefault();
              if (confirm("Estas seguro que quieres eliminarlo?") == false) {
                  return;
              }

              var id = $(this).data('id');
              var nRow = $(this).parents('tr')[0];

              $http.get('api/del/'+model+'/'+id).then(successCallback, errorCallback);
              function successCallback(){

                  
                  oTable_perdidos.fnDeleteRow(nRow);
              }
              function errorCallback(){
                  alert('Hubo un error al borrar el registro!');
              }
              
              
              // alert("Deleted! Do not forget to do some ajax to sync with backend :)");
          });
          
          $('#table-perdidos tbody').on( 'click', '.sorting_1', function () {
              var id = $(this).parent('tr').attr('id').replace("row_", "");;
              
              angular.$locationat.path('/verclientes/'+id).search('id='+id);
              angular.$routeat.reload();
          } );

          $('.create').live('click', function (e) {
                applicationService.create($(this),oTable_perdidos);
                
          });
          $('.dataTables_filter input').attr("placeholder", "Buscar un cliente...");

      };

      editableTable();

      $scope.$on('$destroy', function () {
          $('table').each(function () {
              if ($.fn.dataTable.isDataTable($(this))) {
                  $(this).dataTable({
                      "bDestroy": true
                  }).fnDestroy();
              }
          });
      });
  }]);
