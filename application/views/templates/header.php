<!doctype html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="admin-themes-lab">
        <meta name="author" content="themes-lab">
        <link rel="shortcut icon" href="assets/global/images/favicon.png" type="image/png">
        <title>Administrador</title>
        <base href="<?php echo base_url(); ?>" />
        <link href="assets/global/css/style.css?v=<?php echo $v ?>" rel="stylesheet">
        <link href="assets/global/css/theme.css?v=<?php echo $v ?>" rel="stylesheet">
        <link href="assets/global/css/ui.css?v=<?php echo $v ?>" rel="stylesheet">
        <link href="assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">
        <link href="assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">
        <link href="assets/global/plugins/slick/slick.css" rel="stylesheet">
        <link href="assets/global/plugins/icheck/skins/all.css" rel="stylesheet">
        <link href="assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
        <link href="assets/global/plugins/rateit/rateit.css" rel="stylesheet" />
        <link href="assets/global/plugins/colorpicker/spectrum.css?v=<?php echo $v ?>" rel="stylesheet" />
        <link href="assets/global/plugins/step-form-wizard/css/step-form-wizard.css" rel="stylesheet" />
        <link href="assets/global/plugins/step-form-wizard/plugins/parsley/parsley.css" rel="stylesheet" />
        <link href="assets/global/plugins/ion-slider/ion.rangeSlider.css" rel="stylesheet" />
        <link href="assets/global/plugins/ion-slider/style.css" rel="stylesheet" />
        <link href="assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/cke-editor/skins/bootstrapck/editor.css" rel="stylesheet" />
        <link href="assets/global/plugins/summernote/summernote.css" rel="stylesheet" />
        <link href="assets/global/plugins/cropper/cropper.css" rel="stylesheet" />
        <link href="assets/global/plugins/magnific/magnific-popup.css" rel="stylesheet" />
        <link href="assets/global/plugins/hover-effects/hover-effects.min.css" rel="stylesheet">
        <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/prettify/prettify.css" rel="stylesheet" />
        <link href="assets/global/plugins/jstree/dist/themes/default/style.css" rel="stylesheet" />
        <link href="assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/input-text/style.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/font-awesome-animation/font-awesome-animation.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/uploadify/uploadify.css" rel="stylesheet" />

        <!--
        <link href="assets/global/plugins/bootstrap3-wysihtml5/dist/bootstrap3-wysihtml5.min.css" rel="stylesheet" />
        <link href='assets/global/plugins/froala_editor/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
        <link href='assets/global/plugins/froala_editor/css/froala_style.min.css' rel='stylesheet' type='text/css' />-->


        <!-- BEGIN ANGULARJS STYLE -->
        <link href="css/angular-theme.css" rel="stylesheet">
        <!-- END ANGULARJS STYLE -->
        <!-- BEGIN LAYOUT STYLE -->
        <link href="assets/admin/layout1/css/layout.css?v=<?php echo $v ?>" rel="stylesheet">
        <!-- END LAYOUT STYLE -->
        <script src="assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body ng-app="newApp" class="fixed-topbar fixed-sidebar theme-sdtl color-default" ng-controller="mainCtrl">
