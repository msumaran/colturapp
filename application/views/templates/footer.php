        </section>


        <div id="morphsearch" class="morphsearch">
            <form class="morphsearch-form">
                <input class="morphsearch-input" id="searchinput" type="search" placeholder="Buscar..." />
                <button class="morphsearch-submit" type="submit">Search</button>
            </form>
            <div class="morphsearch-content withScroll">
                <div class="dummy-column user-column">
                    <h2>Clientes</h2>
                    <div id="search_clientes" ng-repeat="val in search_result">

                    <a class="dummy-media-object" href="master/#verclientes/{{val.id}}">

                        <h3><i class="fa fa-star"></i> {{val.name}}</h3>
                    </a>
                    </div>
                </div>
                <div class="dummy-column user-column">
                    <h2>Contactos</h2>
                    <div id="search_contactos" ng-repeat="val in search_result_contactos">

                    <a class="dummy-media-object" href="master/#verclientes/{{val.clientes_id}}">

                        <h3><i class="fa fa-user"></i> {{val.contacto}}</h3>
                    </a>
                    </div>
                </div>
            </div>
            <span class="morphsearch-close"></span>
        </div>
        <div class="loader-overlay">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>



        <script src="plugins/angular/angular.js"></script>
        <script src="plugins/json3/lib/json3.js"></script>
        <script src="plugins/angular-resource/angular-resource.js"></script>
        <script src="plugins/angular-cookies/angular-cookies.js"></script>
        <script src="plugins/angular-sanitize/angular-sanitize.js"></script>
        <script src="plugins/angular-animate/angular-animate.js"></script>
        <script src="plugins/angular-touch/angular-touch.js"></script>
        <script src="plugins/angular-route/angular-route.js"></script>
        <script src="plugins/angular-bootstrap/ui-bootstrap-tpls-0.12.1.js"></script>
        <script src="app/ng_excel.js?<?php echo $v; ?>"></script>
        <script src="app/app.js?<?php echo $v; ?>"></script>
        <script src="directives/ngViewClass.js?v=<?php echo $v ?>"></script>

        <script src="assets/web/plugins/xlsx/xlsx.js"></script>
        <script src="assets/web/plugins/xlsx/jszip.js"></script>
        <script src="assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
        <script src="assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script>
        <script src="assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script>
        <script src="assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/global/plugins/retina/retina.min.js"></script>
        <script src="assets/global/plugins/select2/select2.min.js"></script>
        <script src="assets/global/plugins/icheck/icheck.min.js"></script>
        <script src="assets/global/plugins/dropzone/dropzone.min.js"></script>
        <script src="assets/global/plugins/uploadify/jquery.uploadify.min.js?v1"></script>
        <script src="assets/global/js/sidebar_hover.js"></script>
        <script src="assets/global/js/widgets/notes.js"></script>
        <script src="assets/global/js/pages/search.js?<?php echo $v; ?>"></script>
        <script src="assets/global/js/jquery.uploadifive.min.js"></script>

        <script src="assets/global/plugins/quickSearch/quicksearch.js"></script>
        <script src="assets/global/plugins/icheck/icheck.js"></script>
        <script src="assets/global/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="assets/global/plugins/jquery-validation/jquery.validate.js"></script>
        <script src="assets/global/plugins/bootstrap-slider/bootstrap-slider.js"></script>
        <script src="assets/global/plugins/ion-slider/ion.rangeSlider.js"></script>
        <script src="assets/global/plugins/bootstrap/js/jasny-bootstrap.js"></script>
        <script src="assets/global/plugins/moment/moment.min.js"></script>
        <script src="assets/global/plugins/maps-amcharts/ammap/ammap.min.js"></script>
        <script src="assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.min.js"></script>
        <script src="assets/global/plugins/maps-amcharts/ammap/themes/black.min.js"></script>

        <!--
        <script type='text/javascript' src='assets/global/plugins/froala_editor/js/froala_editor.min.js'></script>-->
        <script type='text/javascript' src='assets/global/plugins/cke-editor/ckeditor.js'></script>

        <script src="assets/global/plugins/maps-amcharts/ammap/ammap_amcharts_extension.js"></script>
        <script src="assets/global/plugins/maps-amcharts/ammap/maps/js/continentsLow.js"></script>
        <script src="assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.js"></script>
        <script src="assets/global/plugins/maps-amcharts/ammap/maps/js/usaLow.js"></script>
        <script src="assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/global/plugins/typed/typed.js"></script>
        <script src="assets/global/plugins/colorpicker/spectrum.js"></script>
        <script src="assets/global/plugins/summernote/summernote.js"></script>
        <script src="assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>
        <script src="assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script>
        <script src="assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script>
        <script src="assets/global/plugins/multidatepicker/multidatespicker.min.js"></script>
        <script src="assets/global/js/widgets/todo_list.js"></script>
        <script src="assets/global/plugins/charts-chartjs/Chart.min.js"></script>
        <script src="assets/global/plugins/charts-highstock/js/highstock.min.js"></script>
        <script src="assets/global/plugins/charts-highstock/js/modules/exporting.min.js"></script>
        <script src="assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.js"></script>
        <script src="assets/global/plugins/charts-highstock/js/highcharts-more.min.js"></script>
        <script src="assets/global/plugins/charts-highstock/js/modules/exporting.min.js"></script>
        <script src="assets/global/plugins/autosize/autosize.min.js"></script>
        <script src="assets/global/plugins/metrojs/metrojs.min.js"></script>
        <script src="js/pages/dashboard.js"></script>
        <script src="js/builder.js"></script>
        <script src="js/pages/layouts_api.js"></script>
        <script src="js/application.js?<?php echo $v; ?>"></script>
        <script src="js/plugins.js?<?php echo $v; ?>"></script>
        <script src="js/quickview.js"></script>
        <script src="app/mainCtrl.js"></script>
        <script src="app/dashboard/dashboardCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/verclientesCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/viajesCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/contactosCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/perdidosCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/empresasCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/eventoslistaCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/eventostipoCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/briefCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/prospectosCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/visitasCtrl.js?<?php echo $v; ?>"></script>
        <script src="app/reportesCtrl.js?<?php echo $v; ?>"></script>
    </body>
</html>
