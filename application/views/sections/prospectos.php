<div>
    <div class="header">
        <h2>Prospectos</h2>
        
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="master/#">Home</a>
                </li>
                <li>
                    <a href="master/#prospectos">Prospectos</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header panel-controls">
                    <h3><i class="fa fa-table"></i> <strong>Lista de</strong> Prospectos</h3>
                </div>
                <div class="panel-content">
                    <!--<p>Lista de eventos</p>-->
                    <div class="m-b-20">
                        <div class="btn-group">
                            <button class="btn btn-sm btn-dark create" data-model="clientes"><i class="fa fa-plus"></i> Nuevo prospectos</button>
                        </div>
                    </div>
                    <table class="table table-hover dataTable" id="table-prospectos">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Estado</th>
                                <th>Lead Score</th>
                                <th>Giro</th>
                                <th class="text-right">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
    <!-- Inicio modal -->
    <div class="modal fade" id="modal_estado_prospectos" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cambiar estado</h4>
              </div>
              <div class="modal-body">
                <p>
                    <?php echo form_dropdown('estado', $row->estados_ar, '',' id="select_estado_prospectos" '); ?>
                    <?php echo form_hidden('id', ''); ?>
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn_change_estado" class="btn btn-primary">Guardar cambios</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


    <?php $this->load->view('templates/footer_int'); ?>
</div>
