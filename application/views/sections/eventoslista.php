<div>
    <div class="header">
        <h2>Lista de <strong>Eventos</strong></h2>
        <!---
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="dashboard.html">Make</a>
                </li>
                <li>
                    <a href="#">Medias</a>
                </li>
                <li class="active">Gallery Sortable</li>
            </ol>
        </div>
        -->
    </div>
    <p class="m-t-10 m-b-20 f-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel panel-transparent">
                <div class="panel-content">
                    <div class="portfolioFilter">
                        <?php

                            echo '<a href="#" data-filter="*" class="current">Todos</a>';
                            foreach ($eventostipo as $row) {
                                echo '<a href="#" data-filter=".eventostipo'.$row->id.'" class="current">'.$row->name.'</a>';

                            }
                        ?>
                    </div>
                    <div class="portfolioContainer grid">

                        <?php


                            foreach ($eventos as $row) {
                                echo '<figure class="eventostipo'.$row->eventos_tipo_id.' effect-zoe magnific" data-mfp-src="assets/global/images/gallery/'.rand(1,9).'.jpg">
                            <img src="assets/global/images/gallery/'.rand(1,9).'.jpg" alt="9" />
                            <figcaption>
                                <h2>'.$row->name.'</h2>
                                <i class="fa fa-heart"></i>
                                <i class="fa fa-eye"></i>
                                <i class="fa fa-paperclip"></i>
                                <p>Descripción corta del evento <button class="btn btn-sm btn-blue create" data-model="eventos"><i class="fa fa-plus"></i> Unirse a evento</button></p>
                            </figcaption>
                        </figure>';
                            }
                            

                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('templates/footer_int'); ?>
</div>
