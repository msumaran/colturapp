<div>
    <div class="header">
        <h2><strong>{{content.name}}</strong></h2>
  
    </div>

    <div class="row">
        <div class="col-md-12">
              <div class="panel">
              <div class="panel-content">
            <div class="skin skin-square">
        
                <form role="form" id="form_lead">
                    <div class="form-body">
                        
                        <div class="form-group">

                     
                            
                            <tabset class="tab-fade-in">
                                <!--TODO: add nav-primary to ul-->
                                <tab select="changeHash(tab.href)" active="isTabActive">
                                    <tab-heading>
                                        Datos
                                    </tab-heading>
                                    <div>
                                        <div class="row column-seperation">
                                            <div class="col-md-6 line-separator">
                                                
                                                <p><strong>Dirección:</strong> {{content.direccion}}</p>
                                                <p><strong>Teléfono:</strong> {{content.telefonos}}</p>
                                                <p><strong>Tipo:</strong> {{content.tipo}}</p>
                                                <p><strong>Estado:</strong> {{content.estado}}</p>
                                                <p><strong>Web:</strong> {{content.web}}</p>
                                                
                                            </div>
                                            <div class="col-md-6">
                                                  <p><strong>Lead Score:</strong> {{content.lead_score}}</p>
                                                <p><strong>Lead Afinidad:</strong> {{content.lead_afinidad}}</p>
                                                <p><strong>Score:</strong> {{content.score}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </tab>
                                <tab select="changeHash(tab.href)">
                                    <tab-heading>
                                        Archivos
                                    </tab-heading>
                                    <ul> 
                                        <li ng-repeat="val in clientes_brief"> {{val.name}}: <input class="form-control form-white" type="text" name="t" value="{{val.input}}" /></li>

                                    </ul>
                                </tab>


                                <tab select="changeHash(tab.href)">
                                    <tab-heading>
                                        Cotizaciones
                                    </tab-heading>
                                    <table class="table table-hover dataTable">
                                        <thead>
                                            
                                            <tr>
                                                <th>Proyecto</th>
                                                <th>Fecha</th>
                                                <th>Enviado</th>
                                                <th>Monto</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="val in cotizacion"> <td>{{val.proyectos}}</td> <td>{{val.creado}}</td>  <td>{{val.enviado}}</td> <td>S/. {{val.totalsigv}}</td> </tr>
                                        </tbody>
                                    </table>
                                </tab>

                                <tab select="changeHash(tab.href)">
                                    <tab-heading>
                                        Contactos
                                    </tab-heading>
                                    <button data-model="clientes_contacto" class="btn btn-xs btn-primary create_contacto">Agregar contacto</button>
                                    <table class="table table-hover dataTable">
                                        <thead>
                                            
                                            <tr>
                                                <th>Contacto</th>
                                                <th>Cargo</th>
                                                <th>Email</th>
                                                <th>Teléfono</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="val in contactos"> <td>{{val.contacto}}</td> <td>{{val.email}}</td> <td> {{val.email}}</td> <td> {{val.telefono}}</td> </tr>
                                        </tbody>
                                    </table>
                                </tab>
                                
                            </tabset>
                            


                    </div>
                </form>
            </div>
            </div><!-- Div panel content -->
            </div><!-- Div panel -->
        </div><!-- Div col-md-12 -->
    </div>
    <?php $this->load->view('templates/footer_int'); ?>
</div>
