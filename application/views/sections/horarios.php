<div>
    <div class="header">
        <h2>{{name_controller}}</h2>
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="master/#">Home</a>
                </li>
                <li>
                    <a href="master/#{{model}}">{{model}}</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
           
            <div class="panel">
                <div class="panel-header panel-controls">
                    <h3><i class="fa fa-table"></i> <strong>Lista de</strong> {{name_controller}}</h3>
                </div>
                <div class="panel-content">
                    <!--<p>Lista de eventos</p>-->
                    <div class="m-b-20">
                        <div class="btn-group">
                            <button class="btn btn-sm btn-dark" ng-click="create()"><i class="fa fa-plus"></i> Nuevo {{model}}</button>
                        </div>
                    </div>
                    <table class="table table-hover dataTable" id="table-horarios">
                        <thead>
                            <tr>
                                <th>Tipo</th>
                                <th>Hora</th>
                                <th>Hora ultimo</th>
                                <th>Descripción</th>
                                <th class="text-right">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="val in horarios"> 
                                <td>{{val.tipo}}</td>
                                <td>{{val.fecha_hora}}</td>
                                <td>{{val.hora}}</td>
                                <td ng-bind-html="val.descp"></td>
                                <td>
                                    <a class="btn btn-sm btn-default" ng-click="editar({{val.id}})" href="javascript:;"><i class="icon-note"></i></a>

                                    <a class="btn btn-sm btn-danger" ng-click="borrar({{val.id}})" href="javascript:;"> <i class="icons-office-52"></i></a>
                                </td> 
                            </tr>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('templates/footer_int'); ?>
</div>
