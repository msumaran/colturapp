<div>
    <div class="header">
        <h2>{{name_controller}}</h2>
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="master/#">Home</a>
                </li>
                <li>
                    <a href="master/#{{model}}">{{model}}</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel" ng-show="participantesselect">
                <div class="panel-content">
                    <div class="form">
                    <?php

                        echo $this->form_builder->open_form(array('action' => '#','ng-submit'=>"\$event.preventDefault();elegir()"));
                        echo $this->form_builder->build_form_horizontal(array(
                           array(
                                'id' => 'viajes_id_select_participantes',
                                'class'=>'viajes_id_select',
                                'ng-model'=>'viajes_id',
                                'label'=>'Elegir viaje',
                                'value' =>''
                            ),
                            
                            array(
                                'id' => 'Elegir',
                                'ng-click'=>'elegir()',
                                'value'=>'Elegir',
                                'type' => 'submit'
                            )
                        ));
                       echo $this->form_builder->close_form();



                    ?>
                    </div>
                </div>
            </div>
            <div class="panel" ng-show="participantesdiv">
                <div class="panel-header panel-controls">
                    <h3><i class="fa fa-table"></i> <strong>Lista de</strong> {{name_controller}}</h3>
                </div>
                <div class="panel-content">
                    <!--<p>Lista de eventos</p>-->
                    <div class="m-b-20">
                        <div class="btn-group">
                            <button class="btn btn-sm btn-dark" ng-click="create()"><i class="fa fa-plus"></i> Nuevo {{model}}</button>
                        </div>
                    </div>

                      <label>Trama:</label> <input type="file" id="pero" /> 
                      <br />
                      <button class="btn btn-sm btn-dark" ng-click="subirtrama()"><i class="fa fa-plus"></i> Subir Trama</button>
                         <div id="drop"></div>
                      <div ng-model="tramamsg"></div>


                    <table class="table table-hover dataTable" id="table-empresas">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>DNI</th>
                                <th>Correo</th>
                                <th class="text-right">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="val in hoteles" ng-bind-html-unsafe="val.imagen"> 
                                <td>{{val.name}} {{val.lastname}}</td> 
                                <td>{{val.dni}}</td>  
                                <td>{{val.email}}</td> 
                                <td>
                                    <a class="btn btn-sm btn-default" ng-click="editar({{val.id}})" href="javascript:;"><i class="icon-note"></i></a>

                                    <a class="btn btn-sm btn-danger" ng-click="borrar({{val.id}})" href="javascript:;"> <i class="icons-office-52"></i></a>
                                </td> 
                            </tr>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('templates/footer_int'); ?>
</div>
