<div class="row">
    <div class="col-sm-6">
        <h3><strong>Contactos</strong> por mes</h3>
        <p>
           Comparativo de contactos vs el mes del año anterior
        </p>
        <canvas id="line-chart" class="full" height="140"></canvas>
    </div>
    
    <div class="col-sm-6">
        <h3><strong>Visitas</strong> por mes</h3>
        <p>
            Numero de visitas mensuales
        </p>
        <canvas id="line-visitas" class="full" height="140"></canvas>
    </div>
    
</div>
<!---
<div class="row m-t-40">
    <div class="col-sm-6">
        <h3><strong>Pie &amp; Donut</strong> Charts</h3>
        <p>
            Pie and doughnut charts are probably the most commonly used chart there are. <br>
            They are divided into segments, the arc of each segment shows the proportional value of each piece of data.
        </p>
        <div class="row">
            <div class="col-md-6">
                <canvas id="pie-chart"></canvas>
            </div>
            <div class="col-md-6">
                <canvas id="pie-chart2"></canvas>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <h3><strong>Polar</strong> Charts</h3>
        <p>Polar area charts are similar to pie charts, but each segment has the same angle - the radius of the segment differs depending on the value.</p>
        <div class="row m-b-60">
            <div class="col-md-6">
                <canvas id="polar-chart"></canvas>
            </div>
            <div class="col-md-6">
                <canvas id="polar-chart2"></canvas>
            </div>
        </div>
    </div>
</div>
-->
<div class="footer">
    <div class="copyright">
        <p class="pull-left sm-pull-reset">
            <span>Copyright <span class="copyright">&copy;</span> <?php echo date('Y'); ?> </span>
            <span>Nativos Digitales</span>.
            <span>All rights reserved. </span>
        </p>
    </div>
</div>