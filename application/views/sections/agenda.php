<div>
    <div class="header">
        <h2>{{name_controller}}</h2>
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="master/#">Home</a>
                </li>
                <li>
                    <a href="master/#{{model}}">{{model}}</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel" ng-show="agendaselect">
                <div class="panel-content">
                    <div class="form">
                    <?php

                        echo $this->form_builder->open_form(array('action' => '#','ng-submit'=>"\$event.preventDefault();elegir()"));
                        echo $this->form_builder->build_form_horizontal(array(
                           array(
                                'id' => 'viajes_id_select_agenda',
                                'class'=> 'viajes_id_select',
                                'ng-model'=>'viajes_id',
                                'label'=>'Elegir viaje',
                                'value' =>''
                            ),

                            array(
                                'id' => 'Elegir',
                                'ng-click'=>'elegir()',
                                'value'=>'Elegir',
                                'type' => 'submit'
                            )
                        ));
                       echo $this->form_builder->close_form();



                    ?>
                    </div>
                </div>
            </div>
            <div class="panel" ng-show="agendadiv">
                <div class="panel-header panel-controls">
                    <h3><i class="fa fa-table"></i> <strong>Lista de</strong> {{name_controller}}</h3>
                </div>
                <div class="panel-content">
                    <!--<p>Lista de eventos</p>-->
                    <div class="m-b-20">
                        <div class="btn-group">
                            <button class="btn btn-sm btn-dark" ng-click="create()"><i class="fa fa-plus"></i> Nuevo {{model}}</button>
                        </div>
                    </div>

                          <?php

                         echo $this->form_builder->open_form(array('id'=>'form_agenda','action' => '#','ng-submit'=>"\$event.preventDefault();guardar_agenda()"));
                         echo form_hidden('viajes_id', '');
                         echo $this->form_builder->build_form_horizontal(array(
                             array(
                                     'id' => 'titulo',
                                     'ng-model'=>'titulo',
                                     'required'=>'',
                                     'label' => 'Titulo',
                                     'value' =>''
                                 ),
                             array(
                                     'id' => 'titulo_fecha',
                                     'ng-model'=>'titulo_fecha',
                                     'required'=>'',
                                     'label' => 'Fecha',
                                     'value' =>''
                                 ),
                             array(
                                 'id' => 'Guardar',
                                 'ng-click'=>'guardar_agenda()',
                                 'value'=>'Guardar',
                                 'type' => 'submit'
                             )
                         ));
                        echo $this->form_builder->close_form();
                        
                    ?>


                    <table class="table table-hover dataTable" id="table-empresas">
                        <thead>
                            <tr>
                                <th>Destino</th>
                                <th>Fecha</th>
                                <th class="text-right">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="val in agenda">
                                <td>{{val.name}}</td>
                                <td>{{val.fecha_text}}</td>
                                <td>
                                     <a class="btn btn-sm btn-default" ng-click="horarios({{val.id}})" href="javascript:;"><i class="icon-calendar"></i> Horarios</a>

                                    <a class="btn btn-sm btn-default" ng-click="editar({{val.id}})" href="javascript:;"><i class="icon-note"></i></a>

                                    <a class="btn btn-sm btn-danger" ng-click="borrar({{val.id}})" href="javascript:;"> <i class="icons-office-52"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('templates/footer_int'); ?>
</div>
