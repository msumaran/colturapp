<div>
    <div class="header">
        <h2>Contactos</h2>
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="master/#">Home</a>
                </li>
                <li>
                    <a href="master/#visitas">Visitas</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header panel-controls">
                    <h3><i class="fa fa-table"></i> <strong>Lista de</strong> Visitas</h3>
                </div>
                <div class="panel-content">
                    <!--<p>Lista de eventos</p>-->
                    <div class="m-b-20">
                        <div class="btn-group">
                            <button class="btn btn-sm btn-dark create" data-title="Visitas" data-model="clientes_visitas"><i class="fa fa-plus"></i> Nueva visita</button>
                        </div>
                    </div>
                    <table class="table table-hover dataTable" id="table_visitas">
                        <thead>
                            <tr>
                                <th>Motivo</th>
                                <th>Tipo</th>
                                <th>Cliente</th>
                                <th class="text-right">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('templates/footer_int'); ?>
</div>
