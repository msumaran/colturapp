<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Nativos Admin - Dashboard</title>
</head>
<body>

	<page backtop='120px' backimg="images/logo_marca_agua.jpg" backimgx="right" backimgy="top">
     <page_header backimg="images/logo_marca_agua.jpg">
   	
     	<div class="foto"><img src="images/logo_marca_agua.jpg" alt="Nativos Digitales" /></div>
     </page_header>

	 <page_footer>

     </page_footer>
	<div id="page">
		<div class="content">
			<div class="caja">
				Contact Report N° <?php echo $numero; ?>

			</div>

		<div class="datos">
			<p>
				<strong>Cliente:</strong> <?php echo $ret->cliente; ?>
			</p>
			<p>
				<strong>Proyecto:</strong> <?php echo $ret->proyecto; ?>
			</p>
			<p>
				<strong>Fecha:</strong> <?php echo pdf_fecha($ret->fecha); ?>
			</p>

		</div>
		<?php
			if($ret->tareas_nativos != '.'){
		?>
		<p><strong>Tareas por parte de Nativos Digitales:</strong></p>
		<?php echo $ret->tareas_nativos; ?>
		<?php
			}
		?>
		<?php
			if($ret->tareas_cliente != '.'){
		?>
		<p><strong>Tareas por parte de <?php echo $ret->cliente; ?>:</strong></p>
		<?php echo $ret->tareas_cliente; ?>
		<?php
			}
		?>
		<br />
		<div class="firma">
			<p>
							<?php echo $name ?><br />
				<strong><?php echo $cargo ?></strong>
			</p>
		</div>


		</div>
	</div>

  	</page>

<style type="text/css">
	.firma{ width: 50%; text-align: center; float: right; margin-left: 320px; margin-top: 25px;}
	body{
		font-family: Tahoma,Geneva,Kalimati,sans-serif;;
		font-size: 14px;
		line-height: 16px;

	}
	ul li{ padding: 6px 0;}
	.mas_foto{display: block; float: left;width: 300px; background-color: #000;height: 120px;}
	.foto{  height: 120px;width: 100%;display: block; text-align: right; background: url('images/logo_marca_agua.jpg') no-repeat top right;}
	#page{
		width: 650px;
		margin: 0 auto;
		font-family: Tahoma,Geneva,Kalimati,sans-serif;;
		font-size: 14px;
		overflow: hidden;
	}
	.caja{ padding: 8px 16px; width: 100%; background-color: #00adca; font-size: 16px; color: #fff; margin: 16px 0 42px 0; text-align: center;
		line-height: 23px; clear: both; font-weight: bold;

	 }
	 p{ margin-top: 8px 0;}
	 .datos{ margin-top: 18px 0;}

	#page .content{
		font-family: Tahoma,Geneva,Kalimati,sans-serif;;
		font-size: 14px;
	}
</style>
</body>

</html>