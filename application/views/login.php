<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="assets/global/images/favicon.png">
        <link href="assets/global/css/style.css" rel="stylesheet">
        <link href="assets/global/css/ui.css" rel="stylesheet">
        <link href="assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>
    <body class="account separate-inputs" data-page="login">
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <div class="account-wall">
                        <i class="user-img icons-faces-users-03"></i>
                        <form class="form-signin" action="dashboard.html" role="form">
                            <label id="error_msg" class="error"></div>
                            <div class="append-icon">
                                <input type="text" name="username" id="username" class="form-control form-white username" placeholder="Correo" required>
                                <i class="icon-user"></i>
                            </div>
                            <div class="append-icon m-b-20">
                                <input type="password" name="password" id="password" class="form-control form-white password" placeholder="Password" required>
                                <i class="icon-lock"></i>
                            </div>
                            <button type="submit" id="submit-form" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left">Ingresar</button>

                            <div class="clearfix">
                                <!--
                                <p class="pull-left m-t-20"><a id="login" href="#">Already got an account? Sign In</a></p>
                                <p class="pull-right m-t-20"><a href="user-signup-v1.html">New here? Sign up</a></p>
                                -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <p class="account-copyright">
                <span>Copyright © <?php echo date('Y'); ?> </span><span>Nativos Digitales</span>.<span>Todos los derechos reservados.</span>
            </p>

        </div>
        <script src="assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/global/plugins/backstretch/backstretch.min.js"></script>
        <script src="assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="assets/global/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="assets/global/plugins/jquery-validation/additional-methods.min.js"></script>
        <script src="assets/global/js/pages/login-v1.js?v1"></script>
    </body>
</html>
