<div class="form">
<?php

    echo $this->form_builder->open_form(array('action' => ''));
                            if($this->input->get('viajes_id')) echo form_hidden('viajes_id', $this->input->get('viajes_id'));
                            echo $this->form_builder->build_form_horizontal(array(
                                array(
                                    'id' => 'name',
                                    'required'=>'',
                                    'label' => 'Destino',
                                    'value' =>$row->name
                                ),
                                array(
                                    'id' => 'tipo',
                                    'type' => 'dropdown',
                                    'required'=>'',
                                    'label' => 'Tipo',
                                    'options' => $this->m_model->tipo_ar,
                                    'value' =>$row->tipo
                                ),
                                
                                array(
                                    'id' => 'vuelo',
                                    'required'=>'',
                                    'label' => 'Vuelo',
                                    'value' =>$row->vuelo
                                ),
                               
                                array(
                                    'id' => 'salida_hora',
                                    'class' => 'timerpicker',
                                    'required'=>'',
                                    'label' => 'Salida',
                                    'value' =>$row->salida_hora
                                ),

                                
                                array(
                                    'id' => 'llegada_hora',
                                    'class' => 'timerpicker',
                                    'required'=>'',
                                    'label' => 'Llegada',
                                    'value' =>$row->llegada_hora
                                ),


                                array(
                                    'id' => '',
                                    'type' => 'submit',
                                    'label' => 'Guardar'
                                )
                            ));
                            echo $this->form_builder->close_form();



?>
</div>
