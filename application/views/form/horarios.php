<div class="form">
<?php

    echo $this->form_builder->open_form(array('action' => ''));
                            if($this->input->get('agenda_id')) echo form_hidden('agenda_id', $this->input->get('agenda_id'));
                            echo $this->form_builder->build_form_horizontal(array(
                                
                                
                                array(
                                    'id' => 'tipo',
                                    'type' => 'dropdown',
                                    'options' => $this->m_model->tipo_ar,
                                    'value' =>$row->tipo
                                ),
                                array(
                                    'id' => 'hora',
                                    'required'=>'',
                                    'label' => 'Hora',
                                    'value' =>$row->hora
                                ),

                                  array(
                                    'id' => 'fecha_hora',
                                    'class'=>'timerpicker',
                                    'required'=>'',
                                    'label' => 'Hora',
                                    'value' =>$row->fecha_hora
                                ),
                                array(
                                    'id' => 'descp',
                                    'type'=>'textarea',
                                    'required'=>'',
                                    //'class' => 'wysihtml5',
                                    'label' => 'Descripción',
                                    'value' =>$row->descp
                                ),


                                array(
                                    'id' => '',
                                    'type' => 'submit',
                                    'label' => 'Guardar'
                                )
                            ));
                            echo $this->form_builder->close_form();



?>
</div>
