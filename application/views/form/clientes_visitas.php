<div class="form">
<?php
    

    if($this->input->get('clientes_id')) $clientes_id = $this->input->get('clientes_id');
    else $clientes_id = $row->clientes_id;


    echo $this->form_builder->open_form(array('action' => ''));
                            if($this->input->get('tipo')) echo form_hidden('tipo', $this->input->get('tipo'));
                            echo $this->form_builder->build_form_horizontal(array(
                                array(
                                    'id' => 'name',
                                    'required'=>'',
                                    'label' => 'Motivo',
                                    'value' =>$row->name
                                ),
                                array(
                                    'id' => 'clientes_id',
                                    'type' =>'dropdown',
                                    'label' => 'Cliente',
                                    'data-live-search'=>'true',
                                    'options' => $this->m_model->combo_clientes,
                                    'value' =>$clientes_id
                                ),
                                array(
                                    'id' => 'tipo',
                                    'type' =>'dropdown',
                                    'label' => 'Tipo',
                                    'data-live-search'=>'true',
                                    'options' => $this->m_model->tipo_ar,
                                    'value' => $row->tipo
                                ),

                                array(
                                    'id' => 'observaciones',
                                    'required'=>'',
                                    'type' => 'textarea',
                                    'row'=> 35,
                                    'label' => 'Observaciones',
                                    'class' => 'wysihtml5',
                                    'value' =>$row->observaciones
                                ),


                                


                                array(
                                    'id' => '',
                                    'type' => 'submit',
                                    'label' => 'Guardar'
                                )
                            ));
                            echo $this->form_builder->close_form();



?>
</div>
