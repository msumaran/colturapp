<div class="form">
<?php

    echo $this->form_builder->open_form(array('action' => ''));
                            if($this->input->get('tipo')) echo form_hidden('tipo', $this->input->get('tipo'));
                            echo $this->form_builder->build_form_horizontal(array(
                                array(
                                    'id' => 'name',
                                    'required'=>'',
                                    'label' => 'Nombre',
                                    'value' =>$row->name
                                ),

                                array(
                                    'id' => 'activo',
                                    'type' =>'dropdown',
                                    'label' => 'Activo',
                                    'data-live-search'=>'true',
                                    'options' => $this->m_model->activo_ar,
                                    'value' =>$row->activo
                                ),
                                array(
                                    'id' => 'archivo',
                                    'type'=>'file',
                                    'label' => 'Archivo',
                                    'value' =>$row->archivo
                                ),
                               
                                array(
                                    'id' => '',
                                    'type' => 'submit',
                                    'label' => 'Guardar'
                                )
                            ));
                            echo $this->form_builder->close_form();



?>
</div>
