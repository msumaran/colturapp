<div class="form">
<?php

    echo $this->form_builder->open_form(array('action' => ''));
                           echo form_hidden('tipo', 'contacto');
                            echo $this->form_builder->build_form_horizontal(array(
                                array(
                                    'id' => 'name',
                                    'required'=>'',
                                    'label' => 'Nombre',
                                    'value' =>$row->name
                                ),

                                 array(
                                    'id' => 'via',
                                    'type' =>'dropdown',
                                    'label' => 'Via',
                                    'data-live-search'=>'true',
                                    'options' => $this->m_model->via_ar,
                                    'value' =>$row->via
                                ),
                                array(
                                    'id' => 'direccion',
                                    'label' => 'Dirección',
                                    'value' =>$row->direccion
                                ),
                                array(
                                    'id' => 'web',
                                    'label' => 'Web',
                                    'value' =>$row->web
                                ),
                                array(
                                    'id' => 'distrito',
                                    'label' => 'Distrito',
                                    'value' =>$row->distrito
                                ),

                                array(
                                    'id' => 'fecha_inicio',
                                    'required'=>'',
                                    'type'=>'date',
                                    'label' => 'Fecha de Inicio',
                                    'value' =>$row->fecha_inicio
                                ),
                                array(
                                    'id' => 'fecha_aniversario',
                                    'type'=>'date',
                                    'label' => 'Fecha de Aniversario',
                                    'value' =>$row->fecha_aniversario
                                ),
                                array(
                                    'id' => 'giros_id',
                                    'type' =>'dropdown',
                                    'label' => 'Giro',
                                    'data-live-search'=>'true',
                                    'options' => $this->m_model->combo_giros,
                                    'value' =>$row->giros_id
                                ),
                                array(
                                    'id' => '',
                                    'type' => 'submit',
                                    'label' => 'Guardar'
                                )
                            ));
                            echo $this->form_builder->close_form();



?>
</div>
