<div class="form">
<?php

    echo $this->form_builder->open_form(array('action' => ''));
                            if($this->input->get('tipo')) echo form_hidden('tipo', $this->input->get('tipo'));
                            echo $this->form_builder->build_form_horizontal(array(
                                array(
                                    'id' => 'name',
                                    'required'=>'',
                                    'label' => 'Nombre',
                                    'value' =>$row->name
                                ),

                                array(
                                    'id' => 'activo',
                                    'type' => 'dropdown',
                                    'options' => $this->m_model->activo_ar,
                                    'value' =>$row->activo
                                ),
                                array(
                                    'id' => 'empresas_id',
                                    'type' => 'dropdown',
                                    'required'=>'',
                                    'label' => 'Empresa',
                                    'options' => $this->m_model->empresas_ar,
                                    'value' =>$row->empresas_id
                                ),
                                
                                array(
                                    'id' => 'instatag',
                                    'required'=>'',
                                    'label' => 'Código Instagram',
                                    'value' =>$row->instatag
                                ),

                                array(
                                    'id' => '',
                                    'type' => 'submit',
                                    'label' => 'Guardar'
                                )
                            ));
                            echo $this->form_builder->close_form();



?>
</div>
