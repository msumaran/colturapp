<div class="form">
<?php
    

    if($this->input->get('clientes_id')) $clientes_id = $this->input->get('clientes_id');
    else $clientes_id = $row->clientes_id;


    echo $this->form_builder->open_form(array('action' => ''));
                            if($this->input->get('tipo')) echo form_hidden('tipo', $this->input->get('tipo'));
                            echo $this->form_builder->build_form_horizontal(array(
                                array(
                                    'id' => 'contacto',
                                    'required'=>'',
                                    'label' => 'Nombre',
                                    'value' =>$row->contacto
                                ),
                                array(
                                    'id' => 'clientes_id',
                                    'type' =>'dropdown',
                                    'label' => 'Cliente',
                                    'data-live-search'=>'true',
                                    'options' => $this->m_model->combo_clientes,
                                    'value' =>$clientes_id
                                ),
                                array(
                                    'id' => 'email',
                                    'required'=>'',
                                    'label' => 'Email',
                                    'value' =>$row->email
                                ),
                                array(
                                    'id' => 'telefono',
                                    'required'=>'',
                                    'label' => 'Teléfono',
                                    'value' =>$row->telefono
                                ),
                                array(
                                    'id' => 'cargo',
                                    'required'=>'',
                                    'label' => 'Cargo',
                                    'value' =>$row->cargo
                                ),
                               
                                array(
                                    'id' => '',
                                    'type' => 'submit',
                                    'label' => 'Guardar'
                                )
                            ));
                            echo $this->form_builder->close_form();



?>
</div>
