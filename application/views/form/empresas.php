<div class="form">
<?php

    echo $this->form_builder->open_form(array('action' => ''));
                            //echo form_hidden('tipo', 'contacto');
                            echo $this->form_builder->build_form_horizontal(array(
                                array(
                                    'id' => 'name',
                                    'required'=>'',
                                    'label' => 'Nombre',
                                    'value' =>$row->name
                                ),

                                array(
                                    'id' => 'telefono',
                                    'label' => 'Telefono',
                                    'value' =>$row->telefono
                                ),


                                array(
                                    'id' => 'color',
                                    'class'=>'colorpicker',
                                    'label' => 'Color Fondo',
                                    'type' => 'color',
                                    'value' =>$row->color
                                ),

                                array(
                                    'id' => 'color2',
                                    'class'=>'colorpicker',
                                    'label' => 'Color Menu:',
                                    'type' => 'color',
                                    'value' =>$row->color2
                                ),

                                array(
                                    'id' => 'color_texto',
                                    'class'=>'colorpicker',
                                    'label' => 'Color Texto:',
                                    'type' => 'color',
                                    'value' =>$row->color_texto
                                ),

                                array(
                                    'id' => 'color_hover',
                                    'class'=>'colorpicker',
                                    'label' => 'Color Hover:',
                                    'type' => 'color',
                                    'value' =>$row->color_hover
                                ),

                                array(
                                    'id' => 'bg',
                                    'type'=>'imagen',
                                    'label' => 'Background (1360x789)',
                                    'value' =>$row->bg
                                ),

                                array(
                                    'id' => 'imagen',
                                    'type'=>'imagen',
                                    'label' => 'Logo (220x82)',
                                    'value' =>$row->imagen
                                ),


                                array(
                                    'id' => '',
                                    'type' => 'submit',
                                    'label' => 'Guardar'
                                )
                            ));
                            echo $this->form_builder->close_form();



?>
</div>
