<div class="inicio">
	<div class="row" style="margin-bottom: 40px;">
	  <div class="col-md-3">

	  	<a href="web/#/destino">
	  		<span class="icon-destino"></span> Destinos
	  	</a>
	  </div>
	  <div class="col-md-3">

	  		<a href="web/#/hotel">
	  		<span class="icon-hotel"></span> Hotel
	  	</a>
	  		
	  </div>
	 
	  <div class="col-md-3">
	  	<a href="web/#/vuelos">
	  			<span class="icon-vuelo"></span> Vuelos
	  		</a>
	  	
	  </div>
	   <div class="col-md-3">
	  		<a href="web/#/agenda">
	  			<span class="icon-agenda"></span> Agenda
	  		</a>
	  </div>

	</div>

	<div class="row">
	    <div class="col-md-3">
	  		<a href="web/#/participantes">
	  			<span class="icon-participantes"></span> Participantes
	  		</a>
	  </div>
	  <div class="col-md-3">
	  		<a href="web/#/recomendaciones">
	  			<span class="icon-recomendaciones"></span> Recomendaciones
	  		</a>
	  </div>
		<div class="col-md-3">
	  		<a href="web/#/imagenes">
	  			<span class="icon-imagenes"></span> Imágenes
	  		</a>
	  </div>
	  <div class="col-md-3">
	  		<a href="web/#/emergencia">
	  			<span class="icon-emergencia"></span> Emergencia
	  		</a>
	  </div>
	  
	</div>
</div>