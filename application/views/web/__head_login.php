<!doctype html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="admin-themes-lab">
        <meta name="author" content="themes-lab">
        <link rel="shortcut icon" href="assets/global/images/favicon.png" type="image/png">
        <title>Coltur</title>
        <base target="_self" href="<?php echo base_url(); ?>" />
        <link href="assets/web/css/style.css?v=<?php echo $v ?>" rel="stylesheet">
        <link href="assets/web/css/theme.css?v=<?php echo $v ?>" rel="stylesheet">
        <link href="assets/web/css/ui.css?v=<?php echo $v ?>" rel="stylesheet">
        <link href="assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
        
        <link href="assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">
        <link href="assets/global/plugins/icheck/skins/all.css" rel="stylesheet">
        <link href="assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
        <link href="assets/global/plugins/colorpicker/spectrum.css?v=<?php echo $v ?>" rel="stylesheet" />
        <link href="assets/global/plugins/step-form-wizard/css/step-form-wizard.css" rel="stylesheet" />
        <link href="assets/global/plugins/step-form-wizard/plugins/parsley/parsley.css" rel="stylesheet" />
        <link href="assets/global/plugins/ion-slider/ion.rangeSlider.css" rel="stylesheet" />
        <link href="assets/global/plugins/ion-slider/style.css" rel="stylesheet" />
        <link href="assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/hover-effects/hover-effects.min.css" rel="stylesheet">
        <link href="assets/global/plugins/jstree/dist/themes/default/style.css" rel="stylesheet" />
        <link href="assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/input-text/style.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/font-awesome-animation/font-awesome-animation.min.css" rel="stylesheet" />
        <!-- BEGIN ANGULARJS STYLE -->
        <link href="assets/web/css/mystyle.css?v=<?php echo $v ?>" rel="stylesheet">
        <!-- END ANGULARJS STYLE -->
        <!-- BEGIN LAYOUT STYLE -->
        <link href="assets/admin/layout1/css/layout.css?v=<?php echo $v ?>" rel="stylesheet">
        <!-- END LAYOUT STYLE -->
        <script src="assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body ng-app="newApp" class="fixed-topbar fixed-sidebar theme-sdtl color-default" ng-controller="loginCtrl" >