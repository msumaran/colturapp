<?php

$this->load->view('web/head_login');
?>

<?php if ($error) { ?>
    <div class="alert alert-danger alert-dismissible" role="alert" style="border-radius: 0; width: 100%; margin-bottom: 0">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Error!</strong> El número de indentidad no es válido.
    </div>

<?php } ?>

<!--<div class="intro"></div>-->
<div id="login">
    <div class="int">
        <br>
        <div class="logo"><img src="assets/web/images/coltur-logo-blanco.png" alt="" /></div>
        <div class="ng-scope">
            <form method="post" class="login">
                <div class="header">DNI o Carnet de extranjeria</div>
                <div class="content">
                    <p><input type="text" ng-model="dni" name="dni" value="" /></p>
                    <p><button type="submit">Entrar</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <style type="text/css">
    body{

        background: url('assets/global/images/web.png') no-repeat center #004c96;
    }

</style>
<?php
$this->load->view('web/footer');
?>
