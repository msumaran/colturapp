<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <style type="text/css">
    .instatag > iframe body {
        border: 0 !important;
    }
    </style>
</head>
<body class="instatag">
    <!-- InstaWidget -->
    <?php if($viaje): ?>
        <a href="https://instawidget.net/v/tag/ColturViajes" id="link-<?php echo $viaje->instatag ?>">#ColturViajes</a>
        <script src="https://instawidget.net/js/instawidget.js?u=<?php echo $viaje->instatag ?>&width=100%"></script>
    <?php else: ?>
        <p style="text-align: center;">Código no encontrado.</p>
    <?php endif; ?>
</body>
</html>
