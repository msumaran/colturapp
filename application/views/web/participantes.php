<div class="participantes" custom-scroll>
	<div class="row">
		<div class="col-md-12 text-left">
		  		<ul>
		  		  	<li ng-repeat="val in participantes">
		  		  		<i class="fa fa-circle"></i> {{val.name}} {{val.lastname}}
		  		 	</li>
		  		</ul>
	  	</div>
	</div>
</div>