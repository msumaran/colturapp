 <?php

    $this->load->view('web/head');
?>
<div class="intro"></div>
<div class="page-content">
	<div class="footerdiv">
		<div class="row">
			<div class="col-md-3">
				<div class="logo_empresa"><img src="{{credentials.imagen}}" alt="" /></div>
			</div>
			<div class="col-md-9">
				<div class="logotipo"><h1>{{title}}</h1></div>
			</div>
		</div>

		<nav class="menu">
			<ul>
				<li ng-class="{ active  : isActive('/')}">
					<a href="web/#/"><span class="icon-home"></span> Home</a>
				</li>

				<li ng-class="{ active  : isActive('/destino')}">
					<a href="web/#/destino"><span class="icon-destino"></span> Destino</a>
				</li>

				<li ng-class="{ active  : isActive('/hotel')}">
					<a href="web/#/hotel"><span class="icon-hotel"></span> Hotel</a>
				</li>

				<li ng-class="{ active  : isActive('/vuelos')}">
					<a href="web/#/vuelos"><span class="icon-vuelo"></span> Vuelos</a>
				</li>

				<li ng-class="{ active  : isActive('/agenda')}">
					<a href="web/#/agenda"><span class="icon-agenda"></span> Agenda</a>
				</li>

				<li ng-class="{ active  : isActive('/participantes')}">
					<a href="web/#/participantes"><span class="icon-participantes"></span> Participantes</a>
				</li>

				<li ng-class="{ active  : isActive('/recomendaciones')}">
					<a href="web/#/recomendaciones"><span class="icon-recomendaciones"></span> Recomendaciones</a>
				</li>
				<li  class="dropdown ultimo" ng-class="{ active  : isActive('/mas')}">
					<a class="dropdown-toggle ht" href="#" data-toggle="dropdown" data-hover="dropdown"> + </a>
						<ul class="dropdown-menu">
      						<li ng-class="{ active  : isActive('/emergencia')}"><a href="web/#/emergencia">Emergencia</a></li>
      						<li ng-class="{ active  : isActive('/imagenes')}"><a href="web/#/imagenes">Imágenes</a></li>
    					</ul>

				</li>

			</ul>
		</nav>
		<div ng-view class="at-view-slide-in-left divtransparent" >

		</div>
	</div>

	<div class="row footerdiv">
			<div class="col-md-5">
			</div>
			<div class="col-md-2 text-right">
				<!--<div class="logotipo">
					<img src="assets/web/images/logo_coltur.png" alt="" />
				</div>-->
			</div>
			<div class="col-md-5 text-right">
				<div class="logotipo iconos_footer text-right">


				  <a target="_blank" href="<?php echo $this->m_opciones->opt['twitter'] ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
          <a target="_blank" href="<?php echo $this->m_opciones->opt['facebook'] ?>"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
					<a target="_blank" href="http://www.colturviajes.com"><img src="assets/web/images/logo_footer.png" alt="" width="90" /></a>
				</div>
			</div>
</div>

</div>

<?php
    $this->load->view('web/footer');
?>
