<div class="vuelos" custom-scroll>
		<div class="col-md-6 text-center" style="border-right: 1px #ffcd00 dashed;">
			<div class="row" style="margin-bottom: 45px;" ng-repeat="val in vuelo_ida" ng-class="{'first': $first, 'last': $last}">
			  		<p class="fecha">{{val.fecha_text}}</p>
			  		<p><img src="assets/web/images/avion.png" alt="" /> {{val.name}}</p>
			  		<p><strong>{{val.vuelo}}</strong></p>
			  			<div class="detalles" style="padding: 0 120px">
			  				<div class="row">
			  						<div class="col-md-6 text-center">
							  						<p><strong>Sale:</strong></p>
							  						<span>{{val.salida_hora}}</span>

				  					</div>
				  					<div class="col-md-6 text-center">


							  				 		<p><strong>Llega:</strong></p>
							  						<span>{{val.llegada_hora}}</span>

					  				</div> <!-- fin col-md-6-->
				  			</div> <!-- fin row -->
			  			  </div> <!-- fin detalles -->

			 </div><!-- fin row -->

		</div><!-- fin col-md-6 ida -->


		<div class="col-md-6 text-center">
			<div class="row" style="margin-bottom: 45px;" ng-repeat="val in vuelo_vuelta" ng-class="{'first': $first, 'last': $last}">
			  		<p class="fecha">{{val.fecha_text}}</p>
			  		<p><img src="assets/web/images/avion.png" alt="" /> {{val.name}}</p>
			  		<p><strong>{{val.vuelo}}</strong></p>
			  			<div class="detalles"  style="padding: 0 120px">
			  				<div class="row">
			  						<div class="col-md-6 text-center">
							  						<p><strong>Sale:</strong></p>
							  						<span>{{val.salida_hora}}</span>

				  					</div>
				  								<div class="col-md-6 text-center">


							  				 		<p><strong>Llega:</strong></p>
							  						<span>{{val.llegada_hora}}</span>

					  				</div> <!-- fin col-md-6-->
				  			</div> <!-- fin row -->
			  			  </div> <!-- fin detalles -->

			 </div><!-- fin row -->

		</div><!-- fin col-md-6 ida -->
</div><!-- fin class vuelos -->
