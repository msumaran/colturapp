<script src="plugins/angular/angular.js"></script>
        <script src="plugins/json3/lib/json3.js"></script>
        <script src="plugins/angular-resource/angular-resource.js"></script>
        <script src="plugins/angular-cookies/angular-cookies.js"></script>
        <script src="plugins/angular-sanitize/angular-sanitize.js"></script>
        <script src="plugins/angular-animate/angular-animate.js"></script>
        <script src="plugins/angular-touch/angular-touch.js"></script>
        <script src="plugins/angular-route/angular-route.js"></script>
        <script src="plugins/angular-bootstrap/ui-bootstrap-tpls-0.12.1.js"></script>
        <script src="app/web/app.js?"></script>
        <script src="directives/ngViewClass.js?v=<?php echo $v ?>"></script>
        <script src="assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script>
        <script src="assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js?v1"></script>
        <script src="assets/global/plugins/retina/retina.min.js"></script>
        <script src="assets/global/plugins/select2/select2.min.js"></script>
        <script src="assets/global/plugins/icheck/icheck.min.js"></script>
        <script src="assets/global/js/sidebar_hover.js"></script>
        <script src="assets/global/js/widgets/notes.js"></script>



        <script src="assets/global/js/pages/search.js?"></script>
        <script src="assets/global/plugins/quickSearch/quicksearch.js"></script>
        <script src="assets/global/plugins/icheck/icheck.js"></script>
        <script src="assets/global/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="assets/global/plugins/jquery-validation/jquery.validate.js"></script>
        <script src="assets/global/plugins/bootstrap-slider/bootstrap-slider.js"></script>
        <script src="assets/global/plugins/ion-slider/ion.rangeSlider.js"></script>
        <script src="assets/global/plugins/bootstrap/js/jasny-bootstrap.js"></script>
        <script src="assets/global/plugins/moment/moment.min.js"></script>
        <script src="assets/global/plugins/moment/moment-with-locales.min.js"></script>

        <script src="assets/global/plugins/typed/typed.js"></script>
        <script src="assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script>
        <script src="assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script>
        <script src="assets/global/plugins/multidatepicker/multidatespicker.min.js"></script>
        <script src="assets/global/plugins/autosize/autosize.min.js"></script>
        <script src="assets/global/plugins/metrojs/metrojs.min.js"></script>
        <script src="js/web/builder.js"></script>
        <script src="js/pages/layouts_api.js"></script>
        <script src="js/web/application.js?"></script>
        <script src="js/web/plugins.js?"></script>
        <script src="app/web/mainCtrl.js?"></script>
        <script src="app/web/dashboardCtrl.js?"></script>
        <!--<script src="app/web/viajesCtrl.js?"></script>-->
    </body>
</html>
