<div class="agenda" id="listdiv">

<div class="row">
		<div class="col-md-12 text-center">
	  			
	  			<div class="clearfix text-center" id="titulo"> </div>
	  			<div class="clearfix fecha text-center" id="titulo_fecha">  </div>
	  	</div>
	</div>



	<div class="divitem"  ng-repeat="val in agenda" >
	<div class="row">
		<div class="col-md-12 text-left agendatxt">
	  			
	  			<div class="clearfix text-left"> 
	  			<span class="icon-destino grande"></span> {{val.name}}</div>
	  			<div class="clearfix fecha text-left"> {{val.fecha_text}} </div>
	  	</div>
	</div>

	<table>
		<tr ng-repeat="ret in val.horarios">
			<td style="width: 140px">
				<div class="horas">
					<div class="cronograma">
						<i class="icon-{{ret.tipo}}"></i> <span>{{ret.hora}}</span>
					</div>
				</div>
			</td>
			<td>
				{{ret.descp}}
			</td>
		</tr>
		
	</table>
	</div>
</div>