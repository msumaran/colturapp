<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="admin-themes-lab">
        <meta name="author" content="themes-lab">
        <link rel="shortcut icon" href="assets/global/images/favicon.png" type="image/png">
        <title>Coltur</title>
        <base target="_self" href="<?php echo base_url(); ?>" />
        <link href="assets/web/css/style.css" rel="stylesheet">
        <link href="plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <link href="assets/web/css/mystyle.css" rel="stylesheet">
    </head>
    <body>
