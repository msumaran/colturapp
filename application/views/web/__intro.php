 <?php

    $this->load->view('web/head');
?>
<div class="intro">


</div>

<div class="int" ng-controller="introCtrl">
	<div class="introdemo">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="logotipo"><img  src="{{credentials.imagen}}" alt="" /></div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-12 text-center">
				<h1>¡Felicitaciones!</h1>
				<div class="text">
				<p> Formas parte del selecto grupo que viajará a {{credentials.viaje}}</p>
				<p>A continuación, te ofrecemos la información necesaria para un viaje inolvidable.</p>
				</div>

				<div class="seguir text-center">

					<button ng-click="seguir()" class="btn-intro">DESCÚBRELO AQUÍ</button>
				</div>
			</div>
		</div>
	</div>

</div>

<?php
    $this->load->view('web/footer');
?>
