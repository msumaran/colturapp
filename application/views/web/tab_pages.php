<?php
   $this->load->view('web/head');
?>

<div class="intro" style="background-color: #000"></div>

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="logo_empresa"><img  src="<?php echo $user_data->imagen ?>" alt="" /></div>
            </div>
            <div class="col-md-9">
                <h1 id="main_title">Inicio</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="tab-page-content">
                    <!-- Nav tabs -->
                    <div style="background-color: #e81a4a;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#inicio" data-toggle="tab"><span class="icon-home"></span>Inicio</a></li>
                            <li><a href="#destino" data-toggle="tab"><span class="icon-destino"></span>Destino</a></li>
                            <li><a href="#hotel" data-toggle="tab"><span class="icon-hotel"></span>Hotel</a></li>
                            <li><a href="#vuelos" data-toggle="tab"><span class="icon-vuelo"></span>Vuelos</a></li>
                            <li><a href="#agenda" data-toggle="tab"><span class="icon-agenda"></span>Agenda</a></li>
                            <li><a href="#participantes" data-toggle="tab"><span class="icon-participantes"></span>Participantes</a></li>
                            <li><a href="#recomendacion" data-toggle="tab"><span class="icon-recomendaciones"></span>Recomendaciones</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <strong style="font-size: 48px">+</strong>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="#emergencia" data-toggle="tab">Emergencia</a></li>
                                    <li><a href="logout">Cerrar sesión</a></li>
                                    <!-- <li><a href="#imagenes" data-toggle="tab">Imagenes</a></li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="inicio">
                            <div class="scrollbar" id="style-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="javascript:onTab('destino')">
                                            <span class="icon-destino"></span>
                                            Destinos
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="javascript:onTab('hotel')">
                                            <span class="icon-hotel"></span>
                                            Hotel
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="javascript:onTab('vuelos')">
                                            <span class="icon-vuelo"></span>
                                            Vuelos
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="javascript:onTab(agenda)">
                                            <span class="icon-agenda"></span>
                                            Agenda
                                        </a>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 40px">
                                    <div class="col-md-3">
                                        <a href="javascript:onTab('participantes')">
                                            <span class="icon-participantes"></span>
                                            Participantes
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="javascript:onTab('recomendacion')">
                                            <span class="icon-recomendaciones"></span>
                                            Recomendaciones
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="javascript:onTab('imagenes')">
                                            <span class="icon-imagenes"></span>
                                            Imagenes
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="javascript:onTab(emergencia)">
                                            <span class="icon-emergencia"></span>
                                            Emergencia
                                        </a>
                                    </div>
                                </div>
                                <div class="force-overflow"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="destino">
                            <div class="scrollbar" id="style-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2><?php echo $destinos->name ?></h2>
                                        <div class="text">
                                            <?php echo $destinos->descp ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="force-overflow"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="hotel">
                            <div class="scrollbar" id="style-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2><?php echo $hoteles->name ?></h2>
                                        <div class="text">
                                            <?php echo $hoteles->descp ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="force-overflow"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="vuelos">
                            <div class="scrollbar" id="style-3">
                                <div class="row">
                                    <div class="col-md-6 text-center" style="border-right: 1px #ffcd00 dashed;">
                                        <?php foreach ($vuelos_ida as $ida) { ?>
                                            <div class="item-ida">
                                                <h3 class="text-center"><?php echo get_fecha_vuelo($ida->salida_hora) ?></h3>
                                                <p class="text-center"><img src="assets/web/images/avion.png" alt="" style="margin-right: 10px"/> <?php echo $ida->name ?></p>
                            			  		<p class="text-center"><strong><?php echo $ida->vuelo ?></strong></p>
                                                <div class="detalles" style="padding: 0 120px;">
                                                    <div class="row">
                                                        <div class="col-md-6 text-center">
                                                            <p class="text-center"><strong>Sale:</strong></p>
                                                            <span class="bag"><?php echo get_hora_vuelo($ida->salida_hora) ?></span>
                                                        </div>
                                                        <div class="col-md-6 text-center">
                                                            <p class="text-center"><strong>Llega:</strong></p>
                                                            <span class="bag"><?php echo get_hora_vuelo($ida->llegada_hora) ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-6 text-center">
                                        <?php foreach ($vuelos_vuelta as $vuelta) { ?>
                                            <div class="item-ida">
                                                <h3 class="text-center"><?php echo get_fecha_vuelo($vuelta->salida_hora) ?></h3>
                                                <p class="text-center"><img src="assets/web/images/avion.png" alt="" style="margin-right: 10px"/> <?php echo $vuelta->name ?></p>
                            			  		<p class="text-center"><strong><?php echo $vuelta->vuelo ?></strong></p>
                                                <div class="detalles" style="padding: 0 120px;">
                                                    <div class="row">
                                                        <div class="col-md-6 text-center">
                                                            <p class="text-center"><strong>Sale:</strong></p>
                                                            <span class="bag"><?php echo get_hora_vuelo($vuelta->salida_hora) ?></span>
                                                        </div>
                                                        <div class="col-md-6 text-center">
                                                            <p class="text-center"><strong>Llega:</strong></p>
                                                            <span class="bag"><?php echo get_hora_vuelo($vuelta->llegada_hora) ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="force-overflow"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="agenda">
                            <div class="scrollbar" id="style-3">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h2>
                                            <?php echo $agenda[0]->titulo ?><br>
                                            <small style="color: #fff"><?php echo $agenda[0]->titulo_fecha ?></small>
                                        </h2>
                                    </div>
                                    <div class="col-md-12">
                                        <?php foreach ($agenda as $agenda) { ?>
                                            <div class="agenda-title">
                                                <span class="icon-destino"></span>
                                                <?php echo $agenda->name ?>
                                                <br>
                                                <?php echo $agenda->fecha_text ?>
                                            </div>
                                            <div class="agenda-horario">
                                                <?php foreach ($agenda->horarios as $horario) { ?>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <i class="icon-<?php echo $horario->tipo ?>"></i>
                                                            <strong><?php echo $horario->hora ?></strong>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <?php echo $horario->descp ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="force-overflow"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="participantes">
                            <div class="scrollbar" id="style-3">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h2 class="text-uppercase">Participante</h2>
                                        <br>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row participantes-lista">
                                            <?php foreach ($participantes as $user) { ?>
                                                <div class="col-md-4">
                                                    <i class="fa fa-circle"></i> <?php echo $user->name . ', ' . $user->lastname ?>
                                                </div>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="force-overflow"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="recomendacion">
                            <div class="scrollbar" id="style-3">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h2 class="text-uppercase">Recomendaciones</h2>
                                        <br>
                                    </div>
                                    <div class="col-md-12">
                                        <?php foreach ($recomendaciones as $rec) { ?>
                                            <div class="text">
                                                <i class="icon-recomendacion"></i>
                                                <?php echo $rec->descp ?>
                                            </div>
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="force-overflow"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="emergencia">
                            <div class="scrollbar" id="style-3">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h2 class="text-uppercase">Emergencias</h2>
                                        <br>
                                    </div>
                                    <div class="col-md-12">
                                        <?php foreach ($emergencias as $emer) { ?>
                                            <div class="text">
                                                <?php echo $emer->descp ?>
                                            </div>
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="force-overflow"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="imagenes">
                            ...
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="pull-right">
            <a target="_blank" href="https://www.linkedin.com/company-beta/10529201/"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            <a target="_blank" href="https://www.facebook.com/Coltur-Viajes-de-Incentivos-946424625439998/?fref=ts"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
			<a target="_blank" href="http://www.colturviajes.com"><img src="assets/web/images/logo_footer.png" alt="" width="90" /></a>
        </div>
    </div>
</div>


<?php
    $this->load->view('web/footer');
?>
