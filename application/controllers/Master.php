<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends Admin_controller {

    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    * 		http://example.com/index.php/welcome
    *	- or -
    * 		http://example.com/index.php/welcome/index
    *	- or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see https://codeigniter.com/user_guide/general/urls.html
    */
    function __construct() {
        parent::__construct();
    }


    public function index()
    {
        //echo 'aaa';
        $this->load->model('m_users');

        $this->salida['v'] = 244;

        $this->load->view('home',  $this->salida);
    }
    public function horarios(){


        $this->load->view('sections/horarios', $this->salida);
    }
    public function vuelos(){


        $this->load->view('sections/vuelos', $this->salida);
    }

    public function viajes(){


        $this->load->view('sections/viajes', $this->salida);
    }
    public function agenda(){


        $this->load->view('sections/agenda', $this->salida);
    }
    public function hotel(){


        $this->load->view('sections/hotel', $this->salida);
    }
    public function destinos(){


        $this->load->view('sections/destinos', $this->salida);
    }
    public function imagenes(){


        $this->load->view('sections/imagenes', $this->salida);
    }
    public function emergencia(){


        $this->load->view('sections/emergencia', $this->salida);
    }
    public function recomendaciones(){


        $this->load->view('sections/recomendaciones', $this->salida);
    }
    public function participantes(){


        $this->load->view('sections/participantes', $this->salida);
    }

    public function opciones(){

        $this->load->view('sections/opciones', $this->salida);
    }


    public function historial(){

        $this->load->view('sections/historial', $this->salida);
    }

    public function notificaciones(){
        //echo 'demo';
        $this->load->view('sections/notificaciones', $this->salida);
    }
















    public function dashboard(){

        $this->load->view('sections/dashboard', $this->salida);
    }
    public function prospectos(){

        $this->load->model('m_clientes');

        $this->salida['row'] = $this->m_clientes;
        $this->load->view('sections/prospectos', $this->salida);
    }

    public function reportes(){

        $this->load->view('sections/reportes', $this->salida);
    }

    public function empresas(){

        $this->load->view('sections/empresas', $this->salida);
    }
    public function eventostipo(){


        $this->load->view('sections/eventostipo', $this->salida);
    }
    public function verclientes($id){



        $this->load->view('sections/verclientes', $this->salida);
    }



    public function visitas(){


        $this->load->view('sections/visitas', $this->salida);
    }

    public function perdidos(){
        $this->load->model('m_clientes');
        $this->salida['row'] = $this->m_clientes;
        $this->load->view('sections/perdidos', $this->salida);
    }
    public function contactos(){


        $this->load->view('sections/contactos', $this->salida);
    }
    public function brief(){


        $this->load->view('sections/brief', $this->salida);
    }




    public function testing(){


        $hostname = '{imap.gmail.com:143/imap/ssl/novalidate-cert}INBOX';
        $username = 'miguel@nativosdigitales.pe';
        $password = 'n3tj0v3n.rosa2';
        $inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());
        $emails = imap_search($inbox,'NEW');

        if($emails) {

            /* begin output var */
            $output = '';

            /* put the newest emails on top */
            rsort($emails);

            /* for every email... */
            foreach($emails as $email_number) {

                /* get information specific to this email */
                $overview = imap_fetch_overview($inbox,$email_number,0);
                $message = imap_fetchbody($inbox,$email_number,2);

                /* output the email header information */
                $output.= '<div class="toggler '.($overview[0]->seen ? 'read' : 'unread').'">';
                $output.= '<span class="subject">'.$overview[0]->subject.'</span> ';
                $output.= '<span class="from">'.$overview[0]->from.'</span>';
                $output.= '<span class="date">on '.$overview[0]->date.'</span>';
                $output.= '</div>';

                /* output the email body */
                $output.= '<div class="body">'.$message.'</div>';
            }

            echo $output;
        }

        /* close the connection */
        imap_close($inbox);




    }
}
