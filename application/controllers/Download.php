<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function brief($id,$clientes_id=0)
	{
		$this->load->helper('download');
		$this->load->model('m_clientes_brief');
		$this->load->model('m_clientes');

		$row = $this->m_clientes_brief->get_id($id);
		$clientes = $this->m_clientes->get_id($clientes_id);
		// Agregamos un punto al Lead de Afinidad
		if($clientes->brief == 'no'){
			$data['id'] = $clientes_id;

			$data['brief'] = 'si';

			$data['lead_afinidad'] = $clientes->lead_afinidad + 15;
			$this->m_clientes->save($data);

		}

		$ruta = archivo_ruta($row->archivo);

		force_download($ruta, NULL);		
	}

}
