<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends MY_Controller {
	var $data = array();
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	

		$this->salida['v'] = 4;
		$this->load->view('web/index' , $this->salida);
	
	}
	public function inicio(){
		$this->load->view('web/inicio' , $this->salida);

	}

	public function intro(){
		
		
		$this->salida['v'] = 4;
		$this->load->view('web/intro' , $this->salida);

	}

	public function destino(){
		$this->load->view('web/destino' , $this->salida);

	}
	public function hotel(){
		$this->load->view('web/hotel' , $this->salida);

	}
	public function vuelos(){
		$this->load->view('web/vuelos' , $this->salida);

	}
	public function agenda(){
		$this->load->view('web/agenda' , $this->salida);

	}
	public function participantes(){
		$this->load->view('web/participantes' , $this->salida);

	}
	public function recomendaciones(){
		$this->load->view('web/recomendaciones' , $this->salida);

	}
	public function emergencia(){
		$this->load->view('web/emergencia' , $this->salida);

	}
	public function imagenes(){
		$this->load->view('web/imagenes' , $this->salida);

	}
}
