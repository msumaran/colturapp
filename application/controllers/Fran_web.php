<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fran_web extends CI_Controller
{
    var $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->data['v'] = rand();
        $this->data['error'] = false;
    }

    public function index()
    {
        $result = false;

        if ($this->input->post()) {
            $this->load->model('m_participantes');

            $form_data = $this->input->post();

            if ($form_data['dni']) {
                $result = $this->m_participantes->get_row_where('dni="'.$form_data['dni'].'"');
            }

            if (!$result) {
                $this->data['error'] = true;
            } else {
                $result->imagen = img_ver(base_url(),$result->imagen);
                $result->bg = img_ver(base_url(),$result->bg);

                $this->session->set_userdata('user_data', $result);

                redirect('intro');
            }
        }
        $this->load->view('web/login', $this->data);
    }

    public function get_session_data()
    {
        if ($this->session->userdata('user_data')) {
            $this->data['user_data'] = (object) array(
                'empresa' => $this->session->userdata('user_data')->empresa,
                'viaje' => $this->session->userdata('user_data')->viaje,
                'imagen' => $this->session->userdata('user_data')->imagen,
                'color' => $this->session->userdata('user_data')->color,
                'color2' => $this->session->userdata('user_data')->color2,
                'telefono' => $this->session->userdata('user_data')->telefono,
                'bg' => $this->session->userdata('user_data')->bg,
                'color_texto' => $this->session->userdata('user_data')->color_texto,
                'color_hover' => $this->session->userdata('user_data')->color_hover,
                'color_button' => $this->session->userdata('user_data')->color_button,
                'id' => $this->session->userdata('user_data')->id,
                'viajes_id' => $this->session->userdata('user_data')->viajes_id,
                'name' => $this->session->userdata('user_data')->name,
                'lastname' => $this->session->userdata('user_data')->lastname,
                'email' => $this->session->userdata('user_data')->email,
                'dni' => $this->session->userdata('user_data')->dni
            );
        } else {
            $this->session->unset_userdata('user_data');
            redirect(base_url());
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_data');
        redirect(base_url());
    }

    public function intro()
    {
        $this->get_session_data();

        $this->load->view('web/intro', $this->data);
    }

    public function tab_pages()
    {
        //$this->output->enable_profiler(TRUE);

        $this->get_session_data();

        $this->load->model('m_destinos');
        $this->load->model('m_hotel');
        $this->load->model('m_agenda');
        $this->load->model('m_horarios');
        $this->load->model('m_participantes');
        $this->load->model('m_recomendaciones');
        $this->load->model('m_emergencia');
        $this->load->model('m_vuelos');

        $this->data['destinos'] = $this->m_destinos->get_row_where('viajes_id="'. $this->data['user_data']->viajes_id .'"');
        $this->data['hoteles'] = $this->m_hotel->get_row_where('viajes_id="'. $this->data['user_data']->viajes_id .'"');
        $this->data['agenda'] = $this->m_agenda->get_result_where('viajes_id="'. $this->data['user_data']->viajes_id .'"');
        foreach ($this->data['agenda'] as  $key => $val) {
            $result = $this->m_horarios->get_result_where('agenda_id="'. $val->id .'"');
            $this->data['agenda'][$key]->horarios = $result;
        }
        $this->data['participantes'] = $this->m_participantes->get_result_where('viajes_id="'. $this->data['user_data']->viajes_id .'"');
        $this->data['recomendaciones'] = $this->m_recomendaciones->get_result_where('viajes_id="'. $this->data['user_data']->viajes_id .'"');
        $this->data['emergencias'] = $this->m_emergencia->get_result_where('viajes_id="'. $this->data['user_data']->viajes_id .'"');
        $this->data['vuelos_ida'] = $this->m_vuelos->get_result_where('viajes_id="'. $this->data['user_data']->viajes_id .'" AND tipo="ida" ');
        $this->data['vuelos_vuelta'] = $this->m_vuelos->get_result_where('viajes_id="'. $this->data['user_data']->viajes_id .'" AND tipo="vuelta" ');

        // echo '<pre>';
        // print_r($this->data['vuelos_ida']);
        // echo '</pre>';

        $this->load->view('web/tab_pages', $this->data);
    }

}
