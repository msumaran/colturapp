<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    var $data = array();
    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    * 		http://example.com/index.php/welcome
    *	- or -
    * 		http://example.com/index.php/welcome/index
    *	- or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see https://codeigniter.com/user_guide/general/urls.html
    */
    public function index()
    {
        $this->salida['v'] = 10;
        $this->load->view('web/login' , $this->salida);
        //redirect('master/','refresh');
    }
    public function inicio()
    {
        $this->load->view('web/inicio' , $this->salida);
        //redirect('master/','refresh');
    }
    public function login(){

        $this->load->model('m_login');
        $this->load->view('login', $this->data);
    }
    public function salir(){

        $this->load->model('m_login');
        $this->m_login->Logout();
    }

    public function session(){
        $this->load->model('m_login');

        echo json_encode($this->m_login);

    }

    public function logear(){
        $this->load->model('m_login');

        $data = $this->input->post();

        if(  $this->m_login->Authenticate( $data['email'], $data['passwd']  )  ){

            echo json_encode($this->m_login);

        }else{
            echo json_encode($this->m_login);

        }

    }
}
