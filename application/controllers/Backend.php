<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function calculadoralead(){
			$this->load->model('m_clientes');
			$data = $this->input->get();
			$suma = 0;

			$total = 100;

			$clientes_id =  $data['clientes_id'];
			unset($data['clientes_id']);

			foreach ($data as $key => $value) {
				 $suma = $suma + $value;
			}

			$data = array();
			$data['id'] = $clientes_id;
			$data['lead_score'] = $suma;
			$this->m_clientes->save($data);

			$resultado = $this->m_clientes->get_id($data['id']);

			echo json_encode($resultado);

	}



	public function reportes($tipo){


		$this->load->model('m_reportes');

		$this->m_reportes->$tipo();

		
	}
	public function form_dialog($model,$id=0)
	{

	    	$modelo = 'm_'.$model;
	    	$this->load->model($modelo,'m_model');


	    if( !empty($id) ){
				$this->salida['row'] = $this->m_model->get_id( $id );
				if ( empty ( $this->salida['row'] ) ){
					show_404();
				}
				$this->salida['editar'] = true;
			}else{
				$this->salida['row'] = $this->m_model;
				$this->salida['editar'] = false;
			}
	    $this->load->view('form/'.$model,$this->salida);
	}

	public function update_dialog(){
	
    
    	$data = $this->input->post();
    	$this->db->where('viajes_id', $data['viajes_id']);
		var_dump($data);
		$this->db->update('agenda', $data);
    }
	public function add_dialog($model,$id=0){
		$this->load->helper('string');
		$this->load->helper('file');
    	$modelo = 'm_'.$model;
    	$this->load->model($modelo,'m_model');
    	$data = $this->input->post();


    	if ($model == 'notificaciones') {
            $date = new DateTime($data['fecha_total']);
            $data['fecha_total'] = $date->format('Y-m-d H:i:s');
        }

    	
    	if(!$data) return;
    	if( !empty($id) ){
				$data['id'] = $id;
		}
	
		$id = $this->m_model->save($data);
	
		$json = array();
		if(empty($id)){
			if(isset($this->m_model->error) and !empty($this->m_model->error)){
				$error = $this->m_model->error;
			}else{
				$error = 'Hubo un error!!';
			}
			$json['error'] = $error;
			echo json_encode($json);
			die();
		}
		$this->load->model($modelo,'m_model');
		if(is_array($this->m_model->get_id($id) )){
			$json = $this->m_model->get_id($id);
			$json['id'] = $id;
			$json['get'] = $this->input->get();
			if(isset( $data['name'] ) ) $json['name'] = $data['name'];
		}else{
			$json = $this->m_model->get_id($id);
			$json->id = $id;
			$json->get = $this->input->get();
			if(isset( $data['name'] ) ) $json->name = $data['name'];
		}
		echo json_encode($json);

    }
		function api($model){
				$modelo = 'm_'.$model;
				$this->load->model($modelo,'m_model');
				$result = $this->m_model->get_result();
				$data = array();

				foreach ($result as $row) {
					 if(isset($row->updated_at)) $row->updated_at = get_fecha_mysql($row->updated_at);
					 if(isset($row->created_at)) $row->created_at = get_fecha_mysql($row->created_at);
					 $row->DT_RowId = 'row_'.$row->id;
					 $row->opciones = $this->m_ui->ui_botones_admin_table($model,$row->id);
					 $data['data'][] = $row;

				}

				//var_dump($data);
				//die;

				echo json_encode($data);
		}
		public function upload_archivo(){
			$this->load->helper('string');
			$mes = date('m');
			$dia = date('d');
			$config['upload_path'] = './_files/'.$mes.'/'.$dia.'/';
			if (!is_dir($config['upload_path'])) {
				if (!is_dir('./_files/'.$mes.'/')) {
					mkdir('./_files/'.$mes.'/',0777);
				}
	    		mkdir($config['upload_path'],0777);
			}
			$this->ruta = $config['upload_path'];
			$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|gif|jpg|png|psd|ai|zip|rar|csv|ppt|pptx|mp3|sql';
			$config['file_name'] = $mes.$dia.random_string('alnum', 4);
			$this->load->library('upload', $config);
			if ( $this->upload->do_upload('Filedata') ){
				$datos = $this->upload->data();

				$datos['html'] = $this->load->view('upload/file', $datos, true);
				$ruta = base_url();
				$datos['url'] = img_ver($ruta,$datos['file_name']);
				echo json_encode($datos);
			}else{
				var_dump($_FILES);
				echo $this->upload->display_errors();
				echo 'No subio el archivo';
			}
		}

}
