<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');

class Webservice extends CI_Controller
{
    function __construct() {
        parent::__construct();

        //$this->load->helper('ui_helper');
    }

    function login() {
        $req = get_php_input();

        $data = array(
            'success' => true
        );

        echo json_encode($data);
    }

    function info() {
        $data = array(
            'id' => 1,
            'dni' => '46716600',
            'background_image' => 'img/brasil-cover-app.png',
            'id_viaje' => 20,
            'success' => true
        );
        echo json_encode($data);
    }
}
