<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function contactreport($id){
    	$this->load->model('m_contactreport');
    	$this->load->model('m_users');
    	$this->load->model('m_sueldos');
    	$this->load->helper('download');
    	 $this->load->library('html2pdf');
    	
		$ret = $this->m_contactreport->get_id($id);
		$name = url_title($ret->proyecto.'-'.$ret->cliente.'-CR-00'.$ret->id).'.pdf';
		$archivo = './_files/pdfs/'.$name;
		
		$users = $this->m_users->get_id( $ret->users_id );
		$sueldos = $this->m_sueldos->get_where( array('users_id'=>$users->id,'principal'=>'si'), false );
		
		$this->html2pdf->folder('./_files/pdfs/');
		
		$this->html2pdf->filename($name);
		$this->html2pdf->paper('a4', 'portrait');
		
		$data['ret'] = $ret;
		$data['cargo'] = $sueldos->cargo;
		$data['name'] = $users->name;

		$ar = explode('-', $ret->fecha);

		$data['numero'] = '0'.$ret->id.'-'.$ar[0];

		$this->html2pdf->html($this->load->view('documentos/contact_report', $data, true));
		if($this->html2pdf->create('save')) {
	    	$data = file_get_contents($archivo);
	    	force_download($name, $data);
		}else{
			echo 'Hubo un error contactate con el area de sistemas!';
		}
    	
    }

}
