<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function _pre($row,$model){
		if(isset($row->updated_at)) $row->updated_at = get_fecha_mysql($row->updated_at);
		if(isset($row->created_at)) $row->created_at = get_fecha_mysql($row->created_at);
		if(isset($row->creado)) $row->creado = get_fecha_mysql($row->creado);
		if(isset($row->fecha)) $row->fecha = get_fecha_mysql($row->fecha);
		if(isset($row->archivo)) $row->url = img_ver(base_url(),$row->archivo);
		if(isset($row->imagen)){
			if(empty($row->imagen)) $row->imagen = '';
			else $row->imagen = '<img src="'.img_ver(base_url(),$row->imagen).'" class="modal-img" />';
		}

		if(isset($row->bg)){
			if(empty($row->bg)) $row->bg = '';
			else $row->bg = img_ver(base_url(),$row->bg);
		}


	    //if(isset($row->color)) $row->color = '<span class="color" style="background-color:'.$row->color.';"> . <span>';
	    $row->opciones = $this->m_ui->ui_botones_admin_table($model,$row->id);
	    $row->DT_RowId = 'row_'.$row->id;
	    return $row;
	}
	function get_id($model,$id){
		header("Access-Control-Allow-Origin: *");
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$result = $this->m_model->get_id($id);
		echo json_encode($result);
	}

    function get_opciones(){
		header("Access-Control-Allow-Origin: *");
		$model = 'opciones';
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$result = $this->m_model->get_result();
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}

    function get_historial(){
		header("Access-Control-Allow-Origin: *");
		$this->load->model('m_log');
		$data = $this->m_log->get_result_2();
		echo json_encode($data);
	}



	function get_notificaciones($id){
		header("Access-Control-Allow-Origin: *");
		$model = 'notificaciones';
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$result = $this->m_model->get_result_where('viajes_id="'.$id.'"');
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}


	function get_participantes($id){
		header("Access-Control-Allow-Origin: *");
		$model = 'participantes';
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$result = $this->m_model->get_result_where('viajes_id="'.$id.'"');
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}

	function excel($id){
		header("Access-Control-Allow-Origin: *");
		$this->load->model('m_participantes');
		$request = get_php_input();

		foreach ($request->json as $row) {
			//var_dump($row);
			$row->viajes_id = $id;
			//var_dump($row);
			if(!empty( $row->dni ) and strtolower( $row->dni ) != 'dni'){
				$row->dni = trim($row->dni);
				$this->m_participantes->save($row);
			}

		}


		//
	}
	function get_recomendaciones($id){
		header("Access-Control-Allow-Origin: *");
		$model = 'recomendaciones';
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$result = $this->m_model->get_result_where('viajes_id="'.$id.'"');
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}

	function get_horarios_array($id){
		header("Access-Control-Allow-Origin: *");
		$this->load->model('m_horarios','m_model');
		$result = $this->m_model->get_result_where('agenda_id="'.$id.'"');
		//$data = array();
		return $result;
	}

	function get_horarios($id){
		header("Access-Control-Allow-Origin: *");
		$this->load->model('m_horarios','m_model');
		$this->db->order_by('hora', 'asc');
		$result = $this->m_model->get_result_where('agenda_id="'.$id.'"');
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row,'horarios');
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}

	function get_agenda_total($id){
		header("Access-Control-Allow-Origin: *");
		$this->load->model('m_agenda');
		$result = $this->m_agenda->get_result_where('viajes_id="'.$id.'"');
		$data = array();
		$this->db->reset_query();
		foreach ($result as $row) {
			 $row = $this->_pre($row, 'agenda');
			 $this->db->order_by('hora', 'asc');
			 $row->horarios = $this->get_horarios_array( $row->id );
			 $data['data'][] = $row;
		}

		echo json_encode($data);
	}


	function get_agenda($id){
		header("Access-Control-Allow-Origin: *");
		$model = 'agenda';
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$this->db->order_by('fecha_hora', 'asc');
		$result = $this->m_model->get_result_where('viajes_id="'.$id.'"');
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}
	function get_emergencia($id){
		header("Access-Control-Allow-Origin: *");
		$model = 'emergencia';
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$result = $this->m_model->get_result_where('viajes_id="'.$id.'"');
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}

	function get_imagenes($id){

		header("Access-Control-Allow-Origin: *");
		$model = 'imagenes';
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$result = $this->m_model->get_result_where('viajes_id="'.$id.'"');
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}
	function get_destinos($id){

		header("Access-Control-Allow-Origin: *");
		$model = 'destinos';
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$result = $this->m_model->get_result_where('viajes_id="'.$id.'"');
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}
	function get_hotel($id){
		header("Access-Control-Allow-Origin: *");
		$model = 'hotel';
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$result = $this->m_model->get_result_where('viajes_id="'.$id.'"');
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 //$row = nl2br($row->descp);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}
	function get_vuelos_tipo($id,$tipo){
		header("Access-Control-Allow-Origin: *");
		$model = 'vuelos';
		$modelo = 'm_'.$model;


		$this->load->model($modelo,'m_model');

		$result = $this->m_model->get_result_where('viajes_id="'.$id.'" AND tipo="'.$tipo.'" ');


		$data = array();
		foreach ($result as $row) {
			 //$row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}


	function get_vuelos($id){
		header("Access-Control-Allow-Origin: *");
		$model = 'vuelos';
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$this->db->order_by('salida_hora', 'asc');
		$result = $this->m_model->get_result_where('viajes_id="'.$id.'"');
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}


	function get($model){
		header("Access-Control-Allow-Origin: *");
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$result = $this->m_model->get_result_all();
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}

		echo json_encode($data);
	}
	function search($model){
		header("Access-Control-Allow-Origin: *");
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		switch ($model) {
			case 'clientes_contactos':
				$this->db->like($model.'.contacto', $this->input->get('q') );
				break;
			default:
				$this->db->like($model.'.name', $this->input->get('q') );
				break;
		}
		$result = $this->m_model->get_result_all();
		$data = array();
		foreach ($result as $row) {
			 $row = $this->_pre($row, $model);
			 $data['data'][] = $row;

		}
		echo json_encode($data);
	}
	function del($model,$id){
		header("Access-Control-Allow-Origin: *");
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$this->m_model->delete($id);
	}

	function save($model,$id){
		header("Access-Control-Allow-Origin: *");
		$modelo = 'm_'.$model;
		$this->load->model($modelo,'m_model');
		$data = json_decode(file_get_contents('php://input'), true);

		if(!empty($id) ) $data['id'] = $id;
		$this->m_model->save($data);
	}

	function login($dni=''){
		header("Access-Control-Allow-Origin: *");
		if ( ! session_id() ) @ session_start();
		$this->load->model('m_participantes');

		//$dni = str_pad($dni, 8, "0", STR_PAD_LEFT);

		if( isset($_SESSION['dni']) and empty($dni) ){
			$dni = $_SESSION['dni'];

		}


		$result = $this->m_participantes->get_row_where('dni="'.$dni.'"');

		if(empty($result)){

			echo json_encode($result);
			die;
		}
		if( !empty( $result) ){
			$_SESSION['dni'] = $dni;

		}
		$result->imagen = img_ver(base_url(),$result->imagen);
		$result->bg = img_ver(base_url(),$result->bg);
		echo json_encode($result);
	}

}
