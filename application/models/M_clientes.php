<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_clientes extends Admin_model {
	public $name, $razon_social, $ruc, $correo, $dni, $web, $contacto, $direccion, $distrito, $telefonos, $fecha_inicio, $fecha_aniversario, $giros_id, $estado, $via;
	public $estados_ar = array('Activo'=>'Activo','Perdido'=>'Perdido','Abandonado'=>'Abandonado');
	public $via_ar = array('correo'=>'Correo','web'=>'Web','telefono'=>'Teléfono','recomendacion'=>'Recomendacion' );
	public $error = '';
	public $combo_giros = array();
	function __construct(){
		// Call the Model constructor
		parent::__construct();

		$this->main_table = "clientes";
		$this->load->model(array('m_giros'));
		$this->combo_giros = $this->m_giros->combo('name');


	}

	public function get_id($id){
		if( empty ($id )) return false;
		$this->db->select($this->main_table .'.*,giros.name as giro,count(cotizacion.id) as cotizaciones');
		$this->db->from($this->main_table );
		$this->db->join('giros', 'giros.id = '.$this->main_table .'.giros_id');
		$this->db->join('cotizacion', 'cotizacion.clientes_id = '.$this->main_table .'.id','left');
		$this->db->where($this->main_table .'.id', $id);
		$query = $this->db->get();
		return $query->row();

	}
	public function join(){
		$this->db->select('count(cotizacion.id) as cotizaciones, giros.name as giro');
		$this->db->join('cotizacion', 'cotizacion.clientes_id = '.$this->main_table .'.id','left');
		$this->db->join('giros', 'giros.id = '.$this->main_table .'.giros_id','left');
		$this->db->group_by('cotizacion.clientes_id');
	}

	/**********************************/

	// nathali was here

}
