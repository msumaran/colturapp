<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_viajes extends Admin_model {
	var $name, $activo, $empresas_id, $instatag;
	public $activo_ar = array('si'=>'Sí','no'=>'No');
	function __construct(){
		// Call the Model constructor
		parent::__construct();

		$this->main_table = "viajes";

		//$this->db->select('users.name as user');
		//$this->db->join('users', 'users.id = eventos.users_id', 'left');
		$this->load->model('m_empresas');
		$this->empresas_ar = $this->m_empresas->combo('name');
	}
	public function join(){
		$this->db->select('empresas.name as empresa');
		$this->db->join('empresas', 'empresas.id = '.$this->main_table.'.empresas_id', 'left');
		//$this->db->join('eventos_tipo', 'eventos_tipo.id = eventos.eventos_tipo_id', 'left');
	}

    public function get_code_insta($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->main_table)->row();
    }
}
