<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_log extends Admin_model {

	function __construct(){
		// Call the Model constructor
		parent::__construct();

		$this->main_table = "log";


	}


    public function save_log($id_table, $main_table, $action) {

        $result_action = array();
        $result_action['id_user'] = $this->m_login->id_usuario;

        $this->load->model('m_' . $main_table, 'm_model');

        $data = $this->m_model->get_row($id_table);

        if ($main_table == 'empresas') {
            $result_action['accion'] = "Se " . $this->select_action($action) . " la empresa: [". $data->id ."] " . $data->name;
        }
        if ($main_table == 'viajes') {
            $result_action['accion'] = "Se " . $this->select_action($action) . " el viaje: [". $data->id ."] " . $data->name;
        }
        if ($main_table == 'hotel') {
            $result_action['accion'] = "Se " . $this->select_action($action) . " el hotel: [". $data->id ."] " . $data->name;
        }
        if ($main_table == 'participantes') {
            $result_action['accion'] = "Se " . $this->select_action($action) . " el participante con dni: [". $data->id ."] " . $data->dni;
        }
        if ($main_table == 'destinos') {
            $result_action['accion'] = "Se " . $this->select_action($action) . " el destino: [". $data->id ."] " . $data->name;
        }
        if ($main_table == 'recomendaciones') {
            $result_action['accion'] = "Se " . $this->select_action($action) . " la recomendación: [". $data->id ."] " . $data->name;
        }
        if ($main_table == 'vuelos') {
            $result_action['accion'] = "Se " . $this->select_action($action) . " el vuelo: [". $data->id ."] " . $data->name;
        }
        if ($main_table == 'agenda') {
            $result_action['accion'] = "Se " . $this->select_action($action) . " la agenda: [". $data->id ."] " . $data->name;
        }
        if ($main_table == 'emergencia') {
            $result_action['accion'] = "Se " . $this->select_action($action) . " la emergencia: [". $data->id ."] " . $data->name;
        }

        $this->db->insert($this->main_table, $result_action);
        return $this->db->insert_id();
    }


    public function select_action($action) {

        if ($action == 'update') {
            return 'actualizo';
        }
        if ($action == 'delete') {
            return 'elimino';
        }

        return 'registro';

    }

    public function get_result_2() {
        $this->db->select('u.name, l.accion, l.updated_at');
        $this->db->from('log AS l');
        $this->db->join('users AS u', 'u.id = l.id_user');
        return $this->db->get()->result();
    }
}
