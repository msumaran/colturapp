<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_participantes extends Admin_model {
	var $name, $lastname, $email, $dni;
	function __construct(){
		// Call the Model constructor
		parent::__construct();

		$this->main_table = "participantes";
			
	
	}
	public function join(){
		$this->db->select('empresas.name as empresa, viajes.name as viaje, empresas.imagen as imagen, empresas.color as color,
			empresas.color2 as color2, empresas.telefono as telefono, empresas.bg as bg,
			color_texto , color_hover, color_button');
		$this->db->join('viajes', 'viajes.id = '.$this->main_table.'.viajes_id', 'left');
		$this->db->join('empresas', 'empresas.id = viajes.empresas_id', 'left');
		//$this->db->join('eventos_tipo', 'eventos_tipo.id = eventos.eventos_tipo_id', 'left');
	}
}
