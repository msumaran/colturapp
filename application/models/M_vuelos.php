<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_vuelos extends Admin_model {
	var $name, $vuelos_id, $tipo, $fecha_text, $vuelo, $salida, $salida_hora, $llegada, $llegada_hora;
	public $tipo_ar = array('ida'=>'Ida','vuelta'=>'Vuelta');
	function __construct(){
		// Call the Model constructor
		parent::__construct();

		$this->main_table = "vuelos";
			
		//$this->db->select('users.name as user');
		//$this->db->join('users', 'users.id = eventos.users_id', 'left');
		$this->load->model('m_empresas');
		$this->empresas_ar = $this->m_empresas->combo('name');
	}
	public function join(){
		$this->db->select('empresas.name as empresa, viajes.name as viaje');
		$this->db->join('viajes', 'viajes.id = '.$this->main_table.'.viajes_id', 'left');
		$this->db->join('empresas', 'empresas.id = viajes.empresas_id', 'left');
		//$this->db->join('eventos_tipo', 'eventos_tipo.id = eventos.eventos_tipo_id', 'left');
	}
}
