<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_eventos extends Admin_model {
	var $name, $ubicacion, $fecha, $photo, $eventos_tipo_ar, $eventos_tipo_id;
	var $combos = ['eventos_tipo_ar'];
	function __construct(){
		// Call the Model constructor
		parent::__construct();

		$this->main_table = "eventos";
			
		//$this->db->select('users.name as user');
		//$this->db->join('users', 'users.id = eventos.users_id', 'left');
		$this->load->model('m_eventostipo');
		$this->eventos_tipo_ar = $this->m_eventostipo->combo('name');
	}
	public function join(){


		$this->db->select('users.name as user, eventos_tipo.name as eventos_tipo');
		$this->db->join('users', 'users.id = eventos.users_id', 'left');
		$this->db->join('eventos_tipo', 'eventos_tipo.id = eventos.eventos_tipo_id', 'left');
	}
}
