<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_clientes_visitas extends Admin_model {
	var $name, $contacto, $activo, $email, $telefono, $cargo, $clientes_id;
	public $tipo_ar = array('correo'=>'Correo','telefono'=>'Teléfono','visita'=>'Visita');
	public $combo_clientes = array();
	function __construct(){
		// Call the Model constructor
		parent::__construct();

		$this->main_table = "clientes_contactos";

		$this->load->model(array('m_clientes'));
		$this->combo_clientes = $this->combo_clientes('name');
	}

	public function combo_clientes($name,$id='id'){
		$table = 'clientes';
		$query = $this->db->select($table.".*")
							->from($table)
							->get();
		$data =  $query->result();
		$options = array();
		$options[''] = 'Seleccionar...';
		foreach($data as $row){

			 $options[$row->$id] = $row->$name;
		}
		return $options;
	}



	public function join(){
		$this->db->select('clientes.name as cliente');
		$this->db->join('clientes', 'clientes.id = '.$this->main_table .'.clientes_id','left');
	
	}
	/**********************************/

	// nathali was here

}