<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_reportes extends Admin_model {

	function __construct(){
		// Call the Model constructor
		parent::__construct();

		$this->main_table = "giros";
	}

	function mensual(){	

		$start = '2016-01-01 00:00:00';
		$end = date('Y-m-d G:i:s');

		$this->db->select('YEAR(clientes.created_at) as ano, MONTH(clientes.created_at) as mes, COUNT(clientes.created_at) as total');
		$this->db->from('clientes');
		$this->db->where('created_at BETWEEN  DATE("'.$start.'") AND  DATE("'.$end.'")', null, false);

		$this->db->group_by('YEAR(clientes.created_at), MONTH(clientes.created_at)');
		$this->db->group_by('YEAR(clientes.created_at), MONTH(clientes.created_at)');
		$query = $this->db->get();

		$years = array();
		foreach ($query->result() as $row) {

			$years[$row->ano][$row->mes] = $row->total;  
		}
		

		$ar = array();
		foreach ($years as $key => $value) {
			
			for ($i = 1; $i <= 12; $i++) {
				 if( isset( $years[$key][$i] ) ) $ar[$key][] = (int)$years[$key][$i];
			     else $ar[$key][] = 0;
			}
		}
		echo json_encode($ar);

	}

	function visitas(){	

		$start = '2016-01-01 00:00:00';
		$end = date('Y-m-d G:i:s', strtotime("+1 day") );

		$this->db->select('YEAR(created_at) as ano, MONTH(created_at) as mes, COUNT(created_at) as total');
		$this->db->from('clientes_visitas');
		$this->db->where('created_at BETWEEN  DATE("'.$start.'") AND  DATE("'.$end.'")', null, false);

		$this->db->group_by('YEAR(created_at), MONTH(created_at)');
		$this->db->group_by('YEAR(created_at), MONTH(created_at)');
		$query = $this->db->get();

		$years = array();
	
		foreach ($query->result() as $row) {

			$years[$row->ano][$row->mes] = $row->total;  
		}
		

		$ar = array();
		foreach ($years as $key => $value) {
			
			for ($i = 1; $i <= 12; $i++) {
				 if( isset( $years[$key][$i] ) ) $ar[$key][] = (int)$years[$key][$i];
			     else $ar[$key][] = 0;
			}
		}
		echo json_encode($ar);

	}
	
	/**********************************/

	// nathali was here

}
