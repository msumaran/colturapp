<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_horarios extends Admin_model {
	var $tipo, $hora, $descp, $fecha_hora;
	public $tipo_ar = array('taza'=>'Taza de cafe',
		'vuelo'=>'Vuelo',
		'hombre'=>'Hombre caminando',
		'montanas'=>'Montañas',
		'bolsa'=>'Bolsa de compras',
		'tarjeta'=>'Tarjeta de credito',
		'sol'=>'Sol',
		'grupo'=>'Grupo de personas',
		'choreo'=>'Fiesta',
		'durmiendo'=>'Durmiendo',
		'campana'=>'Campana de Hotel'
		);
	function __construct(){
		// Call the Model constructor
		parent::__construct();

		$this->main_table = "horarios";
			
		//$this->db->select('users.name as user');
		//$this->db->join('users', 'users.id = eventos.users_id', 'left');
		//$this->load->model('m_empresas');
		//$this->empresas_ar = $this->m_empresas->combo('name');
	}
	public function join(){
		$this->db->select('empresas.name as empresa, agenda.name as agenda, agenda.fecha_text as fecha_text');
		$this->db->join('agenda', 'agenda.id = '.$this->main_table.'.agenda_id', 'left');
		$this->db->join('viajes', 'viajes.id = agenda.viajes_id', 'left');
		$this->db->join('empresas', 'empresas.id = viajes.empresas_id', 'left');
		//$this->db->join('eventos_tipo', 'eventos_tipo.id = eventos.eventos_tipo_id', 'left');
	}
}
