<?php  
class m_ui extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		
		//$this->load->library('My_PHPMailer');
	}


	function send_mail($users_id, $titulo, $name, $btn_accion,$url_accion,$data_extra=''){
		$this->load->model('m_users');
		$this->load->library('parser');
		$this->load->model('m_correos');
		$data = array();
		$ret = $this->m_correos->get_where( array( 'name'=> $name ), false );

		$data = array();

		$data['titulo'] = $titulo;
		$data['btn_accion'] = $btn_accion;
		$data['url_accion'] = base_url().$url_accion;
		$data['base'] = base_url();
		$user = $this->m_users->get_id($users_id);
		if(!empty($data_extra)){

			$user = (Object) array_merge( (array) $user , (array) $data_extra );
		}

		$data['msg'] =  $this->parser->parse_string($ret->mensaje, $user, TRUE);


		
		$msg = $this->load->view('correos/mensaje_reload',$data,true);

		$mail = new PHPMailer();
        $mail->SetFrom( $this->m_opciones->opt['admin'] , $this->m_opciones->opt['razon_social']);  //Quien envía el correo
        $mail->Subject    = $ret->subject;
        $mail->Body      = $msg;
        $mail->AltBody    = strip_tags($msg);
        $mail->AddAddress($user->email, $user->name);
 
        
        if( !$mail->Send() ) {
            $data["msg"] = "Error en el envío: " . $mail->ErrorInfo;
        } else {
            $data["msg"] = "¡Mensaje enviado correctamente!";
        }


	}
	function limpiar_gmail($text=''){
		
		if(empty($text)) return '';
		$sanitizedData = strtr($text,'-_', '+/');
	    return base64_decode($sanitizedData);


	}
	function get_color_fecha($date){
		

		$today = strtotime("now");
		$date = strtotime($date);

		
		if ($date < $today) {
			return 'bg-danger';

		}else{
			return '';

		}

	}
	function generar_modal_form($id,$name,$form){
		return '
	<div  tabindex="-1" id="'.$id.'" role="'.$id.'" aria-labelledby="modal-login-label" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                <h4 id="modal-login-label" class="modal-title">'.$name.'</h4></div>
            <div class="modal-body">
                <div class="form">
                    '.$form.'
                </div>
            </div>
        </div>
    </div>
</div>';


	}
	function calendar_add($cal, $inicio, $fin , $subject, $invite=''){

		$event = new Google_Event();
		$event->setSummary($subject);
		$event->setLocation('Nativos Digitales');
		$start = new Google_EventDateTime();
		$start->setTimeZone('America/Lima');

		$start->setDateTime(date('c', strtotime ($inicio) ) );
		$event->setStart($start);
		$end = new Google_EventDateTime();
		$end->setDateTime(
			date('c', 
					strtotime ( '+1 hour' , $fin )   
				) 
		);
		$event->setEnd($end);
		if(empty($invite)){
			$attendee1 = new Google_EventAttendee(); //note in the API examples it calls EventAttendee().
			$attendee1->setEmail($invitado); //make sure you actually put an email address in here
			$attendees = array($attendee1);
			$event->attendees = $attendees;

		}
		$createdEvent = $cal->events->insert('primary', $event);
	}
	function log($model='',$text,$id_usuario){

		$CI 	=& get_instance();
		$data = array();

		$data['sql'] = $this->db->last_query();
		$data['model'] = $model;
		$data['controller'] = $this->uri->ruri_string();
		$data['text'] = $text;
		$data['users_id'] = $id_usuario;
		$data['browser'] = $this->agent->browser().' '.$this->agent->version();
		$this->db->insert('logs', $data);
	}
	function puntos($model, $model_id, $puntos, $users_id = '', $tipo= ''){
		$this->db->where('model',$model);
		$this->db->where('model_id',$model_id);
        $q = $this->db->get('users_puntos');
		if($q->num_rows() == 0){
			$data = array();
			if(empty($users_id) ) $data['users_id'] = $this->m_login->id_usuario;
			else $data['users_id'] = $users_id;

			if(!empty($tipo) ) $data['tipo'] = $tipo;

			$data['model'] = $model;
			$data['model_id'] = $model_id;
			$data['puntos'] = $puntos;

			$this->db->insert('users_puntos', $data);
		}
	}
	function mes($mes=''){
		if(empty($mes)){
			$mes = date('n');

		}
		$mes_espanol = array( 1 => "Enero",2 => "Febrero",3 => "Marzo",4 => "Abril",5 => "Mayo",
			6 => "Junio",7 => "Julio",8 => "Agosto",9 => "Septiembre",10 => "Octubre",11 => "Noviembre",12 => "Diciembre");
		return '<div class="select-mes">'.form_dropdown('mes', $mes_espanol, $mes,' class="form-control"').'</div>';
	}
	function ano($ano=''){
		if(empty($ano)){
			$ano = date('Y');
			
		}

		$ano_sel = date('Y');
		$array = array();

		for($i=0 ; $i<=3 ; $i++){
			
			$array[$ano_sel] = $ano_sel;
			$ano_sel = $ano_sel - 1;
		}

		return '<div class="select-ano">'.form_dropdown('ano', $array, $ano,' class="form-control"').'</div>';
	}
	function vista_previa($tabla,$id){
		$fields = $this->db->field_data($tabla);
		
		foreach ($fields as $row) {
			var_dump($row);
		}
	}
	function get_restar_day($day){

		
		$semana = date('N',strtotime("-$day day"));
		// es lunes
		if( $semana == 1 ){
			$day = 3;
		}
		$fecha = date('Y-m-d',strtotime("-$day day"));
		return $fecha;
	}
	function validar_hora_trabajo($hora_min=8, $hora_max=18){
		$hora = date('G');
		$semana = date('N');
		if( $semana <= 5 ){
			if( $hora > $hora_min and $hora <= $hora_max ){
				return true;
			}
		}

		return false;
	}
	function send_correo_text(){


		
	}
	function send_email_text($users_id, $data=array(),$subject='',$roles=true){

		$this->load->model('m_correos');
		$this->load->model('m_users');

		$this->load->library('parser');

		
		$user = $this->m_users->get_id( $users_id );
		if($roles){
			switch ($user->roles_id) {
				// Colaboradores
				case 3:
				 	$directores = $this->m_ui->get_director($users_id);	
	   				$gerentes = $this->m_ui->get_gerencia();
				break;
				// Direccion
				case 1:
	   				$gerentes = $this->m_ui->get_gerencia();
				break;
			}
		}
		$user->hora = date("h:i a");
		// Juntamos las variables user con los datos
		$mensaje = $this->load->view('/correos/mensaje',$data,true);
		$cc = array();
		if( isset($directores) and !empty($directores) ){
			foreach ($directores as $raw) {
				$cc[] = $raw->email;
			}

		}
		if( isset($gerentes) and !empty($gerentes) ){
			foreach ($gerentes as $raw) {
				$cc[] = $raw->email;
			}

		}
		$this->email($user->name, $user->email, $cc, $subject, $mensaje);
	}


	function send_email($users_id, $name,$data=array(),$roles=true,$subject=''){
		$this->load->model('m_correos');
		$this->load->model('m_users');
		$this->load->model('m_mails');
		$this->load->library('parser');
		$user = $this->m_users->get_id( $users_id );
		
		$user->hora = date("h:i a");
		// Juntamos las variables user con los datos
		if(!empty($data)){
			foreach($data as $row=>$val){
				$user->$row = $val;
			}
		}
		$ret = $this->m_correos->get_where( array( 'name'=> $name ), false );
		$data = array();
		$data['base'] = base_url();
		$mensaje =  $this->parser->parse_string($ret->mensaje, $user, TRUE);
		//$mensaje = $this->load->view('/correos/mensaje',$data,true);
		
		if(!empty($ret->subject))  $subject =  $ret->subject.' '.$subject;
		//$this->email($user->name, $user->email, $cc, $ret->subject, $mensaje);


		$data = array();
		$data['subject'] = $subject;
		$data['correos_id'] = $ret->id;
		$data['users_id'] = $users_id;
		$data['text'] = $mensaje;
		$this->m_mails->add($data);
		
	}
	function email($name,$to,$cc,$subject, $mensaje){
		$this->load->library('email');

	
		$config['protocol'] = 'mail';
		//$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'utf8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';

		$this->email->initialize($config);


		$this->load->library('email', $config); 
		$config['charset'] = 'utf8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html'; 
		$this->email->initialize($config);
		
		$this->email->to($to, $name);
		
		$this->email->from('erp@nativosdigitales.pe','ERP Nativos Digitales');
		if( !empty($cc)){
			$this->email->cc($cc);
		}
		$this->email->subject($subject);
		$this->email->message($mensaje);	
		if( ENVIRONMENT == 'development'){
			echo $to.'<br />';
			var_dump($cc);
			echo $subject.'<br />';
			echo $mensaje;
			
			//echo $this->email->print_debugger();
			
		}else{
			$this->email->send();
			//echo $this->email->print_debugger();
		}

	}
	function get_dates($inicio, $fin){

		

	}
	function get_administracion(){
		$this->load->model('m_users');
		
		$ret = $this->m_users->get_administracion();
		return $ret;
		
	}
	function get_gerencia(){
		$this->load->model('m_users');
		
		$ret = $this->m_users->get_or_where( array('roles-name '=>'Master','roles-name'=>'Gerencia' ) );
		return $ret;
		
	}
	function get_director($users_id){
		$this->load->model('m_users');
		$this->load->model('m_users_areas');
		$ret = $this->m_users->get_id($users_id);
		
		switch($ret->roles){
			case 'colaborador':
				$row = $this->m_users_areas->get_director(array('users_id'=>$users_id) );
				
			break;

		}
		return $row;
	}
	function randon_frase(){
		$this->load->model('m_frases');
		return $this->m_frases->get_randon()->texto;
	}
	function randon_user(){
		$this->load->model('m_users');
		return $this->m_users->get_randon();
	}
	/**
	*	 Tipo : danger, success, info, 	warning
	*
	***/
	function msj_ui($msj, $tipo='danger'){
		if(empty($msj)) return;
		return '<div class="alert alert-'.$tipo.'">'.$msj.'</div>';

	}
	function btn_proyectos($name,$id){
		return '<a href="#" data-name="'.$name.'"  data-id="'.$id.'" 
		class="proyectosbtn btn btn-red btn-sm" role="button" 
		aria-disabled="false">
		<span class="ui-button-text">
		'.$name.'
		</span>
		</a> ';
	}
	function btn_tareas($name){
		return '<a href="#" data-name="'.$name.'" class="tareasbtn btn btn-green btn-sm" role="button" aria-disabled="false">
		<span class="ui-button-text">'.$name.'</span></a> ';
	}
	function btn_ui($url,$name){
		return '<a href="'.$url.'" class="da-ex-buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-default" role="button" aria-disabled="false">
		<span class="ui-button-text">'.$name.'</span>
		</a> ';
	}
	function btn_regresar($modulo,$name='regresar'){
		if( !$this->input->get() ) return;
		$base = $this->uri->segment(1);
		return '<a href="'.base_url($base.'/'.$modulo).'" class="btn btn-green" role="button" aria-disabled="false">
		<span class="ui-button-text">'.$name.'</span>
		</a> ';
	}
	function btn_cuotas($url){
		$html = '';
		$this->load->model("m_roles");
		$this->load->model("m_users");
		$users = $this->m_users->get_where(array('cuota_activo'=>'si'));
		foreach ($users as $row) {
					$html .= $this->btn_ui($url.$row->id,$row->email);
		}
		return $html;

	}

	function btn_users($url){

		$html = '';
		$this->load->model("m_roles");
		$this->load->model("m_users");

		$roles =  $this->m_users->get_where( array('activo'=>'si', 'tipo'=>'user' ) );
		
		foreach ($roles as $row) {
			
			$html .= $this->btn_ui($url.$row->id,$row->email);

		}
		return $html;
	}
	function inbox(){
		$this->load->model("m_mensajes");

		


	}
	function ui_filtros_buscar($tipo=''){
		
		
		$html = '<form method="get" action="">
					<div class="panel-body">
		            <label>Buscar:</label>
		            <div class="da-form-item small">
		            <div class="col-sm-9">
		                '.form_input('buscar', $this->input->get('buscar'),'class="form-control"').'
					</div>
					<div class="col-sm-3">
		                <input type="submit" name="" value="Buscar" class="btn btn-primary"></div>
		            </div>
		            </div>
		        </div>
		        </form>
		        ';
		return $html;
	}
	function ui_filtros_proveedores($tipo=''){
		$this->load->model("m_proveedores");
		if( empty($tipo) ){
			$combo_proveedores = $this->m_proveedores->combo('name');
		}else{
			$combo_proveedores = $this->m_proveedores->combo_where( array( 'tipo'=>$tipo ) );	
		}
		
		$html = '<div class="panel-body">
		            <label>Filtrar por Proveedores:</label>
		            <div class="da-form-item small">
		                '.form_dropdown('proveedores_id', $combo_proveedores, $this->input->get('proveedores_id'),'data-live-search="true" data-style="btn-white" class="selectpicker form-control" id="filtro_proveedores_id"').'
		            </div>
		        </div>';
		return $html;
	}
	function ui_filtros_cliente(){
		$this->load->model("m_clientes");
		$combo_clientes = $this->m_clientes->combo('name');
		$html = '<div class="panel-body">
		            <label>Filtrar por Cliente:</label>
		            <div class="da-form-item small">
		                '.form_dropdown('clientes_id', $combo_clientes, $this->input->get('clientes_id'),'data-live-search="true" data-style="btn-white" class="selectpicker form-control" id="filtro_clientes_id"').'
		            </div>
		        </div>';
		return $html;
	}
	function ui_filtros_usuarios(){
		$this->load->model("m_users");
		$combo_users = $this->m_users->combo_where( array('activo'=>'si', 'tipo'=>'user') );

		$html = '<div class="panel-body">
		            <label>Filtrar por Colaborador:</label>
		            <div class="da-form-item small">
		                '.form_dropdown('users_id', $combo_users, $this->input->get('users_id'),'data-live-search="true" data-style="btn-white" class="selectpicker form-control" id="filtro_users_id"').'
		            </div>
		        </div>';
		return $html;
	}

	function ui_filtros_cotizaciones_estados(){
		$this->load->model("m_cotizaciones_estados");
		$combo_estados = $this->m_cotizaciones_estados->combo( 'name' );

		$html = '<div class="panel-body">
		            <label>Filtrar por Estados:</label>
		            <div class="da-form-item small">
		                '.form_dropdown('cotizaciones_estados_id', $combo_estados, $this->input->get('cotizaciones_estados_id'),'data-live-search="true" data-style="btn-white" class="selectpicker form-control" id="filtro_cotizaciones_estados_id"').'
		            </div>
		        </div>';
		return $html;
	}
	function ui_js_css(){
		//echo $this->router->class.'/'.$this->router->method;
		$this->db->select('*');
		$this->db->from('librerias');
		$this->db->where('name', $this->router->class.'/'.$this->router->method);
		$query = $this->db->get();
		return $query->row();

	}

	function ui_botones_admin($model){
		$html = '';
		$base = base_url();
		$html .= '<div class="portlet-header pam mbn">
                    <div class="caption"></div>
                    <div class="actions"><a href="'.$base.$this->uri->segment(1).'/'.$this->uri->segment(2).'_form/" class="btn btn-green create" data-model="'.$model.'" ><i class="fa fa-plus"></i>&nbsp;
                        Agregar</a>
                    </div>
                </div>';
        return $html;
	}
	function ui_botones_admin_table_js($model='',$id=0,$borrar = true, $descargar=false){
		$html = '';
		$base = base_url();
		$html .= '<a class="btn btn-default btn-xs create" data-model="'.$model.'" data-id='.$id.' href="'.$base.$this->uri->segment(1).'/'.$this->uri->segment(2).'_form/'.$id.'" style="margin-left:12px;">  <i class="fa fa-edit"></i></a>';

		if($borrar){
			$html .=  '<a class="btn btn-danger btn-xs delete2" data-model="'.$model.'" data-id='.$id.' href="'.$base.$this->uri->segment(1).'/'.$this->uri->segment(2).'/del/'.$id.'" style="margin-left:12px;" onclick="return confirm(\'Estas seguro?\')" >
			<i class="fa fa-trash-o"></i></a>';
		}
		return $html;

	}


	function ui_botones_admin_table($model='',$id=0,$borrar = true, $descargar=false){
		$html = '';
		$base = base_url();
		$html .= '<a class="edit btn btn-sm btn-default create" data-model="'.$model.'" data-id="'.$id.'" href="javascript:;"><i class="icon-note"></i></a>';

		if($borrar){
			$html .=  '<a class="delete btn btn-sm btn-danger" data-model="'.$model.'" data-id="'.$id.'"  href="javascript:;"> <i class="icons-office-52"></i></a>';
		}
		return $html;

	}
	/****
	*
	*	Color: red, green, blue, pink, orange, gray
	*
	***/
	function ui_botom($url, $texto, $color){
		return '<a href="'.$url.'" data-name="'.$texto.'" class="btn btn-'.$color.'" role="button" aria-disabled="false">
		<span class="ui-button-text">'.$texto.'</span></a>';
		//return '<input type="button" class="da-button '.$color.' btn_url" data-url="'.$url.'" value="'.$texto.'">';
	}

	function paginador($total){

		$this->load->library("pagination");
		$config = array();
        $config["base_url"] = base_url() . $this->uri->segment(1)."/".$this->uri->segment(2);
        $config["total_rows"] = $total;
        $config["per_page"] = $this->config->item('nro_paginador');
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);
        return $this->pagination->create_links();
	}
	function limpiar_where($row){
		//var_dump($row);	
		$row = str_replace('migual', ' >=', $row);
		$row = str_replace('-igual', ' <=', $row);
		$row = str_replace('m', ' >', $row);
		$row = str_replace('-', ' <', $row);
		return $row;
	}
	public function css_js($array,$tipo){
		if(!is_array($array)) return;
		//var_dump($array);
		foreach ($array as $ret) {
			switch ($ret) {
				case 'cropper':
				if($tipo=='css'){
					$this->_css('cropper/css/cropper.min.css');
				}else{
					$this->_js('cropper/js/cropper.min.js');
					
				}
				break;
				case 'tree':
				if($tipo=='css'){
					$this->_css('jquery-treetable/stylesheets/jquery.treetable.css');
					$this->_css('jquery-treetable/stylesheets/jquery.treetable.theme.custom.css');
				}else{
					$this->_js('jquery-treetable/javascripts/src/jquery.treetable.js');
					
				}
				break;
				case 'timepicker':
				if($tipo=='css'){
					$this->_css('bootstrap-timepicker/css/bootstrap-timepicker.min.css');
				}else{
					$this->_js('bootstrap-timepicker/js/bootstrap-timepicker.js');
					
				}
				break;
				case 'moment':
				if($tipo!='css'){
					$this->_js('moment/min/moment.min.js');
					$this->_js('moment/lang/es.js');
				}
				break;
				case 'flot':
					if($tipo=='css'){
					}else{
						$this->_js('flot-chart/jquery.flot.js');
						$this->_js('flot-chart/jquery.flot.categories.js');
						$this->_js('flot-chart/jquery.flot.pie.js');
						$this->_js('flot-chart/jquery.flot.tooltip.js');
						$this->_js('flot-chart/jquery.flot.resize.js');
						$this->_js('flot-chart/jquery.flot.fillbetween.js');
						$this->_js('flot-chart/jquery.flot.stack.js');
						$this->_js('flot-chart/jquery.flot.spline.js');

					} 
				case 'estadisticas':
					if($tipo=='css'){
					}else{
						$this->_js('jquery-highcharts/highcharts.js');
						$this->_js('jquery-highcharts/highcharts-more.js');
						$this->_js('jquery-highcharts/data.js');
						$this->_js('jquery-highcharts/drilldown.js');
						$this->_js('jquery-highcharts/exporting.js');
						$this->_js('jquery-highcharts/exporting.js');
						$this->_js('jquery-highcharts/funnel.js');

					} 
				break;
				case 'tablas':
					if($tipo=='css'){
						$this->_css('DataTables/media/css/jquery.dataTables.css');
						$this->_css('DataTables/media/css/dataTables.bootstrap.css');
						$this->_css('DataTables/extensions/TableTools/css/dataTables.tableTools.min.css');
						$this->_css('DataTables/extensions/responsive/dataTables.responsive.css');
					}else{
						$this->_js('DataTables/media/js/jquery.dataTables.min2.js');
						$this->_js('DataTables/media/js/dataTables.bootstrap.js');
						
						$this->_js('DataTables/extensions/TableTools/js/dataTables.tableTools.min.js');
						$this->_js('DataTables/extensions/responsive/dataTables.responsive.js');

					} 

				break;
				case 'tareas':
					if($tipo=='css'){
						$this->_css('bootstrap-select/bootstrap-select.css');
						$this->_css('bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');
						$this->_css('bootstrap-timepicker/css/bootstrap-timepicker.min.css');
						$this->_css('DataTables/media/css/jquery.dataTables.css');
						$this->_css('DataTables/media/css/dataTables.bootstrap.css');
						$this->_css('DataTables/extensions/responsive/dataTables.responsive.css');
					}else{
						$this->_js('moment/moment.min.js');
						$this->_js('moment/moment-with-locales.min.js');
						$this->_js('bootstrap-select/bootstrap-select.js');
						$this->_js('bootstrap-datepicker/js/bootstrap-datepicker.js');
						$this->_js('DataTables/media/js/jquery.dataTables.js');
						$this->_js('bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');
						$this->_js('DataTables/extensions/responsive/dataTables.responsive.js');
					} 
				

				break;
				case 'cargar':
					if($tipo=='css') $this->_css('uploadify/uploadify.css');
					else $this->_js('uploadify/jquery.uploadify.min.js?ver='.date('U'));
				break;
				case 'dateranger':
					if($tipo=='css'){

						$this->_css('bootstrap-daterangepicker/daterangepicker-bs3.css');
					}else{
						$this->_js('bootstrap-datepicker/js/bootstrap-datepicker.js');
						$this->_js('moment/moment.js');	
						$this->_js('bootstrap-daterangepicker/daterangepicker.js');
					} 
				break;
				case  'ckeditor':
					if($tipo=='css'){
						$this->_css('summernote/summernote.css');
					}else{
						$this->_js('summernote/summernote.js');
					}


				break;
				case 'editor':

					if($tipo=='css'){
						$this->_css('bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
					}else{
						$this->_js('bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');
					} 
			
				break;
				case 'datetimepicker':
					if($tipo=='css'){
						
						$this->_css('bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');
						$this->_css('bootstrap-timepicker/css/bootstrap-timepicker.min.css');
					}else{
						$this->_js('moment/moment.min.js');
						$this->_js('moment/moment-with-locales.min.js');
						$this->_js('bootstrap-datepicker/js/bootstrap-datepicker.js');
						$this->_js('bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');
					} 
				

				break;
				case 'timepicker':
					if($tipo=='css'){
						$this->_css('bootstrap-timepicker/css/bootstrap-timepicker.min.css');
						$this->_css('bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');
					}else{
						$this->_js('moment/moment.min.js');
						$this->_js('moment/moment-with-locales.min.js');
						$this->_js('bootstrap-datetimepicker/build/js/bootstrap-timepicker.js');

					} 
				break;
				case 'notas':
					if($tipo=='css'){
						//$this->_css('bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');
					}else{
						$this->_js('comentarios/notas.js');
						//$this->_js('bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');
					}
				break;
				case 'progressbar':
					if($tipo=='css'){
						$this->_css('bootstrap-progressbar/css/bootstrap-progressbar-3.1.1.min.css');
					}else{
						$this->_js('bootstrap-progressbar/bootstrap-progressbar.min.js');
						$this->_js('bootstrap-progressbar/ui-progressbars.js');
					}
				break;
			}
		}
		
	}
	private function _js($js){

		echo '<script type="text/javascript" src="vendors/'.$js.'"></script>';
	}
	private function _css($css){

		echo '<link type="text/css" rel="stylesheet" href="vendors/'.$css.'" />';
	}

	private function _menu_li(){

		return '<li><a href="#"><i class="fa fa-laptop fa-fw">
                        <div class="icon-bg bg-pink"></div>
                    </i><span class="menu-title">Frontend</span></a>
                    </li>';
	}
}
