<?php
function pdf_fecha($fecha){
	setlocale(LC_ALL,"es_ES");
	return strftime("%d de %B del %Y", strtotime($fecha));
}
function br2nl($string)
{
    return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
}

function transform_text($string) {


	$str = html_entity_decode(strip_tags($string));

	return $str;

}

function desencriptar($code){
	$code = base64_decode($code);
	list($id, $dni) = explode(':', $code);
	return $id;

}
function get_php_input(){

    $request = file_get_contents('php://input');

    if (!strlen($request)) {

        return false;
    }

    return json_decode($request);
}
function encriptar($user){
	return base64_encode($user->id.':'.$user->dni);

}
function non_empty(array $a) {
    return array_sum(array_map(function($b) {return empty($b) ? 0 : 1;}, $a));
}
function ultimoDiaSemana(){
	$semana = date('N', strtotime( "-1 day", time() ));

    switch ($semana) {
    	case 7:
    		$i = 3;
    	break;
    	case 6:
    		$i = 2;
    	break;
    	default:
    		$i = 1;
    }

    return  date( 'Y-m-d' , strtotime( "-$i day", time() ));

}
function getUltimoDiaMes($elAnio,$elMes) {
  return date("Y-m-d",(mktime(0,0,0,$elMes+1,1,$elAnio)-1));
}
function headers_api(){

	if (isset($_SERVER['HTTP_ORIGIN'])) {
		    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		    header('Access-Control-Allow-Credentials: true');
		    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
		}
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		    header("Access-Control-Allow-Headers: *");
		}
}
function convert_array($datos){
	$ar = array();
	foreach ($datos as $row) {
		$ar[$row->id] = $row;
	}
	return $ar;
}
function create_url_calendar($name,$inicio,$fin,$descp){
	$html = '';
	$html .= '<a class="btn btn-blue btn-xs" href="http://www.google.com/calendar/event?action=TEMPLATE&text='.urlencode($name);
	$html .= '&dates='.date('Ymd\THi00\Z', (strtotime($inicio) + 5*60*60) ).'/'.date('Ymd\THi00\Z', (strtotime($fin) + 5*60*60) );
	$html .= '&details='.urlencode($descp).'&location=Nativos+Digitales&trp=false&sprop=&sprop=name:"';
    $html .= 'target="_blank" rel="nofollow"><i class="fa fa-calendar"></i> Calendar</a>';
    return $html;
}
function dameFecha($fecha,$dia)
{   list($day,$mon,$year) = explode('/',$fecha);
    return date('d/m/Y',mktime(0,0,0,$mon,$day+$dia,$year));
}

function calcularigv($total,$igv){

	return round( ($total*$igv) / 100,2);
}
function array_random($arr, $num = 1) {
    shuffle($arr);

    $r = array();
    for ($i = 0; $i < $num; $i++) {
        $r[] = $arr[$i];
    }
    return $num == 1 ? $r[0] : $r;
}
 function data_last_month_day() {
      $month = date('m');
      $year = date('Y');
      $day = date("d", mktime(0,0,0, $month+1, 0, $year));

      return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
  }
function urlsafe_b64decode($string)
{
  $data = str_replace(array('-','_','.'),array('+','/','='),$string);
  $mod4 = strlen($data) % 4;
  if ($mod4) {
    $data .= substr('====', $mod4);
  }
  return base64_decode($data);
}
function getDiasHabiles($fechainicio, $fechafin, $diasferiados = array()) {
        // Convirtiendo en timestamp las fechas
        $fechainicio = strtotime($fechainicio);
        $fechafin = strtotime($fechafin);

        // Incremento en 1 dia
        $diainc = 24*60*60;

        // Arreglo de dias habiles, inicianlizacion
        $diashabiles = array();

        // Se recorre desde la fecha de inicio a la fecha fin, incrementando en 1 dia
        for ($midia = $fechainicio; $midia <= $fechafin; $midia += $diainc) {
                // Si el dia indicado, no es sabado o domingo es habil
                if (!in_array(date('N', $midia), array(6,7))) { // DOC: http://www.php.net/manual/es/function.date.php
                        // Si no es un dia feriado entonces es habil
                        if (!in_array(date('Y-m-d', $midia), $diasferiados)) {
                                array_push($diashabiles, date('Y-m-d', $midia));
                        }
                }
        }

        return count($diashabiles);
}
function dateTimeDiff($date1, $date2) {

    $alt_diff = new stdClass();
    $alt_diff->y =  floor(abs($date1->format('U') - $date2->format('U')) / (60*60*24*365));
    $alt_diff->m =  floor((floor(abs($date1->format('U') - $date2->format('U')) / (60*60*24)) - ($alt_diff->y * 365))/30);
    $alt_diff->d =  floor(floor(abs($date1->format('U') - $date2->format('U')) / (60*60*24)) - ($alt_diff->y * 365) - ($alt_diff->m * 30));
    $alt_diff->h =  floor( floor(abs($date1->format('U') - $date2->format('U')) / (60*60)) - ($alt_diff->y * 365*24) - ($alt_diff->m * 30 * 24 )  - ($alt_diff->d * 24) );
    $alt_diff->i = floor( floor(abs($date1->format('U') - $date2->format('U')) / (60)) - ($alt_diff->y * 365*24*60) - ($alt_diff->m * 30 * 24 *60)  - ($alt_diff->d * 24 * 60) -  ($alt_diff->h * 60) );
    $alt_diff->s =  floor( floor(abs($date1->format('U') - $date2->format('U'))) - ($alt_diff->y * 365*24*60*60) - ($alt_diff->m * 30 * 24 *60*60)  - ($alt_diff->d * 24 * 60*60) -  ($alt_diff->h * 60*60) -  ($alt_diff->i * 60) );
    $alt_diff->invert =  (($date1->format('U') - $date2->format('U')) > 0)? 0 : 1 ;

    return $alt_diff;
}

function array_dias_laborables($dias,$feriados){
	$fechas = array();
	for ($i=1; $i <  30; $i++) {
		$fecha = date("Y-m-d",strtotime ( '-'.$i.' days' ));
		$dia = date('N', strtotime($fecha));
		if(in_array($fecha, $feriados)){
			continue;
		}
		if( $dia == 6 or $dia == 7 ){
			continue;
		}
		$fechas[] = $fecha;

	}
	return $fechas;
}
function numero_dias($start='',$end=''){
	if( empty($start) or empty( $end ) ) return '0';
	$start = strtotime($start);
	$end = strtotime($end);

	return ceil(($end - $start) / 86400);
}
function numeros_dias($inicio,$fin,$feriados){


	$start = strtotime($inicio);
	$end = strtotime($fin);
	$dias = ceil(($end - $start) / 86400);


	$dias_totales = 0;
	for ($i=0; $i <= abs($dias); $i++) {

		$fecha = date("Y/m/d", strtotime("$inicio +$i day"));
		$dia = date('N', strtotime($fecha));
		if(in_array($fecha, $feriados)){

			$dias_totales--;
		}
		if( $dia == 6 or $dia == 7 ){
			$dias_totales--;
		}
		$dias_totales++;
	}

	return $dias_totales;
}
function nolaborable($inicio,$fin,$dias,$feriados=array()){

	for ($i=0; $i <= $dias; $i++) {
		$fecha = date("Y/m/d", strtotime("$inicio +$i day"));
		$dia = date('N', strtotime($fecha));
		if(in_array($fecha, $feriados)){

			$dias++;
		}
		if( $dia == 6 or $dia == 7 ){
			$dias++;
		}
	}
	return date("m/d/Y", strtotime("$inicio +$dias day"));
}
function reverse_number_format($num)
{
	$num = (float)str_replace(',', '', $num);
	return $num;
}
function generateRandomColor()
{
    $r = rand(128,255);
    $g = rand(128,255);
    $b = rand(128,255);
    $color = dechex($r) . dechex($g) . dechex($b);
    return "#".$color;
}
function tarea_save($fecha){
	list($fecha,$hora) = explode(' ', $fecha);



   	$ar_fecha = explode("/",$fecha);
   	if(!isset($ar_fecha[2])) $ar_fecha[2] = date('Y');

	$ar_inicio = explode(":", $hora );
	$fecha = mktime($ar_inicio[0], $ar_inicio[1], 0, $ar_fecha[1], $ar_fecha[0], $ar_fecha[2]);
	//echo date('Y-m-d H:i:s',$fecha);
	return date('Y-m-d H:i:s',$fecha);

}
function tiempo_mysql($hora,$fecha){

   	$ar_fecha = explode("/",$fecha);
   	if(!isset($ar_fecha[2])) $ar_fecha[2] = date('Y');
	$ar_inicio = explode(":", $hora );
	$fecha = mktime($ar_inicio[0], $ar_inicio[1], 0, $ar_fecha[1], $ar_fecha[0], $ar_fecha[2]);
	//echo date('Y-m-d H:i:s',$fecha);
	return date('Y-m-d H:i:s',$fecha);
}
function reporte_fecha($fecha){
	$ar_fecha = explode("-",$fecha);

	return $ar_fecha[2].'-'.$ar_fecha[1].'-'.$ar_fecha[0];

}

function datephp_datejs($date){
	$ar = explode('-', $date);

	$m = $ar[1]-1;
	$date = $ar[0].','.$m.','.$ar[2];
	return $date;
}



function join_date_hour($date,$hour){
	$date = save_fecha($date);
	$string = $date.' '.$hour.':00';

	return $string;
}

function get_hora_vuelo($date) {
    $date = strtotime($date);

    return date('H:i', $date);
}

function get_fecha_vuelo($date) {
    $date = strtotime($date);

    $mes = date('F', $date);

    switch ($mes) {
        case 'January':
            $mes = 'Enero';
            break;
        case 'February':
            $mes = 'Febrero';
            break;
        case 'March':
            $mes = 'Marzo';
            break;
        case 'April':
            $mes = 'Abril';
            break;
        case 'May':
            $mes = 'Mayo';
            break;
        case 'June':
            $mes = 'Junio';
            break;
        case 'July':
            $mes = 'Julio';
            break;
        case 'August':
            $mes = 'Agosto';
            break;
        case 'September':
            $mes = 'Septiembre';
            break;
        case 'October':
            $mes = 'Octubre';
            break;
        case 'November':
            $mes = 'Noviembre';
            break;
        case 'December':
            $mes = 'Diciembre';
            break;
    }

    return date('j', $date) . ' de ' . $mes . ' de ' . date('Y', $date);
}



function save_fecha($fecha){
	if(empty($fecha)) return false;

	$ar_fecha = explode("/",$fecha);
	//var_dump($ar_fecha);

	return $ar_fecha[2].'-'.$ar_fecha[1].'-'.$ar_fecha[0];
}

function get_fecha($fecha){
	if(empty($fecha) or $fecha == '0000-00-00') return;
	$ar_fecha = explode("-",$fecha);
	return $ar_fecha[2].'/'.$ar_fecha[1].'/'.$ar_fecha[0];
}
function get_fecha_reporte($fecha){
	if(empty($fecha) or $fecha == '0000-00-00') return;
	$ar_fecha = explode("-",$fecha);
	return $ar_fecha[2].'/'.$ar_fecha[1];
}
function unixtime_fechamysql($date){

	return date('Y-m-d',$date);
}
function hora_fecha_mysql_save($string){


	list($date,$hora) = explode(' ',$string);
	//var_dump($date);
	$ar_fecha = explode("/",$date);
	$date = $ar_fecha[2].'-'.$ar_fecha[1].'-'.$ar_fecha[0];

	return $date.' '.$hora.':00';



	//return date('Y-m-d',$date);
}
function tiempo_mysql_save($date){
	$date = strtotime($date);
	return date('Y-m-d H:i:s',$date);
}
function unixtime_mysql_save($date){
	return date('Y-m-d H:i:s',$date);
}
function get_fecha_actual_mysql(){
	date_default_timezone_set("America/Lima");
	return date('Y-m-d H:i:s');

}
function parse_bbcode($str = '', $max_images = 0){
// Max image size eh? Better shrink that pic!
if($max_images > 0):
   $str_max = "style=\"max-width:".$max_images."px; width: [removed]this.width > ".$max_images." ? ".$max_images.": true);\"";
endif;

$find = array(
  "'\[b\](.*?)\[/b\]'is",
  "'\[i\](.*?)\[/i\]'is",
  "'\[u\](.*?)\[/u\]'is",
  "'\[s\](.*?)\[/s\]'is",
  "'\[img\](.*?)\[/img\]'i",
  "'\[url\](.*?)\[/url\]'i",
  "'\[url=(.*?)\](.*?)\[/url\]'i",
  "'\[link\](.*?)\[/link\]'i",
  "'\[link=(.*?)\](.*?)\[/link\]'i"
);

$replace = array(
  '<strong>\\1</strong>',
  '<em>\\1</em>',
  '<u>\\1</u>',
  '<s>\\1</s>',
  '<img src="\\1" alt="" />',
  '<a href="\\1" target="_blank">\\1</a>',
  '<a href="\\1" target="_blank">\\2</a>',
  '<a href="\\1" target="_blank">\\1</a>',
  '<a href="\\1" target="_blank">\\2</a>'
);

return preg_replace($find, $replace, $str);

}
function get_fecha_time($date){
	$date = strtotime($date);
	return date('Y-m-d',$date);

}
function get_fecha_mysql($date){
	if($date == '0000-00-00 00:00:00') return '';
	$date = strtotime($date);
	return date('d/m/Y',$date);

}
function get_hora_mysql($date){
	$date = strtotime($date);
	return date('H:i:s',$date);

}
function get_timepicker($date){
	if(empty($date)) return;
	$date = strtotime($date);
	return date('d/m/y H:i',$date);

}
function save_timepicker($date){
	//$string = $date.':00';
	$string = $date;
	if(empty($string)) return;
	$ar = explode(' ', $string);
	list($dia,$mes,$ano) = explode("/", $ar[0]);
	$hora = $ar[1];
	$string = $mes.'/'.$dia.'/'.$ano.' '.$hora;
	//var_dump($string);
	$date = strtotime($string);
	return date('Y-m-d H:i:s',$date);

}
function get_fecha_hora_mysql_input($date){
	if($date == '0000-00-00 00:00:00') return '---';

	$date = strtotime($date);
	return date('d/m/y H:i',$date);

}
function get_fecha_hora_mysql($date){
	if($date == '0000-00-00 00:00:00') return '---';

	$date = strtotime($date);
	return date('d.m.y H:i',$date);

}
function get_current_fecha_mysql($date){
	$date = strtotime($date);
	return date('d.m.Y');

}
function get_hora_total_mysql($date){
	$date = strtotime($date);
	return date('d.m.y H:i:s',$date);

}
function encrypt_password($salt, $password) {
    $pepper = '';
    $digest = $pepper;
    $stretches = 10;

    for ($i=0; $i<$stretches; $i++) {
        $join = '--'.$salt.'--'.$digest.'--'.$password.'--'.$pepper.'--';
        $digest = Sha1($join);
    }
    $result = substr($digest, 0, 40);
    return $result;
}
function edadActual($fecha){
    // Comprobamos que $fecha tiene el formato deseado
    if(preg_match("/([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})/", $fecha, $partes)){

        // Validamos que la fecha suministrada sea válida
        if(!checkdate(intval($partes[2]), intval($partes[3]), $partes[1]))
            return false;

        // Calculamos las diferencias con la fecha actual
        $annos = abs(date("Y") - $partes[1]);
        $meses = abs(date("m") - $partes[2]);
        $dias = abs(date("d") - $partes[3]);

        // Corregimos dependiendo de los valores obtenidos
        if($meses < 0){
            $annos--;
        }
        elseif(($meses == 0) && ($dias < 0)){
            $annos--;
        }

        // Devolvemos la cantidad de años
        return $annos;
    } else {
        return false;
    }
}
function hora_min_list($tiempo){
	$retorno = '';

	$dia = floor($tiempo/(60*60*24));
	if($dia > 0 ){
		$tiempo = $tiempo - $dia*60*60*24;
		$retorno .= $dia.'D - ';
	}

	$hora = floor($tiempo/(60*60));
	if($hora > 0 ){

		$tiempo = $tiempo - $hora*60*60;
		if(strlen($hora) == 1 ) $hora = '0'.$hora;
	}else{
		$hora = 0;
	}
	$minutos = floor($tiempo/60);
	if($minutos > 0 ){

		$tiempo = $tiempo - $minutos*60;

		if(strlen($minutos) == 1 ) $minutos = '0'.$minutos;

	}else{
		$minutos = 0;
	}


	$segundos = $tiempo;

	if(!empty($hora)){
		$retorno .= $hora.':';
	}
	if(!empty($minutos)){
		$retorno .= $minutos;
	}else{
		if(!empty($hora)) $retorno .= '00';

	}
	if(empty($hora) and empty($minutos)){
        $retorno .= ' sec';
    }else{
        if(empty($hora) ){
            $retorno .= ' min';
        }
    }
    return $retorno;
}
function hora_list_laboral($tiempo){
	$retorno = '';
	$dia = floor($tiempo/(60*60*8));
	if($dia > 0 ){
		$tiempo = $tiempo - $dia*60*60*8;
		$retorno .= $dia.'D - ';
	}
	$hora = floor($tiempo/(60*60));
	if($hora > 0 ){
		$tiempo = $tiempo - $hora*60*60;
		if(strlen($hora) == 1 ) $hora = '0'.$hora;
	}else{
		$hora = 0;
	}
	$minutos = floor($tiempo/60);
	if($minutos > 0 ){

		$tiempo = $tiempo - $minutos*60;

		if(strlen($minutos) == 1 ) $minutos = '0'.$minutos;

	}else{
		$minutos = 0;
	}


	$segundos = $tiempo;

	if(!empty($hora)){
		$retorno .= $hora.'h ';
	}
	if(!empty($minutos)){
		$retorno .= $minutos.':';
	}else{
		if(!empty($hora)) $retorno .= '00:';

	}
	if(strlen($segundos) == 1 ) $segundos = '0'.$segundos;
	$retorno .= $segundos;


	if(empty($hora) and empty($minutos)){
        $retorno .= ' sec';
    }else{
        if(empty($hora) ){
            $retorno .= ' min';
        }
    }
    return $retorno;
}
function hora_list($tiempo){
	$retorno = '';

	$dia = floor($tiempo/(60*60*24));
	if($dia > 0 ){
		$tiempo = $tiempo - $dia*60*60*24;
		$retorno .= $dia.'D - ';
	}

	$hora = floor($tiempo/(60*60));



	if($hora > 0 ){

		$tiempo = $tiempo - $hora*60*60;
		if(strlen($hora) == 1 ) $hora = '0'.$hora;
	}else{
		$hora = 0;
	}
	$minutos = floor($tiempo/60);
	if($minutos > 0 ){

		$tiempo = $tiempo - $minutos*60;

		if(strlen($minutos) == 1 ) $minutos = '0'.$minutos;

	}else{
		$minutos = 0;
	}


	$segundos = $tiempo;

	if(!empty($hora)){
		$retorno .= $hora.'h ';
	}
	if(!empty($minutos)){
		$retorno .= $minutos.':';
	}else{
		if(!empty($hora)) $retorno .= '00:';

	}
	if(strlen($segundos) == 1 ) $segundos = '0'.$segundos;
	$retorno .= $segundos;


	if(empty($hora) and empty($minutos)){
        $retorno .= ' sec';
    }else{
        if(empty($hora) ){
            $retorno .= ' min';
        }
    }
    return $retorno;
}
function fecha_list($date){
	date_default_timezone_set("America/Lima");
	setlocale(LC_ALL,"es_PE");
	setlocale(LC_TIME,"es_ES.utf8");

	if($date == '0000-00-00 00:00:00') return '';
	$fecha = date('m.d.y',strtotime($date));

	$hoy = date( "m.d.y" );


	$ayer = date( "m.d.y", strtotime( "-1 day", strtotime( $hoy ) ) );


	if($fecha == $hoy){
		return 'Hoy';
	}elseif ($fecha == $ayer) {
		return 'Ayer';
	}else{

		return date('D j. M',strtotime($date));
	}

}

function fecha_lista($date){
	setlocale(LC_ALL,"es_ES");
	$date = strtotime($date);

	return date("j M, Y, g:i a",$date);

}
if ( ! function_exists( 'array_column' ) ) {
	function array_column(array $array, $column_key, $index_key=null){

	    $result = [];
	    foreach($array as $arr){
	        if(!is_array($arr)) continue;

	        if(is_null($column_key)){
	            $value = $arr;
	        }else{
	            $value = $arr[$column_key];
	        }

	        if(!is_null($index_key)){
	            $key = $arr[$index_key];
	            $result[$key] = $value;
	        }else{
	            $result[] = $value;
	        }

	    }

	    return $result;
	}
}

function get_id_youtube($url){
	parse_str( parse_url( $url, PHP_URL_QUERY ), $ar );
	return $ar['v'];
}
function img_ver($base,$image){
	$image = trim($image);
	return $base.'_files/'.substr($image, 0, 2).'/'.substr($image, 2, 2).'/'.$image;
}
function archivo_ruta($image){
	$image = trim($image);
	return './_files/'.substr($image, 0, 2).'/'.substr($image, 2, 2).'/'.$image;
}

function array_meses($mes){

	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	return $meses[$mes];

}
