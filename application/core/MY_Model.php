<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {

	var $main_table = "";
	var $main_table_id_name = "id";
	var $sec_models = false;
	var $sec_models_id_name = "";
	var $main_table_join = "";
	var $pagination_uri_segment = 2;
	var $pagination_base_url = "";
	var $pagination_total_rows = 0;
	var $pagination_perpage = 20;
	var $pagination_disable_first_last = true;

	var $result_limit = 20;

	function __construct(){
		parent::__construct();

		if($this->sec_models){

			$this->sec_models = explode("|", $this->sec_models);
		}

		$this->order_by = "-id";

        $this->load->model('m_log');
	}

    function order_by ($sort = false) {

        if ($sort) {

            $this->order_by = $sort;
        }
    }


	public function combo($name,$id='id'){
		$data = $this->get_result();
		$options = array();
		$options[''] = 'Seleccionar...';
		foreach($data as $row){

			 $options[$row->$id] = $row->$name;
		}
		return $options;
	}


	public function get_id($id){


		$this->db->select($this->main_table .'.*');
		$this->db->from($this->main_table );
		$this->db->where($this->main_table .'.id', $id);
		if(  method_exists($this, 'join') ) $this->join();
		$query = $this->db->get();
		$row =  $query->row();
		if(isset($this->combos) and count($this->combos) > 0){

			foreach ($this->combos as $ret) {
					$row->$ret = $this->$ret;
			}
		}

		return $row;
	}

	////////////////////////////////////

	public function get_result($offset = 0){

		if($this->select_only_actives){
			$this->db->where("activo", "si");
		}

		if(  method_exists($this, 'join') ) $this->join();

		$query = $this->db->select($this->main_table.".*")
							->from($this->main_table)
							->limit($this->result_limit, $offset)
							->get();
		return $query->result();
	}

	public function get_result_where($condition){
		if($this->select_only_actives){
			$this->db->where("activo", "si");
		}
		if(  method_exists($this, 'join') ) $this->join();

		//var_dump($this->main_table);
		$query = $this->db->select($this->main_table.".*")
							->from($this->main_table)
							->where($condition, null, false)
							->get();

		return $query->result();
	}

	public function get_result_in($in_array){

		if($this->select_only_actives){
			$this->db->where("activo", "si");
		}

		$query = $this->db->select("*")
							->from($this->main_table)
							->where_in("id", $in_array)
							->get();

		return $query->result();
	}

	public function get_result_by_array($field, $array){

		if($this->select_only_actives){
			$this->db->where("activo", "si");
		}

		$query = $this->db->select("*")
							->from($this->main_table)
							->where_in($field, $array)
							->get();

		return $query->result();
	}

	public function get_result_all(){

		if(  method_exists($this, 'join') ) $this->join();
		$this->db->select($this->main_table.'.*');
		if (substr($this->order_by, 0, 1) === "-") {

			$this->db->order_by(substr($this->order_by, 1), "DESC");
		} else {

			$this->db->order_by($this->order_by, "ASC");
		}


		if($this->select_only_actives){
			$this->db->where("activo", "si");
		}

		$query = $this->db->from($this->main_table)->get();

		//var_dump($this->db);
		//var_dump($query);
		return $query->result();
	}

	public function get_num_rows(){


		if($this->select_only_actives){
			$this->db->where("activo", "si");
		}

		$query = $this->db->select("*")
							->from($this->main_table)
							->get();

		return $query->num_rows();
	}

	public function get_row($id){

		if($this->select_only_actives){
			$this->db->where("activo", "si");
		}

		$query = $this->db->select("*")
							->from($this->main_table)
							->where("id", $id)
							->get();

		return $query->row();
	}

	public function get_row_where($condition = ""){

		if($condition){

			if($this->select_only_actives){
				$this->db->where("activo", "si");
			}
			if(  method_exists($this, 'join') ) $this->join();
			$query = $this->db->select($this->main_table.'.*')
								->from($this->main_table)
								->where($condition, null, false)
								->limit(1)
								->get();

			return $query->row();
		}

		return false;
	}

	public function get_last(){

		if($this->select_only_actives){
			$this->db->where("activo", "si");
		}

		$query = $this->db->select("*")
							->from($this->main_table)
							->limit(1, 0)
							->get();

		return $query->row();
	}

	public function save($data){

		$id = false;

		if( is_array($data) and isset( $data['id'] ) ){

			$id = $data['id'];

			unset($data['id']);
		}

        if($id){

            // update
            $this->db->where("id", $id);
            $this->db->update($this->main_table, $data);

            //Log
            $this->m_log->save_log($id, $this->main_table, 'update');

            return $id;
        }else{
            //insert
            $this->db->insert($this->main_table, $data);
            $id = $this->db->insert_id();

            //Log
            $this->m_log->save_log($id, $this->main_table, 'insert');

            return $id;
        }
	}

	public function delete($id){

		if($this->sec_models and count($this->sec_models)){

			foreach($this->sec_models as $model_name){

				$this->load->model($model_name);

				$this->$model_name->delete_where($this->sec_models_id_name . " = " . $id);
			}
		}

        //Log
        $this->m_log->save_log($id, $this->main_table, 'delete');

        return $this->db->delete($this->main_table, array("id" => $id));
	}

	public function delete_where($condition = ""){

		$result = $this->get_result_where($condition);

		if($result){

			foreach($result as $row){

				$this->delete($row->entry);
			}
		}

		return true;
	}

	public function delete_by_array($field, $array){

		$result = $this->get_result_by_array($field, $array);

		if($result){

			foreach($result as $row){

				$this->delete($row->entry);
			}
		}

		return true;
	}

	////////////////////////////////////

	function paginate(){

		$this->load->library('pagination');

		$config['uri_segment'] 		= $this->pagination_uri_segment;
		$config['base_url'] 	 	= $this->pagination_base_url;
		$config['total_rows'] 	 	= $this->get_num_rows();
		$config['per_page'] 	 	= $this->result_limit;
		$config['num_links'] 	 	= 10;

		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';

		$config['num_tag_open'] = "<li class='circle'>";
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="disabled"><a>';
		$config['cur_tag_close'] = '</a></li>';

		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['prev_link'] = '<i class="fa fa-arrow-circle-left"></i>';

		$config['next_tag_open'] = '<li class="next">';
		$config['next_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-arrow-circle-right"></i>';

		if($this->pagination_disable_first_last){
			$config['first_tag_open'] = '<!-- <li>';
			$config['first_tag_close'] = '</li> -->';
			$config['first_link'] = 'Primero';

			$config['last_tag_open'] = '<!-- <li>';
			$config['last_tag_close'] = '</li> -->';
			$config['last_link'] = 'Último';
		}else{
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['first_link'] = 'Primero';

			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['last_link'] = 'Último';
		}

		$this->pagination->initialize($config);

		return $this->pagination->create_links();
	}

	function send_mail($mail_data){

        $this->load->library('email');

        $content = "";
        if($mail_data->template_file){
	        // setup
	        $config["mailtype"] = "html";

	        $datos = $mail_data->datos;

	        $content = $this->load->view($mail_data->template_file, $datos, true);
        }


        $this->email->initialize($config);

        $this->email->from($mail_data->from);
        $this->email->to($mail_data->to);

        $this->email->subject($mail_data->subject);
        $this->email->message($content);

        if(isset($mail_data->attach) and $mail_data->attach){
        	$this->email->attach($mail_data->attach);
        }

        if(!$this->email->send()){
            return $this->email->print_debugger();
        }

        return true;
	}
}

class Client_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->select_only_actives = true;

		$this->pagination_perpage = 20;

		$this->pagination_url = base_url();
		$this->pagination_uri_segment = 2;
	}
}

class Admin_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->select_only_actives = false;

		$this->pagination_url = base_url()."master/";
		$this->pagination_uri_segment = 3;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
