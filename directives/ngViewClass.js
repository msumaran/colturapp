﻿
angular.module('newApp')
.directive('ngViewClass', function ($location) {
    return {
        link: function (scope, element, attrs, controllers) {
            var classes = attrs.ngViewClass ? attrs.ngViewClass.replace(/ /g, '').split(',') : [];
            setTimeout(function () {
                if ($(element).hasClass('ng-enter')) {
                    for (var i = 0; i < classes.length; i++) {
                        var route = classes[i].split(':')[1];
                        var newclass = classes[i].split(':')[0];

                        if (route === $location.path()) {
                            $(element).addClass(newclass);
                        } else {
                            $(element).removeClass(newclass);
                        }
                    }
                }
            })

        }
    };
});

angular.module('newApp').directive('focus', function() {
    return function(scope, element) {
        element[0].focus();
    }      
});
 
angular.module('newApp').directive('passwordMatch', [function () {
    return {
        restrict: 'A',
        scope:true,
        require: 'ngModel',
        link: function (scope, elem , attrs,control) {
            var checker = function () {
 
                //get the value of the first password
                var e1 = scope.$eval(attrs.ngModel); 
 
                //get the value of the other password  
                var e2 = scope.$eval(attrs.passwordMatch);
                if(e2!=null)
                return e1 == e2;
            };
            scope.$watch(checker, function (n) {
 
                //set the form control to valid if both 
                //passwords are the same, else invalid
                control.$setValidity("passwordNoMatch", n);
            });
        }
    };
}]);