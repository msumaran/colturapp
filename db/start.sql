-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 03-02-2017 a las 11:35:37
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `colturapp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `parent_id` int(10) NOT NULL,
  `tipo` enum('master','reportes','productos','opciones','usuarios','contabilidad','clientes','ayuda','crm') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'master',
  `modulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `parent_id`, `tipo`, `modulo`, `name`, `icono`, `created_at`, `updated_at`) VALUES
(98, 0, 'crm', 'empresas', 'Empresas', 'fa-briefcase', '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(99, 0, 'crm', 'clientes', 'Clientes', 'fa-star', '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(100, 0, 'crm', 'brief', 'Brief', 'fa-pencil-square-o', '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(101, 0, 'crm', 'contactos', 'Contactos', 'fa-user', '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(102, 0, 'crm', 'perdidos', 'Perdidos', 'fa-user-times', '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(103, 0, 'crm', 'prospectos', 'Prospectos', 'fa-user-plus', '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(104, 0, 'crm', 'visitas', 'Visitas', 'fa-users', '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(105, 0, 'crm', 'reportes', 'Reportes', 'fa-line-chart', '2014-01-16 09:42:46', '2014-01-16 09:42:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opciones`
--

CREATE TABLE `opciones` (
  `id` int(11) NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `calendario_ver` enum('todos','colaboradores','solo') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'solo',
  `resource_id` int(11) DEFAULT NULL,
  `resource_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `calendario_ver`, `resource_id`, `resource_type`, `created_at`, `updated_at`) VALUES
(1, 'Dirección', 'colaboradores', NULL, NULL, '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(2, 'Gerencia', 'solo', NULL, NULL, '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(3, 'colaborador', 'solo', NULL, NULL, '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(4, 'Master', 'todos', NULL, NULL, '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(5, 'Contabilidad', 'todos', NULL, NULL, '2014-01-16 09:42:46', '2014-01-16 09:42:46'),
(6, 'Proyectos', 'todos', NULL, NULL, '2014-01-16 09:42:46', '2014-01-16 09:42:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_menus`
--

CREATE TABLE `roles_menus` (
  `roles_id` int(11) DEFAULT NULL,
  `menus_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `tipo` enum('user','cliente') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activo` enum('si','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'si',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dni` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `sexo` enum('Hombre','Mujer') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Hombre',
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `celular` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `roles_id` int(10) NOT NULL DEFAULT '0',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `passwd` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nacimiento` date NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `logeado` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `tipo`, `name`, `activo`, `email`, `dni`, `sexo`, `direccion`, `celular`, `roles_id`, `encrypted_password`, `passwd`, `foto`, `nacimiento`, `fecha_ingreso`, `sign_in_count`, `logeado`, `created_at`, `updated_at`) VALUES
(1, 'user', 'Admin', 'si', 'admin@none.com', '', 'Hombre', '', '', 0, '', 'e10adc3949ba59abbe56e057f20f883e', '', '0000-00-00', '0000-00-00', 0, NULL, NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles`
--

CREATE TABLE `users_roles` (
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `opciones`
--
ALTER TABLE `opciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_roles_on_name_and_resource_type_and_resource_id` (`name`,`resource_type`,`resource_id`),
  ADD KEY `index_roles_on_name` (`name`);

--
-- Indices de la tabla `roles_menus`
--
ALTER TABLE `roles_menus`
  ADD KEY `index_areas_menus_on_areas_id_and_menus_id` (`roles_id`,`menus_id`),
  ADD KEY `index_areas_menus_on_areas_id` (`roles_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_users_on_email` (`email`),
  ADD KEY `activo` (`activo`);

--
-- Indices de la tabla `users_roles`
--
ALTER TABLE `users_roles`
  ADD KEY `index_users_roles_on_user_id_and_role_id` (`user_id`,`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT de la tabla `opciones`
--
ALTER TABLE `opciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
